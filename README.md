# imuko
> A PHP. imuko api

# Requirements

- PHP v8.1
- Laravel v8
- Composer v2.5
- Base de datos Mysql

## Build Setup
 ``` bash 
# install dependencies 
composer install

# start the Laravel development server: http://127.0.0.1:8000
php artisan serve 

# view php commands and arguments 
php artisan 
```
### Run migrations
> sh migrate.sh

### Generate migrations
> php artisan module:make-migration create_industry_table GeneralData

