<?php

namespace Modules\Payments\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Support\Renderable;
use NextApps\VerificationCode\VerificationCode;
use Modules\Payments\Gateways\Paypal\PaypalPayment;


class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('payments::index');
    }


    public function generateCode(Request $request)
    {

        try {

            $email = "tatiana@imuko.co";
            $code = VerificationCode::send($email);
            return response()->json([
                "status" => 1
            ]);
        } catch (Exception $e) {
            return response()->json([
                "status" => -1,
                "message" => $e->getMessage()
            ]);
        }
    }


    /**
     * Controlador de envio de pagos 
     */
    public function payouts(Request $request)
    {

        try {
            //Validación de los datos de pago
            $batch = $request->input("batch");
            $emailSubject = $request->input('emailSubject');
            $bank = $request->input('bank');
            $code = $request->input('codeSecurity');

            if (!$batch) {
                throw new Exception('No se ha enviado un batch para procesamiento');
            }

            if (!$emailSubject) {
                throw new Exception('No se ha enviado un asunto para el procesamiento');
            }

            if(!VerificationCode::verify($code, "tatiana@imuko.co")){
                throw new Exception('El codigo es incorrecto');
            }


            $payment = new PaypalPayment();
            $idBatch =  $payment->payout($emailSubject, $batch, $bank);

            return response()->json([
                "status" => 1,
                "idBatch" => $idBatch 
            ]);
        } catch (Exception $e) {
            return response()->json([
                "status" => -1,
                "message" => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('payments::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('payments::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('payments::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

}
