<?php
namespace Modules\Payments\Gateways\Paypal;


use PaypalPayoutsSDK\Core\SandboxEnvironment;
use PaypalPayoutsSDK\Core\PayPalHttpClient;


class PaypalClient {

    public static function client() {
        return new PayPalHttpClient(PaypalClient::environment());
    }

    public static function environment() {

        var_dump(env('CLIENT_ID_PAYPAL',''),env('CLIENT_SECRET_PAYPAL',''));
        
        $clientId = env('CLIENT_ID_PAYPAL','');
        $clientSecret = env('CLIENT_SECRET_PAYPAL');
        return new SandboxEnvironment($clientId,$clientSecret);
    }

}