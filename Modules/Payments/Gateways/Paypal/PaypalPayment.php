<?php

namespace Modules\Payments\Gateways\Paypal;

use Modules\Payments\Gateways\Paypal\PaypalClient;
use PaypalPayoutsSDK\Payouts\PayoutsPostRequest;
use Modules\Payments\Gateways\Paypal\Includes\Payout;
use Modules\Payments\Gateways\Includes\PaymentAbstract;


class PaypalPayment extends PaymentAbstract
{

    public $client;

    public function __construct()
    {
        $this->client = PaypalClient::client();
    }


    public function getItemsPayouts($items = [])
    {

        if (!is_array($items)) {
            $items = json_decode($items);
        }


        $payouts = [];
        foreach ($items as $item) {
            $payout = new Payout($item['recipient_type'], $item["receiver"]);
            $amount = $item['amount'];
            $payout->setAmount($amount['currency'], $amount['value']);
            $payout->setNota($item['note']);
            $payout->setChargeAccount($item['account_id']);
            array_push($payouts, $payout);
        }

        return $payouts;
    }

  
    


    public function payout($emailSubject, $batch = [],$bank)
    {

        $request  = new PayoutsPostRequest();
        $items = $this->getItemsPayouts($batch);
        $body = json_decode(
            '{
                "sender_batch_header":
                {
                  "email_subject": "' . $emailSubject . '"
                },
                "items": ' . json_encode($items) . '
              }',
            true
        );

        $request->body = $body;
        $response = $this->client->execute($request);
        $statusCode = $response->statusCode;
        $resultResponse = $response->result; 
        $idBatch = $resultResponse->batch_header->payout_batch_id;
        if($statusCode > 200){
            $this->saveBatchTransaction($items,$idBatch,$bank);
            $this->updateStatusAccountBatch($items,2);

        }

        return $idBatch;

    }
}
