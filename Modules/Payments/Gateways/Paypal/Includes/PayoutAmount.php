<?php

namespace Modules\Payments\Gateways\Paypal\Includes;

use JsonSerializable;

class PayoutAmount implements JsonSerializable {


    private $currency;
    private $value;


    public function __construct($currency,$value)
    {
       $this->currency = $currency;
       $this->value = $value;
    }

    public function setCurrency($currency) {
        $this->currency = $currency;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function getValue() {
        return $this->value;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
  


}