<?php

namespace Modules\Payments\Gateways\Paypal\Includes;

use JsonSerializable;
use Modules\Payments\Gateways\Paypal\Includes\PayoutAmount;


class Payout implements JsonSerializable
{


    private $chargeAccountId;
    private $recipient_type;
    private $receiver;
    private $note;
    private $senderItemId = "";
    private $amount;

    public function __construct($recipientType, $receiver)
    {
        $this->recipient_type = $recipientType;
        $this->receiver = $receiver;
    }

    public function setAmount($currency, $value)
    {
        $this->amount = new PayoutAmount($currency, $value);
    }

    public function setNota($nota)
    {
        $this->note = $nota;
    }

    public function setChargeAccount($accountId) {
        $this->chargeAccountId = $accountId;
    }

    public function getChargeAccount() {
        return $this->chargeAccountId;
    }

    public function getReceiver() {
    
        return $this->receiver;
    }

    public function getAmount()
    {
        return $this->amount;
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
