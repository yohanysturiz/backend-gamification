<?php
namespace Modules\Payments\Gateways\Includes;
use Modules\Payments\Entities\Transaction;
use Modules\PaymentAccount\Entities\PaymentAccount;

abstract class PaymentAbstract {


    public function saveTransaction($accountId,$transactionId,$amount,$bank) {
        //Se guarda la transacción
        $transaction = new Transaction();
        $transaction->transaction_id = $transactionId;
        $transaction->amount = $amount;
        $transaction->bank = $bank;
        $account = PaymentAccount::find($accountId);
        $account->transactions()->save($transaction);
    }

    public function updateStatusAccount($accountId,$statusCode) {
        $account = PaymentAccount::find($accountId);
        $account->status = $statusCode;
        $account->save();
    }

    public function updateStatusAccountBatch($batchs,$statusCode) {
        for($i = 0; $i < count($batchs)  ; $i++){
            $batch = $batchs[$i];
            $this->updateStatusAccount($batch->getChargeAccount(),$statusCode);
        }
    }

    public function saveBatchTransaction($batchs,$idBatch,$bank) {
        for($i = 0; $i < count($batchs)  ; $i++){
            $batch = $batchs[$i];
            $this->saveTransaction($batch->getChargeAccount(),$idBatch,$batch->getAmount()->getValue(),$bank);
        }
    }

}

