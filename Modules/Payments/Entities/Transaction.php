<?php

namespace Modules\Payments\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\PaymentAccount\Entities\PaymentAccount;

class Transaction extends Model
{
    protected $fillable = ["transaction_id"];


    public function chargeAccout() {
        return $this->belongsTo(PaymentAccount::class,'charge_account_id');
    }
   

}
