<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('charge_account_id');
            $table->string("transaction_id",300);
            $table->double('amount');
            $table->string("bank",100);
            $table->timestamps();
        });

        Schema::table('transactions', function (Blueprint $table) {

            $table->foreign('charge_account_id')->references('id')->on('payment_account');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
