<?php

namespace Modules\Payments\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NextApps\VerificationCode\Notifications\VerificationCodeCreatedInterface;
use SendinBlue\Client\Model\SendSmtpEmail;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Api\ContactsApi;
use Illuminate\Queue\SerializesModels;
use Illuminate\Notifications\Messages\MailMessage;

use GuzzleHttp\Client as GuzzleClient;
use phpDocumentor\Reflection\PseudoTypes\True_;

class NotifySendCodePaypal extends Notification implements ShouldQueue, VerificationCodeCreatedInterface
{
    use Queueable;

    /**
     * @var string
     */
    public $code;

    /**
     * Create a new message instance.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail()
    {
        return (new MailMessage)
            ->subject(__('Your verification code'))
            ->greeting(__('Hello!'))
            ->line(__('Your verification code: :code', ['code' => $this->code]))
            ->line(__('Kind regards'));
    }
}
