<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/payments', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'payments', 'middleware' => ['api']],function() {
    Route::get('/', 'PaymentsController@index');
    Route::post('/pay','PaymentsController@payouts');
    Route::post('/code','PaymentsController@generateCode');
  
});
