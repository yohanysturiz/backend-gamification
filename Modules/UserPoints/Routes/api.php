<?php

// use Google\Service\CloudRun\Route;
use Illuminate\Http\Request;
use Modules\UserPoints\Http\Controllers\UserPointsController;
use Modules\UserPoints\Http\Controllers\PointsWalletUserController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('points/user', 'UserPointsController@index');
Route::get('points/user/{id}', 'UserPointsController@userPoints');
Route::get('points/user/{id}/detail', 'UserPointsController@userPointsDetail');
Route::post('points/user', 'UserPointsController@store');

Route::get('points_wallet', 'PointsWalletUserController@index');
Route::get('points_wallet/{user_id}', 'PointsWalletUserController@userPointsWallet');
Route::get('points_wallet/{id}', 'PointsWalletUserController@userPoints');
// route::get('points/user/rankind/{user_id}', 'PointsWalletUserController@pointsPosition');
Route::post('points_wallet', 'PointsWalletUserController@store');
Route::put('point_wallet/{user_id}', 'PointsWalletUserController@edit');
