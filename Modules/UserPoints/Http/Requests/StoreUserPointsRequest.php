<?php

namespace Modules\UserPoints\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreUserPointsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gamification_id'=>'required',
            'user_id'=>'required',
            'points'=>'required'
        ];
    }

    public function messages(){
        return [
            'gamification_id'=>'el gamification_id tienen que ser requeridos',
            'user_id'=>'El user_id tiene que ser requerido',
            'points'=>'Los puntos son requeridos',
        ];

    }

}
