<?php

namespace Modules\UserPoints\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PointsWalletUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>'required',
            'points_total'=>'required',
            'status'=>'nullable'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
