<?php

namespace Modules\UserPoints\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\UserPoints\Entities\UserPoints;
use Modules\UserPoints\Http\Requests\StoreUserPointsRequest;
use DB;

class UserPointsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $users_ranking = UserPoints::join('user', 'user_points.user_id', '=', 'user.id')
            ->select('user.first_name as name', DB::raw('SUM(user_points.points) as total_points'))
            ->groupBy('user.first_name')
            ->orderBy("total_points", "DESC")
            ->get();

        return $users_ranking;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('userpoints::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreUserPointsRequest $request)
    {
        return UserPoints::create($request->all());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function userPoints($id)
    {
        $points = UserPoints::where("user_id", $id)->sum("points");
        $ranking = UserPoints::select("user_id", \DB::raw('@rownum := @rownum + 1 AS position'))
            ->from(\DB::raw('(SELECT @rownum := 0) r, (SELECT user_id, SUM(points) AS total_points FROM user_points GROUP BY user_id) t'))
            ->orderBy('total_points', 'DESC')
            ->get()->toArray();

        $position_user = current(array_filter($ranking, function ($obj) use ($id) {
            return $obj["user_id"] == $id ? $obj["position"] : 0;
        }));

        return response()->json([
            "points" => intval($points),
            "ranking" => $position_user
        ], 200);
    }

    public function userPointsDetail($id)
    {
        $point_details = UserPoints::join('gamification', 'user_points.gamification_id', '=', 'gamification.id')
            ->select('gamification.name as name', 'user_points.points as points', 'user_points.created_at as created_at')
            ->orderBy("user_points.created_at", "DESC")
            ->get();

        return response()->json([
            "detail" => $point_details
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('userpoints::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
