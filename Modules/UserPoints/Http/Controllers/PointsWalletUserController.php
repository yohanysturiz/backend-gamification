<?php

namespace Modules\UserPoints\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\UserPoints\Entities\PointsWalletUser;
use Modules\UserPoints\Http\Requests\PointsWalletUserRequest;
use DB;

class PointsWalletUserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return PointsWalletUser::all();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('points_wallet_user::create');
    }

    public function userPointsWallet($user_id)
    { {
            $user = PointsWalletUser::where('user_id', $user_id)->first();
            if ($user) {
                return response()->json($user, 200);
            }
        }
    }

    public function userPoints($id)
    {
        $points = PointsWalletUser::where("user_id", $id)->sum("points_total");
        $ranking = PointsWalletUser::select("user_id", \DB::raw('@rownum := @rownum + 1 AS position'))
            ->from(\DB::raw('(SELECT @rownum := 0) r, (SELECT user_id, SUM(points_total) AS total_points FROM points_wallet_user GROUP BY user_id) t'))
            ->orderBy('total_points', 'DESC')
            ->get()->toArray();

        $position_user = current(array_filter($ranking, function ($obj) use ($id) {
            return $obj["user_id"] == $id ? $obj["position"] : 0;
        }));

        return response()->json([
            "points" => intval($points),
            "ranking" => $position_user
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(PointsWalletUserRequest $request)
    {
        return PointsWalletUser::create($request->all());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('points_wallet_user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Request $request, $user_id)
    {
        $points_edit = PointsWalletUser::where('user_id', $user_id)->update($request->all());
        return response()->json($points_edit, 202);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
