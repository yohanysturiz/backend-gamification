<?php

namespace Modules\UserPoints\Entities;


use Illuminate\Database\Eloquent\Model;

class UserPoints extends Model
{
    protected $table = "user_points";

    protected $fillable = ['gamification_id', 'user_id','points'];
}