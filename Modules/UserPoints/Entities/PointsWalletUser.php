<?php

namespace Modules\UserPoints\Entities;

use Illuminate\Database\Eloquent\Model;

class PointsWalletUser extends Model
{
    protected $table = "points_wallet_user";

    protected $fillable = ['user_id','points_total','status'];
}
