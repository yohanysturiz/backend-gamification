<?php

namespace Modules\Clients\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Clients\Entities\CustomerRequests;
use Modules\Developer\Entities\Developer;

class EmailRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $candidates;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CustomerRequests $customer,$candidates)
    {
        $this->customer = $customer;
        $this->candidates = $candidates;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('clients::emails.request');
    }
}
