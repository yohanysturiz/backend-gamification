<?php

namespace Modules\Clients\Listeners;

use Modules\Clients\Entities\Contract;
use Carbon\Carbon;

class ReloaddateListener
{

    public function __construct()
    {
        //
    }

    public function handle()
    {
        $this->date();
    }

    protected function date(){

        $tablafechas = Contract::select('end_date')->get();
        $contract_all = 0;
        foreach ($tablafechas as $key => $fechas) {
            $mytime   = Carbon::now();
            $end_date = Carbon::parse($fechas->end_date);
            $diff = $end_date->diffInDays($mytime);
            if ($end_date < $mytime){
            $contract_all = $diff;
             $update = Contract::where('end_date', $fechas->end_date)->update(['status' => "vencido"]);
            }

        }

    }
}
