<div>
    <h1>Solicitud # {{$customer->id}} </h1>
    <p>Damos respuesta a la solcitiud realizada por usted con los siguientes candidatos posibles de acuerdo a los requerimientos enviados.</p>
    <h2>Candidatos</h2>
    <table>
        <tr>
            <th>Nombre</th>
            <th>Correo Electronico</th>
            <th>Télefono</th>
        </tr>
        @foreach($candidates as $candidate)

        <tr>
            <td> {{$candidate->namecandidate}} </td>
            <td> {{$candidate->emailcandidate}} </td>
            <td> {{$candidate->phonecandidate}} </td>

        </tr>
        @endforeach
    </table>

</div>