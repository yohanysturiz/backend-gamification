<!DOCTYPE html>
<html lang="en">
<style>
    .content {
        width: 100%;
    }
   .body{
        margin-left: 50px;
        margin-right: 50px;
        margin-top: 0px;
        margin-bottom: 10px;
    }

    .left {
        padding-left: 0px;
        padding-top: 5px;
        margin-left: 0px;
        float: left;
        position: relative;
        width: 50%;
        height: auto;
    }

    .right {
        padding-top: 5px;
        padding-left: 5px;
        margin-left: 5px;
        position: relative;
        float: left;
        width: 45%;
        height: auto;
    }
</style>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style.css">
    <title>Contrato</title>

</head>

<body>
<div class="logo-div">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWYAAACiCAYAAAEFL1eWAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAHbtJREFUeNpiYBgFo2AUjAK8YMHt/diEGVF4OhH/Ga6sYKTYMkrNWXD7P4ZYgircPCYsFjpQNbRAHiDHwSBHwjCaR1jIsggUijoRCkDWfXiIwvQghzC6OSCLe2oZGHKrGRjYOQSBDvqA1XykUIXzQXoX3HYAsg8wEXQcNj7EwQlEhTBMz8zLEAeDwORWBiD7PRnxth978sDlcFQP3CfZOqBDrdzMGP5fXg7G+DIaTgCNASay0+qVFQ1EeRYpiRzbdQpdVSBRGRGNz4LFsgNAi6iT0SBpHyJf0gxKEgyMupEQufhsBow0veC2AU6HI6VzRgZ6A4hjLgCxAYpjIA4+D8QfgGKC+Iygv6Pxl8cEHUxZmqYeEISHNhEOHiyOJrkkGmhHGwKxANZqexSMAvIAQACNolEwCkYB3nZJAhDPx99g0ok4D255UdqxxdbtIs2x5+EtQCI6tgZUDy2diASSO7YQdxTi69gy4QwtnYgCKP0fSew/mpr7ODsDOhEgy+fDQ3zB7fcMQU0QM3C1NRA98QloMhuIbTD1o3SZsHSfgEABj/7z0MY+BPTUCjDcugZjM5DUSEpQDSTW0Y0Ee+fERTXcgSidWljPnIzOAhOV0i62UFPEq8fGGfd4B7IjF9wOgLISiXH0AwJOVcTbsb2y4gFcHuoYUKcW3rG1cAjEG6KQwRkQfz20G7ZgYPqIeHrYKMNhBMDAdWwJDDIO7j4ikQ4dLI5eQK7GwTPuQUaIj4JRgAUABNAoGgWjYBSMglEw4sHQaFTpRIAmdR3I7FfTts+CAHgn5YhZf6AI754OHHAYwMBtAJL1aKKBwEDdAJUHLUkQYECe6sLSK2AhwioFnOMIOhEgR9gDI8IRTVwA2uc/iHXGH7LmwgEotwCnuYRWCkDG284jjVUwYgkk0HBrApRnCAyACyQGMiwQQeACUL8hls6tIEaKB9Fogc1IRIp2hK5IoGxiEjEGiJpC0AMI2R7s44agEab5KHzkCAN58twJBoZ9W7G7A7QKAjKl/YEmfVcceljIzVBAzyXiDRx0OWotQEMOZHTzYEujoAA+xglKUTYpDAwfv0LGO0ua39OhfjqAzCF3iObhQI6RoESiTgTWZVu7F6AWq/+PzEFwIKPixKyaKiSyIgTJC6ClZkdSy2jqAlDZqxNRj6OYIkZ/Iri4QOgTQMotigwcHPcZfvxgcE2ATASU5oYydG84yMDw+BVq8YEWEMSkSqKnLLAUM7hStCC8yQIqn9GzKWZFdYGIAGJEYwti2AkRh5WdE7C45wCKGQgzD0A9+IDhzAJGcEAKQLR0T16NCGSQOKSMJlTOGkAD9TxS042RAd9AKfoM3JBsR9OvLUxS23j4dVgGIsCpPELOMuIDGXVKbT+tOkejKZpOKXp0Agk1wBuBAdzAMApGwSggAAACsHf2Og3DQBw3FQMSA+0AYkGChQGxdGOjfQCksiBG2EEqAxMLzAzwBsDWBZWJDQkkHiBDxcKCxMYCI0zQi21yThzXdpIWmvuJCuWjNP3nYp/tu4N+CIIgCIIgCIIgxhWaLPKBhycs9l9XODXif4scrfnBEtbHiITFcRtJDFOlpgQxeJ39AYHx6vP7iAT+Ngosz+FphBYi82AVSZuaBm0RC7mgGs+6ggXaMztL/i/E0y+LFpiLW0PbN0LsA5NhVjJ8wWr4KKuWHz0N6mNuelrix3ZEfJ1tO83C3NekQNXf/D3XMkGOba7Ifw3Sbs6kk9XoaYQBLbxKT1VpN00VquR71M+IOpfV7Xr/eGB1Lb3OecICvz55ASlgeaXhkoso/kYDbd1b3IR62jpiJddHN61jAuu0A3cuLUuB9bFzUmAAkpth31PgEpPRRQI2s0jjK/JHLMIHUxf7cVL1RSFt5oDgxAS31yy0cKh84HbD2fBF7nVqhmOB+H080k5xZvo30x1HggoLt7np96MVuaCL8WiiWkpHh0NscbRnH6iZ59jRNQ1u3NBFHjaBtt00F6Fg7Y31rO7c4MFIxMnfEXl1u+vRVNW1HSGkN+jqSAi29k99rnBTGW1G9ScG+dPHfi5cfuD0hZbXYAIXdZTNRq9zw9b6XtfjHZ+UgVoYC3PssLXOw2slU1OhLM4ehty+fHb2p3WW/FKoxGkJPOk8pOw/SIgBXxAqZ0pe31SBgb0jOG/XYzjtPWCpGEXQu2i7sfObhvNrVq6XziWU3gn2UtTj50wGlOP9s/NL2kBwCBqX+/mMmutw2jSIuTQdH9/5ZDUljfvvOI0tLc3MVMHGk3JN2kf1a7PPV4y5C5dlkidINHcFC1w+kbl4Nh3vZp4fWc4UhzSLjdrjbp5NaYURhUMiq95IIUyUVFDvlWeyZPt2uGYcWJAl52rRDSb+V0kRFkwQBEEQpeRHAPaumLeJGAqbf3D9BaRDJQRIFPgDqQQLS1N1YbxMLCCoxMZAKzaWMLA3sKMWMTCwtAsLEs1Clw4Jv6D5B1Cffcld/Pzsu3PSS+771FZtLnF958/287Pf9/AFAAAAAAAAAAAAAAAAAAAAAAAAAKsEnKcJCRXeL/UMBkmsVpOhoiLl2S3fBO3y+P6WV8qeIGRWcbgv9V/dGmT0qguJW1c/h+S160zptngCy44cSnNpTwuNBCYzGsv1fOzZIJrwfJQI1uacSqfDBQn4xu/ETEO2JxmQgDIjuoy2kIG7O0tI4rxqkgn/pEX2IIk005QzayBCeK6HxLHWQxjqEb2jhdriJSJyiyFy1xCCcw6XG2s6JsIW9nqpY48rj8xFGooKdh5PRI6yGmZUT6bEkMp8Jv95cxr0nf5N8Y6BoYJS7dlk69R3EKgtsgExDrp56VOUx9BCympmlQov7lt0M84403geYcJUY0Weii7Z990XfgoAESkKOCXQPmnP+eSDpeu86ZXx2PRycNMxnx47bdisAKELL97EGSm34tmU+U5lC0RdC8ijNfKZ9S+Oru5lZ1FknmencI+idVtw8Z04n5SZajhpgkgxR6k1WAQp6Z/sCnF78yyRWwoX2hhbRtVwctiyLFoiqrPIkVkw0+iNEg2+Z8ikch6Eu08PWYLUA27TKCUyI6a5vdsWz7VK3sdvp+LrF2IdLjuB/JaqTUQO+lXCIheAB8y1E6YDfLC8vlXY81IfRLas6bkR6PQ7eSEVLj3efyYePbyVfMvfDUHTLNKylC+4KrqWDhhO199ezz14M+pJ6H/MQk+IXz+NSz/6b50Fk++ZlrUdwASwLVRfBeks3OYLs5HSBDKX21ZWi7YwphVnxyuXXGemwdRMpWRJc3j8/rPzX5LvmZZ1Gsimtd1TT6chiEqQONKL3V4ZT8nqk9mWvmBKpJ7h/eAU/qutF2wj2hFhdowS+ddZnA+VJq8FybVzwmuWlhU2yzeXLeAyk1+jwxC4M3mf/ZmPfWz9Zij4zYo2z06NVLKT+dSjm9ibdF2inDcm3lhPdsVev4uoRSBHaAOpZG+oxV9R4VQf8eriM0DJkZnzqfpuZfN+2U+W10clbn/EjIxd7zJ4F9/AczE6YOoydi6S1Q7a/YSMWRFvX8jPUJrUVUisRtAz6wjKL/T9nQVu7Wuzgzd6CSbPlaSNwJkjdUH/QtoPLWVuXFX3z28h/mqT4ua6EHceSJ+yq5RxoW1mv5HYXqba9pYr0thWuibvqOrjwYm3VYWbgAOdYqwnzGR5x3oWa5Um8TUAZAapy6BWJAaZm0dqOc0friKJm+XNACTuMdfyZ4XtZ4ujOt8gdgCbA9r9qLwG45nX5Oh7smw3CDIDNrSXrcIwM5oD2/ngdAPnxIPEB3W+QSwAm7cQLJuBemtyZgRkBmpEaFfky1KRGAAAAAAAAAAAAGgY/gvA3hmEOFJEYbhGFhwQ1slhQbyYQRQRkWS9eNuJ7G0PTpAFYZVJFBFE2AniSSQT0JMis4wehIHp8eKgLJM96GEFJ4KHPTlRRw8OuHMQEfYwzZ72Fvt1dSbppKq6qrs66aT/jw2zSXdVdXf9/erV6+oq/AMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACyCWY0AunD1zWkz6WhX2kJt47N2ZIgZpvw9fxopnpHubB7PgRcY2aTmzdUC1ZCzJMTsWjutq4n6HIORSxfXVWPVty1ChcMK43utGJQUQ2o+Oy6yGbWrHvXycmRkHvW8oqxZuGCZmWJZ41Ur5UHMfMl2Qo5ELHJrKImFMZm9VegO3P+qaQS96FkJUsQciJOTdbgTroMxCr0mnt0hOwEVnZhaOVVarE2LeXvg2UgpuueUEeJoh+7M+lb8xVjVdQ94ToSn9j1Ixj0iYp80OpXGku26frMcscefnO86zOeZnOmOtV8GeG7NjtwER3IspdnN003A9gT//qMnYVdIUeni1xl9tyUKpN87bWhDtINzyq1I9LUgjQ8SsDTdAzKXA/KIwvoGqRbCoRGkYlNC+eevLPEm+XrgYvCQtckZow2RqdPHoFIAglaZqGpXEV041wKQhWt7ln2hNANBCnyjVa8bdzHGvUdufD3hZ1PnqaqvBHGw4pN7zd6QlfXOBc61tqIj1swuhmir02floaIyBoWFZGTprdP80xUBmEtQ3akHT07ZTqh6x4utzoZn1ku1n5lNbVOpC+05149HLE+0Wl0j13H19dNq3N91EJWPy10jsM39J+e6/jjd4w9eDDYZ3GRsZeuMPZs6HLR2IdKCpa5Z9W9sFSGbZ+5qNjW1Myj5lX8SlD5JYM0JaMjpTKSbDcr665CyG6EkGtnQr7t/fn0Q8a+vxkWMkHf6Xfafnt/0OLxdbBzQVY7gAfM/IHDYSbPhAu5qLDchQiLzFs6Eulvv+iVSfvR/n33wzmexLVxLOe3OS9ijiucWgaPqqjloonZPxNyHAbpSsEwzDT51XJ+P2VdzBW/AvsfvbWY3VAatQB2ZujWK2h09hj7/ONkpWx/NtzapUnXcn5ulsXcGAulHe1tsMEC5PrN8Ow/qClrRESK7L477hv3efQR9s3W+6z3+9f+X/ouloTnMt93h92WtFiZdn6TE7MsRnu0V7HpN80IzQirzN2lr74Qbv7BabLez9vs6spF/zv9pe++qEUM8kmz5bpuOb+1efOZbym2dVKPQqTHahD/VgtDYJXfrF1hl194RpiIRP3yK4LTH+ST5ig+23kX890BnC1UITNpmHH7vdeUmbY33p7EsZ8oW5TEcRGpO+RCzFlFNR78nxO7Zd37z2Zu1ZQ74LLrUoeY43UwbLkrhVjuxsOLds/WZn6q0WtJY9qq9LWn2hBzvA6GnQ4Nj1q0DN0Nl114zO7ZnrfuLrekLpJzvB9TyDsKFysyjJsHMXeMOxh8gJG92uchSNfA3bgh2/XJtz5SFhW13aJ13lC2OKaP0fn+tZjl5UbMDYWIeqFmnv7Px4TUrB+F+sXWsLvRr7il8SR/3/mDrW58Kc7E+522j8cZCtHXIo3oBg0WirLStJ0PKko8rHT+X5viQ0/VUQX1dptQTP1A4W6EHwa9/g5jW+NPAG/d7LAF70NhuKcfv8D++vee/5sUyoffJPbi9vqWdzXhFAR13WGlefGZ6xm5sUhxXUVLUQpZI+q0PbEs3Z0E/MnWt2ohU3re+XMsC3kSb5470ncIcytmPuDfnULJHcGxlJVN88DVcP30V9/gY5XjQOkoPc+vPmNCrpoes66YNxXNps5+JwksmYxdozRmk7HQeZlMrdWWlFkx8gFHj50PrHfZux+YC5r2p3QDEZYmIOSyFaPBpyNomyZ7SFNUDUGF1QUDh1xhc3a0txxRQsHYNeDWtmuYZiGiuW0FI/M6vq89Hg5yhYOcjvaqght5WXEcLht/8FCQVGzBP08SZvWapk27FhYy5zCRoHWETPFnfrzLMUtZTvK2Sn6nCeDRg5JGC5AN+Hhk3nmkUXB3DsKD9Z+/yNiLFZ14cuQr+7GFLE5LkaHRl2+HW+yWiV8MMc8Tg7dPZOKiVnSNqV8503/ZNYmQJwzEPL+ijxIhuQPF4MYYnbKg4VvLGRIyxAxBJyFTQoaYIei5ETLEnB9B9+ZdyBAzBD03QiYwOD8/dBLnkGEhQ8z5YkXye3tkEvCqwrovQcwgq7iegKsj1pee9MoGwq9DzGDa/rLMKlcl7sSGZP9LEDMAEDOwgnxJ36ahJUcHEGS4UzgqXN7Jk70Ns5vlk0GcOT9+c9STQLLeRaZ60dfmZOIpgKXT8gONMT5VWmk1rayfICxzvqzzAYs7uU3GrTLEDHdjboSMDmAe4a816U45cDIrQoZlhpWmCVpWhSLmg4pcXCQAAAAAAAAAAAAAAAAAAAAAwHT4X4D2zic0sqSO45VlwEVQElBQEKaDjC5j0GRWYfEyHXFXdz1Mx2UhXkzn4kWXmRxWWESSnBZWYTPMycOSHkE2KEM6B10dZdNzGQTZmahxWBw0vTCgIphGQcbTWL+u6ul0p/+8qlevu7r782HfzDLJe1WvXtW3fvWrX1XxHwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBnpigCABhJSvdz+s+Cvi7pK5/iSRV97dX/Lp47QJgBOjG3vKH/vKyv6U7NUV9r6nCnRkFNlAhLXbjSo16ERurXVX1tabEeeF1DmCEmQX7DNr4kHGhxXqDQxlqM5/Wf2/qajyA3YkmvDsqinsqgcUlvtttnaLGpG9UGNQ/arOR1x7vKuh4tUXhjZxlLB12MOZf1UVuGlvRU4Ma17Vigi7phVaiNoOvOvvLxEx7uMOobH0Hej8Q6drGiF7MQ6DMBG1bRo5fbV7hTACZdlGWEXQj81Kq92snZKwTSiRzr/Muk4WKcwuz7snPLeaxmgIkU5Lw1ztIK8KYWxlKKfIhBuaL8Izvy+hmPrPUcRMvOUDtg4jCju+0uP5WGtUTUR+ai7DLR205ZmYm4MN/IiHrJ5mva1g0fC35f3y9RHGsIM0AYQW5aPzI8nVue1eJcpdAyEWXXuagGq6ks42QiXat3zE1LetvxCVfq0SQpXRsIMyDIXZqovjYyFiixzk5OdtViWeAQmSiXdLmsDjyvDUvaPc/5+j0p8nxmRBrWtC2YboHltXoBdhp+zi3nlAnDSlqwNTtUclvEYPL4hmM6JoA9xLC5d/omrSxCFHsvBjFDzmG5BeaW55XxYfosSCgFEqK8LZ9Cwt/v9K9VW5ZXdWMfXSveWKCuohzMb5tCoFd13q8rN394Ud9zy9fCnwrcQNc97uweMmfE5sihYYkAzNaFwM9K6txAD3dWe7x3Gl/ZSUSg1zzL3mVWWzqcrQDf+4rtCJJQ0Wku9nleuHC5tILc63sns4L7+yj/91Cpf/799L9/9GNKfeDJJPVcJry21ChhJshcmI2qIzJLwI8cRd1LY2MXZp9n1lQ2Szabi2KMFb6rsom5FAuz5FDuLgLZYEGncZDiW+eV+2x670VFIYQ5nSAf2LpY82y0nQ2Bf+vH/VZn6Q93/GvEZy8o9Yzu1z483S3fq9G7QEr3Xdty9v7kkN+5V70vnnMeqY6jjzmrdfTruuGv2OdnuVZ/W6dz2WG58XREZTQczMjKd3FCzQryQbCG+pf3lHr7hlIPH4Z5PxH1hrA/qa3p519U6pNPNX4q73xX56NW73DjdXVcdOokYxRlYwGLz/myQ1276JMMk39u5AaUzrwWmyMtFrMUeV9RfpTibrfRyWmXRaub7eZuOss4CSL2uz9pWtLPLZ3sbI9ChWtlgMtoaC/yWrfnIMx5nwSemICmW7IW0ZQd9s7WG2TnVUExpZWzw3vI4juZb+QrytLYjh+L8gP9eX/4/exFuZMlLek+aKleEq51NOLfpzLp+RtXi7n78NTEppZUY9bdz0c7qLTy9UlMXwGB00PkNH7kpig3O8xbbyv1u9vDfaudN5X6whf1oPn5Zqcu4lw8N6ojrnzk4pz5yHkchbnq5AKQCIW5ZRHQ3QGldaDcJs5WVKjQrcnGLBxIJ8rTLd/Ow3XxyssvqVe/8VU186EPdv2d4//8V7321i/VD679LPmDG51DqzhvDyX+t1tbSS5on4u8Ll1yrHe4MjTXne843Cl79tA+aVUc08rbyS1Ih/HBzi2n2SynOdqRCT4HUf7ptVfUoz++pV7/1td7irIgP5ffk9+X+5zEWfLVpGj3NI6BssPvFiLKd3vnnFNuy7XLPsk8QXtNxaCGW/MUdTB2bdy3D8XH/3f7ncQ3/ev2m+ql/AWvBOW+v968lvyGO7dPi1wcXHX+TnGyn/F7I8wwsRS0OB87jUSMb7nJP/6W6LbXXi32tZD7Mfvxj9RdIIl4/9S838UoStyE8bksiMlFN4lp8pNzucM3vhxhntxh/ThR9SyDYy/XxoPkyX3+02eDvOBzT5/PJH8DFuc1x28l4nw8dLeGuC8kH26iXE3j30eYR5+Kxz2XUqa5Es3bm7A3mYD1Pf8vmWvj5H4Nn0jePm++ey/Iazo9pzV/sam0fCeXCTHpQGUBzd0hibLUDZdtIZR9v1TnUSLMo47fIQNFu3zZHSNixQjL4cDGjvuUR1LXRvPZZ5MF40hkxW/efS/Vq8n9iSM0TucrrsUaZlvNWY8OY76+14YItJmAy1KMp206j5S7j968X8q9ohHm8WDT4567dqOnJGJsFruYVXaFqEvCbJbkc0BrEtdGc0Vd/oXED362uKkKGz/yeh25T+5PTGu+ZGlzObpvJKJlYqx9OtF5ZVY4ikjvBxNp467Yt2J8rPwm3OWIqZkQG/izJHs8rOYNu4+HayXdtgfo1mwj+b1qxptKLGlejaI/WsIf55Zn6p2Pe5mIa6Pz6dsykVO6Lyq5Xt8F7ktfU+qdnyd66N6NiprSlzo/q3793W+qLz/9VE8L+dnXf6zUPce5L8mP5KtJnCeIG5/xfoC6lbcifdJaLdt6fGA7plqLJWwEd97W70LA+h10e1KEeXxYUO6+sJPWYiF6a9hNnM2Q0m+HwkLXvUpkp7DS/bN1d86FZ8ymQr+4kfzJWmydLOCkvPCiFv0WI282ug2Nwglyr3pcbEsz87fKYhFPDK6M8T6xYZBCdLgzo+LfZyAktQTlsqGMT9N1eJmz25t2GoqvPnZriBi+/D0j0MNA0pX0m6Is7zkT2T7G8zaq4a4an4igtfpeyxmtrAwpzCWPe8p9lsj6VK5KZo18kILiWw7+PlaX8i1lUtGzukf2LDGdVtnpG/WaWDWb1M/UDQvZ2P47WhxXvj04gZZ0JD1Jt3Vj/bVgh5QiyJ2MyAUryJkeUjAV9Glu++Im23LRbSi6ZJdX++bfZbP2QaW16Bl50djQfjdlozCnZbSfemJ800WHDngpYP3x30PZTO7tJmiAyTc7ah+iy+kkv9JJ/Ple+Bb7qfNKfWWp3yknpnyGtXl+epdFa/7NBJ/L8XBhXRVms/uBjkCmMnuyEYVLJxqZFPYt1e1svmQNSia4CqcavTnPrhIw7xttec86rZMTd42JuM1Up4ycTkee3zh7LtdHlK57f6ew+W2fnCkHKxPTCbSXRbrnm8klEZDWo8bu6Uf+6U6nVXn9kfC3z1xo9x/7CdyoCXLvci7adhNy8Umj7peH7QqaUgCQlVAVrVDnhpyTILG1Qxdkt/w08nJyVFp5nF7kR3EhzACDH+bnrXB02sdCLLX3rfVW6RDu5XuEVtNF002gWwWteU/v349HkMcIhBlgNMU9jSA2hND3jERx6aWJAUaQEWYABDoSEGSEGWBiBDpJpAmCjDADwBAEuqhkmT2CjDADQHQCLUdgXRlyLhYQZH/YXQ5gvER53lOUxbrdqlu4ZkWj/C3LjSueOVnnY2AxA4ARZpfVq4IsplhK8FwRfNfN6pei3HYUixkABmwtu4jyV
        iJRFoxbYsYxRyt8FIQZYNJxDZtzO93ELDQpZZgfQJgBJp6cxz15ig1hBoDkFm1FuW2Vu21D7PrTOAfPTcz3+Ch+MPkHME74xzJLRMbmqX0xSvfFQpbwO9fl21V7rh8gzACgxXRDDT9cbSaaDftHEFwZAOOGnEvodyJMCKqIMhYzAHS3nNNuE+rKWtZHLiHMAIBAI8gIMwBkLtJ5ZfzP+RRPETfFVWUWqOCyQJgBICOxluusOh0OJ+d0VpU5TaVKYQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDk8X9kxNVOrt/0cwAAAABJRU5ErkJggg==" style="width: 200px;object-fit: cover;position: relative;">
         </div>
    <p style="text-align: center;">CONTRATO DE MANDATO ENTRE @if(!empty($ContractPdf->firm)) {{strtoupper($ContractPdf->Developer->namecandidate)}} {{strtoupper($ContractPdf->Developer->lastnamecandidate)}} @else xxxxx @endif
     e IMUKO S.A.S</p>
    <div class="body" style="text-align: justify;">
    <p>Entre los suscritos a saber, <strong>TATIANA PRIETO TRIANA </strong>persona con domicilio y residencia en la
        ciudad de Bogotá D.C Colombia identificada con la Cédula de Ciudadanía No. 52981604
        expedida en Bogotá D.C. domiciliado y residente en la ciudad de Bogotá D.C, actuando en su calidad
        de Representante Legal de <strong>IMUKO S.A.S</strong> sociedad identificada con el NIT 901250358 - 1 como
        consta en el Certificado de Existencia y Representación Legal expedido por la Cámara de Comercio
        de Bogotá D.C, quien para efectos del presente contrato se denominará <strong>EL MANDANTE
        </strong>por una parte, y por la otra, @if(!empty($ContractPdf->firm)) <strong>{{strtoupper($ContractPdf->Developer->namecandidate)}} {{strtoupper($ContractPdf->Developer->lastnamecandidate)}}
        </strong>, persona con domicilio y residencia en la ciudad de {{ucfirst($ContractPdf->cityresidence->name)}}, identificado con {{strtoupper($ContractPdf->type_document)}}
        {{strtoupper($ContractPdf->number_document)}} expedida en {{ucfirst($ContractPdf->cityexpedition->name)}} @else xxxxx  @endif actuando en nombre propio, quien para los efectos del presente
        documento se denominará <strong>EL MANDATARIO</strong>, hemos acordado celebrar el presente
        <strong>CONTRATO DE MANDATO</strong> previas las siguientes</p>
    <p>&nbsp;</p>
    <p style="text-align: center;"><strong>CONSIDERACIONES </strong></p>
    <p>&nbsp;</p>
    <ol>
        <li>Que <strong>LAS PARTES</strong> participan dentro de la ejecución del presente contrato según
            los roles y el conocimiento de cada una de ellas.</li>
        <li>Que, dentro de los objetivos estratégicos de la ejecución de este contrato<strong>,
            </strong>se encuentra el apoyo que prestará <strong>EL MANDATARIO a EL MANDANTE</strong> en la
            ejecución de distintas actividades y proyectos relacionados con labores de desarrollo, de conformidad
            con las actividades y calendario de que disponga <strong>EL MANDANTE</strong></li>
        <li>Que, para garantizar la transparencia en el desarrollo del objeto del presente acuerdo, <strong>LAS
                PARTES</strong> convendrán de manera recíproca la forma y condiciones como se
            manejarán aspectos tales como: cronogramas, actividades a desarrollar, costos de cada una de ellas,
            entre otros aspectos.</li>
    </ol>
    <p>Según lo anterior, <strong>LAS PARTES</strong> acuerdan celebrar el presente <strong>CONTRATO DE
            MANDATO</strong> que se regirá por las siguientes <strong>CLÁUSULAS</strong>:</p>
    <p><strong>PRIMERA. - OBJETO: </strong>Acordar los términos de prestación de los servicios referentes
        a desarrollo web, móvil, y demás de acuerdo a la experticia del <strong>MANDATARIO</strong> y de
        conformidad con los cronogramas y actividades en los que requiera apoyo el <strong>MANDANTE.</strong></p>
    <p>En ese orden de ideas, <strong>EL MANDANTE </strong>faculta al mandatario para la administración y
        ejeejecución de las siguientes actividades.</p>
    <ol>
        <li>Apoyo en las actividades que requiera <strong>EL MANDANTE</strong> relacionadas con el objeto principal del
            presente mandato.</li>
        <li>Actuar como enlace entre EL MANDANTE y las empresas y/o personas que requieran el desarrollo de proyectos en
            los que apoyará EL MANDATARIO desde su experticia y conocimientos.</li>
        <li>demás actividades acordadas entre las partes y que guarden estrecha relación con el objeto
            principal del mandato.</li>
    </ol>
    <p><strong>SEGUNDA. - OBLIGACIONES DE LAS PARTES. LAS PARTES</strong> se comprometen a cumplir con las obligaciones
        que le se&ntilde;ale este mandato y la Ley colombiana e internacional y a las que a continuación se
        enumeran:</p>
    <ol>
        <li>Verificar que los servicios prestados por cada una de <strong>LAS PARTES</strong>, se lleve a cabo de manera
            legal, con integridad, y de acuerdo a los requerimientos solicitados.</li>
        <li>Realizar los aportes en dinero y en especie a que hubiere lugar y definir de manera conjunta el valor de los
            servicios y costos asociados a los mismos en caso de ser requerido para la ejecución de las
            actividades objeto del presente mandato.</li>
        <li>Definir los roles y dejar claras las actividades por rol que se llevarán a cabo para la
        ejecución del presente acuerdo.</li>
        <li>Garantizar la infraestructura tecnológica adecuada para la ejecución de las actividades
            descritas en el presente documento.</li>
        <li>Garantizar la tenencia de recursos propios suficientes, necesarios para llevar a cabo el objeto del
            contrato.</li>
        <li>Ser participantes proactivos del desarrollo positivo de cada de las tareas que implique la ejecución
            del objeto del contrato, de manera cumplida y responsable, tanto ante <strong>LAS PARTES</strong>, como ante
            los clientes y los usuarios finales.</li>
        <li>No efectuar acuerdos, o realizar actos o conductas que tengan por objeto o como efecto la colusión
            del presente contrato.</li>
        <li>Manejar adecuada y razonablemente los recursos del proyecto y obrar con lealtad, responsabilidad y buena fe
            durante la ejejecución del contrato, evitando dilaciones innecesarias.</li>
        <li>Realizar los procesos de registro y legalización correspondientes si hay lugar a ello.</li>
        <li>Pagar los impuestos requeridos para el ejercicio comercial derivados del presente contrato si a ello hubiere
            lugar.</li>
        <li><strong>LAS PARTES</strong> deben responder de manera responsable por sus actividades, de tal forma que si
            por algún tipo de falla, acto, omisión, hecho o irresponsabilidad encontrada, se derivara
            algún tipo de problema judicial, legal, económico, social o que acarree inconvenientes para
            una de <strong>LAS PARTES</strong>, y cuyas causas y consecuencias fuera previsibles o conocidas, la parte
            que haya cometido la irresponsabilidad, mantendrá indemne a la otra.</li>
        <li>Satisfacer las demás obligaciones a su cargo que se deriven de la naturaleza del contrato y de las
            exigencias legales.</li>
        <li>Las demás no consideradas en este documento, y que tengan relación directa con la
        ejecución del objeto de este contrato.</li>
    </ol>
    <p><strong>TERCERA: PLAZO DE EJECUCIÓN:</strong> El presente mandato se suscribe a un término de UN
        (1) a&ntilde;o contado a partir de la firma del presente documento, con renovación automática por
        periodos iguales si ninguna de las partes manifiesta lo contrario antes de UN (1) mes de agotado el plazo para
        la terminación del periodo anual estipulado en la presente cláusula.</p>
    <p><strong>CUARTA. - APORTES Y COMPETENCIAS</strong>: Según las competencias y responsabilidades de cada una
        de <strong>LAS PARTES</strong>, los aportes a realizar estarán descritos principalmente a los ya
        se&ntilde;alados para cada una de ellas por lo que cada una de las responsabilidades y actividades
        estarán en cabeza de la parte a la que le ha sido asignada tal responsabilidad por lo que no se pacta
        ningún tipo de responsabilidad solidaria.</p>
    <p><strong>QUINTA. - DE LOS BENEFICIOS: </strong>Los beneficios que se lograrán como objeto de la
     ejecución del presente contrato, serán establecidos de la siguiente manera:</p>
    <p>Para <strong>EL MANDANTE </strong>los beneficios obtenidos en virtud del presente mandato se circunscriben a
        recibir de parte del <strong>MANDATARIO</strong> el apoyo desde su experticia y conocimientos, para la
        ejecución de los distintos proyectos en los que participe <strong>EL MANDANTE </strong></p>
    <p>Para <strong>EL MANDATARIO</strong>: Su beneficio principal estará dado en actuar como intermediario
        entre: <strong>EL MANDANTE</strong> y las empresas y/o personas que celebren contratos o convenios con
        <strong>EL MANDANTE </strong>para la ejecución de los respectivos proyectos, demás de recibir las
        contraprestaciones que de ello se derivan dependiendo de lo acordado y estipulado con las distintas empresas y
        personas para cada proyecto en particular. &nbsp;</p>
    <p><strong>SEXTA. - SUPERVISION</strong>: Sin que implique pérdida de la autonomía e independencia de
        <strong>LAS PARTES</strong>, se reconocen como complementarias en la ejecución del contrato, por tanto,
        cada una de <strong>LAS PARTES</strong> asume el siguiente protocolo de requerimientos:</p>
    <ol>
        <li>En el momento de requerir algún tipo de apoyo, quien lo necesite, lo comunicará utilizando los
            canales ya establecidos para ello (WhatsApp, Skype, correo electrónico, entre otras).</li>
        <li>Para que un requerimiento se comprenda como aceptado, quien realiza el requerimiento deberá
            publicarlo o ponerlo en conocimiento de quien sea requerido, quien reciba el requerimiento, deberá
            evaluar su capacidad de respuesta al mismo, de tal forma que pueda cumplir en el periodo solicitado o
            acordado.</li>
        <li>De no poder responder por el requerimiento, deberá informar en el momento en que se le haga la
            solicitud para no crear retrasos en el proceso.</li>
    <li>En caso tal que un requerimiento haya sido aceptado por una de <strong>LAS PARTES</strong>, y pasado el
        término del requerimiento no se haya dado respuesta positiva, y que demás no se haya presentado
        ningún tipo de documentación o argumento válido que exponga el porqué de su
        incumplimiento, la parte incumplida deberáhacer llegar a su contraparte, una explicación formal
        vía correo electrónico con la explicación pertinente, y con la solución a dicho inconveniente.</li>
        </ol>

    <p><strong>SÉPTIMA. - INDEPENDENCIA: LAS PARTES</strong> actuarán por su propia cuenta y riesgo, con
        absoluta autonomía, sin relación de subordinación o dependencia entre sí, y sus
        derechos se limitarán, de acuerdo con la naturaleza del contrato, a realizar y exigir el cumplimiento de
        los requerimientos u obligaciones estipuladas para la prestación de las actividades acordadas.</p>
    <p><strong>OCTAVA: EXCLUSIÓN DE LA RELACIÓN LABORAL: </strong>se comprende que este contrato, dada su
        naturaleza, no generará ninguna clase de relación laboral entre ninguna de <strong>LAS
            PARTES</strong> participantes. Cada una de <strong>LAS PARTES</strong> que integra el contrato, reconoce su
        participación de manera voluntaria e independiente.</p>
    <p><strong>DÉCIMA. - CESIÓN: LAS PARTES</strong> no podrán ceder ni subcontratar el presente
        contrato a persona natural o jurídica, nacional o extranjera alguna, sin el consentimiento previo y
        escrito de todas <strong>LAS PARTES</strong>, pudiendo la contraparte reservarse las razones que tenga para
        negar la autorización de la cesión.</p>
    <p><strong>DÉCIMA PRIMERA. CONTRAPRESTACIONES:&nbsp; </strong>Cada una de <strong>LAS PARTES</strong>
        recibirá como parte del presente mandato las respectivas contraprestaciones acordadas entre las mismas y
        derivadas del objeto principal del presente mandato y de lo pactado en los diferentes proyectos que desarrolle
        <strong>EL MANDANTE</strong> y que requieran el apoyo de <strong>EL MANDATARIO. </strong></p>
    <p><strong>PARÁGRAFO: </strong>Para cada proyecto en el que <strong>EL MANDANTE</strong> requiera el apoyo de
        <strong>EL MANDATARIO</strong> se suscribirá un otrosí adicional para cada uno, en donde se fijan
        los tiempos de ejecución, actividades, pagos y demás aspectos relevantes a cada proyecto.</p>
    <p><strong>DÉCIMA SEGUNDA. - INABILIDADES E INCOMPATIBILIDADES</strong>: <strong>LAS PARTES</strong> declaran
        bajo la gravedad del juramento, la cual se entenderá cumplida con la suscripción del presente
        contrato, no estar incurso en alguna de las causales de inhabilidad e incompatibilidad para celebrar este
        contrato, de conformidad con las leyes de ambos países y normas de carácter internacional sobre la
        materia. &nbsp;</p>
    <p><strong>DÉCIMA TERCERA. &ndash; MODIFICACIONES AL CONTRATO: </strong>Toda modificación del objeto
        del presente contrato durante su ejecución deberá constar en otrosí anexo al presente
        documento que será firmado por la totalidad de <strong>LAS PARTES. </strong></p>
    <p><strong>DÉCIMA CUARTA. - SUSPENSION DEL CONTRATO</strong>: <strong>LAS PARTES</strong> de común
        acuerdo podrán suspender el contrato cuando se presenten circunstancias que así lo justifiquen,
        siempre y cuando con ello, no se cause perjuicios, ni se originen mayores costos para <strong>LAS
            PARTES</strong>. De la suspensión del contrato se dejará constancia en acta suscrita por
        <strong>LAS PARTES</strong>, la cual deberá contener la fecha de reanudación del mismo.</p>
    <p><strong>DÉCIMA QUINTA. - REQUISITOS DE PERFECCIONAMIENTO, LEGALIZACION Y EJECUCIÓN</strong>: Para su
        perfeccionamiento e inicio de su ejecución, se requiere la suscripción del presente contrato por
        <strong>LAS PARTES</strong>.</p>

    <p><strong>DECIMO SEXTA. - CLAÚSULA COMPROMISORIA: </strong>Toda controversia o diferencia relativa a este
        contrato, su interpretación, ejecución y liquidación, se resolverá en primera
        instancia de mutuo acuerdo por <strong>LAS PARTES </strong>aquí intervinientes quienes se comprometen a
        buscar todos los mecanismos tendientes a dar solución a las controversias suscitadas. De no haber acuerdo
        entre las mismas transcurridos <strong>QUINCE (15)</strong> días hábiles contados desde que una de
        las partes ponga en conocimiento de la otra la situación objeto de controversia, y si durante ese tiempo
        no se ha dado solución a la misma, las partes deberán acudir a un Centro de Conciliación
        y/o Amigable Composición autorizado para tal efecto por las normas de ambos países y a
        elección de <strong>LAS PARTES</strong> para intentar lograr un Acuerdo Conciliatorio, si luego de
        agotada dicha posibilidad persisten las diferencias y de no poder resolver las diferencias de esta manera, las
        partes quedan en libertad de someter el asunto en conocimiento de las autoridades judiciales competentes de cada
        país o autoridades de carácter internacional que tengan competencia para dirimir las diferencias
        entre ambas partes.</p>
    <p><strong>DÉCIMA SÉPTIMA: CONFIDENCIALIDAD Y MANEJO DE LA INFORMACIÓN:</strong> Debido a la
        naturaleza del contrato, se hace necesario que <strong>LAS PARTES</strong> manejen información
        confidencial y/o información sujeta a derechos de propiedad intelectual, ya sea de las empresas o
        personas firmantes, o de los clientes o usuarios finales de las aplicaciones, antes, durante y en la etapa
        posterior a la ejecución del contrato, por lo tanto, <strong>LAS PARTES</strong> reconocen expresamente
        que aceptan que la confidencialidad es fundamental dentro del marco del presente contrato, y para efecto se
        suscribe y se obligan a mantener la información en reserva o secreto, brindándole a la misma el
        carácter de estrictamente confidencial, y manteniéndola debidamente protegida, igualmente,&nbsp;
        el contenido de este contrato, los documentos y la información que sea compartida por <strong>LAS
        PARTES</strong>, con ocasión del desarrollo y ejecución del mismo, gozan de confidencialidad.
        Este compromiso mantendrá su validez y vigencia plena aun después de terminado o vencido este
        contrato. Cualquier violación de la misma podrá ser sancionada de manera penal, civil y en general
        según las normas que rigen la materia.</p>
    <p><strong>DÉCIMA OCTAVA. PROPIEDAD INTELECTUAL E INDUSTRIAL: </strong></p>
    <p>Los derechos y el manejo de cada una de las partes sobre la propiedad intelectual e industrial se
        regularán de acuerdo con los siguientes parámetros:</p>
    <ul>
        <li>De la propiedad intelectual que surja del desarrollo del presente contrato (desarrollos, software,
            códigos entre otros), el <strong>MANDATARIO</strong> será titular de los derechos morales y
            <strong>EL MANDANTE</strong>de los derechos patrimoniales, al igual que el derecho a utilizar su contenido.
        </li>
        <li>Los derechos de propiedad sobre las marcas, nombres, logos y emblemas tanto del MANDANTE como del MANDATARIO
            son de propiedad exclusiva de cada una de ellas, Por lo tanto, ninguna de las partes podrá utilizar
            la marca, el nombre, el logo y el emblema o cualquier otro signo que lo distinga sin que exista
            autorización previa de la otra parte, y su utilización no constituirá en ningún
            sentido derechos de propiedad intelectual sobre los mismos.</li>
    </ul>
    <p><strong>DÉCIMA NOVENA. - - INDEMNIDAD:</strong> De conformidad con lo dispuesto en el presente documento,
        <strong>LAS PARTES</strong> que incumplan cualquier tipo de responsabilidad civil, legal, contractual y
        extracontractual, mantendrán indemne de cualquier reclamación proveniente de terceros que tenga
        como causa sus actuaciones a <strong>LAS PARTES</strong> cumplidas.</p>
    <p><strong>VIGÉSIMA. - TERMINACION: </strong>Este contrato se dará por terminado en cualquiera de los
        siguientes casos: a) por vencimiento del plazo de ejecución si no se hubiere hecho prórroga
        automática o la terminación de sus respectivas prórrogas; b) por mutuo acuerdo, siempre que
        no se causen perjuicios a las partes; y c) por fuerza mayor o caso fortuito que haga imposible ejecutar el
        objeto contractual. d) Por incumplimiento de las obligaciones por alguna de <strong>LAS PARTES</strong>, en cuyo
        caso la terminación quedara al árbitro de <strong>LAS PARTES</strong> cumplidas.</p>
    <p>En consecuencia, se suscribe por <strong>LAS PARTES </strong>en siete ejemplares del mismo tenor y valor el
        día {{strtoupper(Carbon\Carbon::parse($ContractPdf->date_firm)->formatLocalized("%d"))}}
         de <?php
       setlocale(LC_TIME, "spanish"); $mi_fecha = strtoupper($ContractPdf->date_firm); $mi_fecha = str_replace("/", "-", $mi_fecha);
       $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));  $Mes_Anyo = strftime("%B", strtotime($Nueva_Fecha));
       echo $Mes_Anyo;
       ?> de  {{strtoupper(Carbon\Carbon::parse($ContractPdf->date_firm)->formatLocalized("%Y"))}}
        en la ciudad de Bogotá D.C y firman a continuación:</p>


    <div class="content">
    <div class="left">
            @if(!empty($ContractPdf->firm))
                <img style="width: 200px;object-fit: cover;position: relative;" src="./firmatatiana.jpg" alt="">
            @else xxxxx
            @endif
             <p><strong>TATIANA PRIETO TRIANA</strong></p>
            <p>Cédula de Ciudadanía N<strong>&deg;</strong>52981604 expedida en Bogotá D.C</p>
            <p>Representante Legal</p>
            <p><strong>IMUKO S.A.S </strong></p>
            <p><strong>EL MANDANTE </strong></p>
        </div>
        <div class="right">
            @if(!empty($ContractPdf->firm))
                    <img src="{{$ContractPdf->firm}}" style="width: 200px;object-fit: cover;position: relative;">

                <p><strong>{{strtoupper($ContractPdf->Developer->namecandidate)}}
                        {{strtoupper($ContractPdf->Developer->lastnamecandidate)}} </strong></p>

                <p>{{strtoupper($ContractPdf->type_document)}} N<strong>&deg;</strong>{{strtoupper($ContractPdf->number_document)}} expedida en
                {{ucfirst($ContractPdf->cityexpedition->name)}}</p>
                 <p> <strong>EL MANDATARIO </strong></p>
            @else xxxxx
            @endif
        </div>
    </div>
</body>

</html>
