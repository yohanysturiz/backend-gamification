<!DOCTYPE html>
<html lang="en">
<style>
    .content {
        width: 100%;
    }

    .body {
        margin-left: 55px;
        margin-right: 55px;
        margin-top: 0px;
        margin-bottom: 10px;
    }

    .left {
        padding-left: 0px;
        padding-top: 5px;
        margin-left: 0px;
        float: left;
        position: relative;
        width: 50%;
        height: auto;
    }

    .right {
        padding-top: 5px;
        padding-left: 5px;
        margin-left: 5px;
        position: relative;
        float: left;
        width: 45%;
        height: auto;
    }
</style>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style.css">
    <title>Terminacion de Contrato</title>
</head>
<body>
   <div class="body">
    <div class="logo-div">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWYAAACiCAYAAAEFL1eWAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAHbtJREFUeNpiYBgFo2AUjAK8YMHt/diEGVF4OhH/Ga6sYKTYMkrNWXD7P4ZYgircPCYsFjpQNbRAHiDHwSBHwjCaR1jIsggUijoRCkDWfXiIwvQghzC6OSCLe2oZGHKrGRjYOQSBDvqA1XykUIXzQXoX3HYAsg8wEXQcNj7EwQlEhTBMz8zLEAeDwORWBiD7PRnxth978sDlcFQP3CfZOqBDrdzMGP5fXg7G+DIaTgCNASay0+qVFQ1EeRYpiRzbdQpdVSBRGRGNz4LFsgNAi6iT0SBpHyJf0gxKEgyMupEQufhsBow0veC2AU6HI6VzRgZ6A4hjLgCxAYpjIA4+D8QfgGKC+Iygv6Pxl8cEHUxZmqYeEISHNhEOHiyOJrkkGmhHGwKxANZqexSMAvIAQACNolEwCkYB3nZJAhDPx99g0ok4D255UdqxxdbtIs2x5+EtQCI6tgZUDy2diASSO7YQdxTi69gy4QwtnYgCKP0fSew/mpr7ODsDOhEgy+fDQ3zB7fcMQU0QM3C1NRA98QloMhuIbTD1o3SZsHSfgEABj/7z0MY+BPTUCjDcugZjM5DUSEpQDSTW0Y0Ee+fERTXcgSidWljPnIzOAhOV0i62UFPEq8fGGfd4B7IjF9wOgLISiXH0AwJOVcTbsb2y4gFcHuoYUKcW3rG1cAjEG6KQwRkQfz20G7ZgYPqIeHrYKMNhBMDAdWwJDDIO7j4ikQ4dLI5eQK7GwTPuQUaIj4JRgAUABNAoGgWjYBSMglEw4sHQaFTpRIAmdR3I7FfTts+CAHgn5YhZf6AI754OHHAYwMBtAJL1aKKBwEDdAJUHLUkQYECe6sLSK2AhwioFnOMIOhEgR9gDI8IRTVwA2uc/iHXGH7LmwgEotwCnuYRWCkDG284jjVUwYgkk0HBrApRnCAyACyQGMiwQQeACUL8hls6tIEaKB9Fogc1IRIp2hK5IoGxiEjEGiJpC0AMI2R7s44agEab5KHzkCAN58twJBoZ9W7G7A7QKAjKl/YEmfVcceljIzVBAzyXiDRx0OWotQEMOZHTzYEujoAA+xglKUTYpDAwfv0LGO0ua39OhfjqAzCF3iObhQI6RoESiTgTWZVu7F6AWq/+PzEFwIKPixKyaKiSyIgTJC6ClZkdSy2jqAlDZqxNRj6OYIkZ/Iri4QOgTQMotigwcHPcZfvxgcE2ATASU5oYydG84yMDw+BVq8YEWEMSkSqKnLLAUM7hStCC8yQIqn9GzKWZFdYGIAGJEYwti2AkRh5WdE7C45wCKGQgzD0A9+IDhzAJGcEAKQLR0T16NCGSQOKSMJlTOGkAD9TxS042RAd9AKfoM3JBsR9OvLUxS23j4dVgGIsCpPELOMuIDGXVKbT+tOkejKZpOKXp0Agk1wBuBAdzAMApGwSggAAACsHf2Og3DQBw3FQMSA+0AYkGChQGxdGOjfQCksiBG2EEqAxMLzAzwBsDWBZWJDQkkHiBDxcKCxMYCI0zQi21yThzXdpIWmvuJCuWjNP3nYp/tu4N+CIIgCIIgCIIgxhWaLPKBhycs9l9XODXif4scrfnBEtbHiITFcRtJDFOlpgQxeJ39AYHx6vP7iAT+Ngosz+FphBYi82AVSZuaBm0RC7mgGs+6ggXaMztL/i/E0y+LFpiLW0PbN0LsA5NhVjJ8wWr4KKuWHz0N6mNuelrix3ZEfJ1tO83C3NekQNXf/D3XMkGOba7Ifw3Sbs6kk9XoaYQBLbxKT1VpN00VquR71M+IOpfV7Xr/eGB1Lb3OecICvz55ASlgeaXhkoso/kYDbd1b3IR62jpiJddHN61jAuu0A3cuLUuB9bFzUmAAkpth31PgEpPRRQI2s0jjK/JHLMIHUxf7cVL1RSFt5oDgxAS31yy0cKh84HbD2fBF7nVqhmOB+H080k5xZvo30x1HggoLt7np96MVuaCL8WiiWkpHh0NscbRnH6iZ59jRNQ1u3NBFHjaBtt00F6Fg7Y31rO7c4MFIxMnfEXl1u+vRVNW1HSGkN+jqSAi29k99rnBTGW1G9ScG+dPHfi5cfuD0hZbXYAIXdZTNRq9zw9b6XtfjHZ+UgVoYC3PssLXOw2slU1OhLM4ehty+fHb2p3WW/FKoxGkJPOk8pOw/SIgBXxAqZ0pe31SBgb0jOG/XYzjtPWCpGEXQu2i7sfObhvNrVq6XziWU3gn2UtTj50wGlOP9s/NL2kBwCBqX+/mMmutw2jSIuTQdH9/5ZDUljfvvOI0tLc3MVMHGk3JN2kf1a7PPV4y5C5dlkidINHcFC1w+kbl4Nh3vZp4fWc4UhzSLjdrjbp5NaYURhUMiq95IIUyUVFDvlWeyZPt2uGYcWJAl52rRDSb+V0kRFkwQBEEQpeRHAPaumLeJGAqbf3D9BaRDJQRIFPgDqQQLS1N1YbxMLCCoxMZAKzaWMLA3sKMWMTCwtAsLEs1Clw4Jv6D5B1Cffcld/Pzsu3PSS+771FZtLnF958/287Pf9/AFAAAAAAAAAAAAAAAAAAAAAAAAAKsEnKcJCRXeL/UMBkmsVpOhoiLl2S3fBO3y+P6WV8qeIGRWcbgv9V/dGmT0qguJW1c/h+S160zptngCy44cSnNpTwuNBCYzGsv1fOzZIJrwfJQI1uacSqfDBQn4xu/ETEO2JxmQgDIjuoy2kIG7O0tI4rxqkgn/pEX2IIk005QzayBCeK6HxLHWQxjqEb2jhdriJSJyiyFy1xCCcw6XG2s6JsIW9nqpY48rj8xFGooKdh5PRI6yGmZUT6bEkMp8Jv95cxr0nf5N8Y6BoYJS7dlk69R3EKgtsgExDrp56VOUx9BCympmlQov7lt0M84403geYcJUY0Weii7Z990XfgoAESkKOCXQPmnP+eSDpeu86ZXx2PRycNMxnx47bdisAKELL97EGSm34tmU+U5lC0RdC8ijNfKZ9S+Oru5lZ1FknmencI+idVtw8Z04n5SZajhpgkgxR6k1WAQp6Z/sCnF78yyRWwoX2hhbRtVwctiyLFoiqrPIkVkw0+iNEg2+Z8ikch6Eu08PWYLUA27TKCUyI6a5vdsWz7VK3sdvp+LrF2IdLjuB/JaqTUQO+lXCIheAB8y1E6YDfLC8vlXY81IfRLas6bkR6PQ7eSEVLj3efyYePbyVfMvfDUHTLNKylC+4KrqWDhhO199ezz14M+pJ6H/MQk+IXz+NSz/6b50Fk++ZlrUdwASwLVRfBeks3OYLs5HSBDKX21ZWi7YwphVnxyuXXGemwdRMpWRJc3j8/rPzX5LvmZZ1Gsimtd1TT6chiEqQONKL3V4ZT8nqk9mWvmBKpJ7h/eAU/qutF2wj2hFhdowS+ddZnA+VJq8FybVzwmuWlhU2yzeXLeAyk1+jwxC4M3mf/ZmPfWz9Zij4zYo2z06NVLKT+dSjm9ibdF2inDcm3lhPdsVev4uoRSBHaAOpZG+oxV9R4VQf8eriM0DJkZnzqfpuZfN+2U+W10clbn/EjIxd7zJ4F9/AczE6YOoydi6S1Q7a/YSMWRFvX8jPUJrUVUisRtAz6wjKL/T9nQVu7Wuzgzd6CSbPlaSNwJkjdUH/QtoPLWVuXFX3z28h/mqT4ua6EHceSJ+yq5RxoW1mv5HYXqba9pYr0thWuibvqOrjwYm3VYWbgAOdYqwnzGR5x3oWa5Um8TUAZAapy6BWJAaZm0dqOc0friKJm+XNACTuMdfyZ4XtZ4ujOt8gdgCbA9r9qLwG45nX5Oh7smw3CDIDNrSXrcIwM5oD2/ngdAPnxIPEB3W+QSwAm7cQLJuBemtyZgRkBmpEaFfky1KRGAAAAAAAAAAAAGgY/gvA3hmEOFJEYbhGFhwQ1slhQbyYQRQRkWS9eNuJ7G0PTpAFYZVJFBFE2AniSSQT0JMis4wehIHp8eKgLJM96GEFJ4KHPTlRRw8OuHMQEfYwzZ72Fvt1dSbppKq6qrs66aT/jw2zSXdVdXf9/erV6+oq/AMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACyCWY0AunD1zWkz6WhX2kJt47N2ZIgZpvw9fxopnpHubB7PgRcY2aTmzdUC1ZCzJMTsWjutq4n6HIORSxfXVWPVty1ChcMK43utGJQUQ2o+Oy6yGbWrHvXycmRkHvW8oqxZuGCZmWJZ41Ur5UHMfMl2Qo5ELHJrKImFMZm9VegO3P+qaQS96FkJUsQciJOTdbgTroMxCr0mnt0hOwEVnZhaOVVarE2LeXvg2UgpuueUEeJoh+7M+lb8xVjVdQ94ToSn9j1Ixj0iYp80OpXGku26frMcscefnO86zOeZnOmOtV8GeG7NjtwER3IspdnN003A9gT//qMnYVdIUeni1xl9tyUKpN87bWhDtINzyq1I9LUgjQ8SsDTdAzKXA/KIwvoGqRbCoRGkYlNC+eevLPEm+XrgYvCQtckZow2RqdPHoFIAglaZqGpXEV041wKQhWt7ln2hNANBCnyjVa8bdzHGvUdufD3hZ1PnqaqvBHGw4pN7zd6QlfXOBc61tqIj1swuhmir02floaIyBoWFZGTprdP80xUBmEtQ3akHT07ZTqh6x4utzoZn1ku1n5lNbVOpC+05149HLE+0Wl0j13H19dNq3N91EJWPy10jsM39J+e6/jjd4w9eDDYZ3GRsZeuMPZs6HLR2IdKCpa5Z9W9sFSGbZ+5qNjW1Myj5lX8SlD5JYM0JaMjpTKSbDcr665CyG6EkGtnQr7t/fn0Q8a+vxkWMkHf6Xfafnt/0OLxdbBzQVY7gAfM/IHDYSbPhAu5qLDchQiLzFs6Eulvv+iVSfvR/n33wzmexLVxLOe3OS9ijiucWgaPqqjloonZPxNyHAbpSsEwzDT51XJ+P2VdzBW/AvsfvbWY3VAatQB2ZujWK2h09hj7/ONkpWx/NtzapUnXcn5ulsXcGAulHe1tsMEC5PrN8Ow/qClrRESK7L477hv3efQR9s3W+6z3+9f+X/ouloTnMt93h92WtFiZdn6TE7MsRnu0V7HpN80IzQirzN2lr74Qbv7BabLez9vs6spF/zv9pe++qEUM8kmz5bpuOb+1efOZbym2dVKPQqTHahD/VgtDYJXfrF1hl194RpiIRP3yK4LTH+ST5ig+23kX890BnC1UITNpmHH7vdeUmbY33p7EsZ8oW5TEcRGpO+RCzFlFNR78nxO7Zd37z2Zu1ZQ74LLrUoeY43UwbLkrhVjuxsOLds/WZn6q0WtJY9qq9LWn2hBzvA6GnQ4Nj1q0DN0Nl114zO7ZnrfuLrekLpJzvB9TyDsKFysyjJsHMXeMOxh8gJG92uchSNfA3bgh2/XJtz5SFhW13aJ13lC2OKaP0fn+tZjl5UbMDYWIeqFmnv7Px4TUrB+F+sXWsLvRr7il8SR/3/mDrW58Kc7E+522j8cZCtHXIo3oBg0WirLStJ0PKko8rHT+X5viQ0/VUQX1dptQTP1A4W6EHwa9/g5jW+NPAG/d7LAF70NhuKcfv8D++vee/5sUyoffJPbi9vqWdzXhFAR13WGlefGZ6xm5sUhxXUVLUQpZI+q0PbEs3Z0E/MnWt2ohU3re+XMsC3kSb5470ncIcytmPuDfnULJHcGxlJVN88DVcP30V9/gY5XjQOkoPc+vPmNCrpoes66YNxXNps5+JwksmYxdozRmk7HQeZlMrdWWlFkx8gFHj50PrHfZux+YC5r2p3QDEZYmIOSyFaPBpyNomyZ7SFNUDUGF1QUDh1xhc3a0txxRQsHYNeDWtmuYZiGiuW0FI/M6vq89Hg5yhYOcjvaqght5WXEcLht/8FCQVGzBP08SZvWapk27FhYy5zCRoHWETPFnfrzLMUtZTvK2Sn6nCeDRg5JGC5AN+Hhk3nmkUXB3DsKD9Z+/yNiLFZ14cuQr+7GFLE5LkaHRl2+HW+yWiV8MMc8Tg7dPZOKiVnSNqV8503/ZNYmQJwzEPL+ijxIhuQPF4MYYnbKg4VvLGRIyxAxBJyFTQoaYIei5ETLEnB9B9+ZdyBAzBD03QiYwOD8/dBLnkGEhQ8z5YkXye3tkEvCqwrovQcwgq7iegKsj1pee9MoGwq9DzGDa/rLMKlcl7sSGZP9LEDMAEDOwgnxJ36ahJUcHEGS4UzgqXN7Jk70Ns5vlk0GcOT9+c9STQLLeRaZ60dfmZOIpgKXT8gONMT5VWmk1rayfICxzvqzzAYs7uU3GrTLEDHdjboSMDmAe4a816U45cDIrQoZlhpWmCVpWhSLmg4pcXCQAAAAAAAAAAAAAAAAAAAAAwHT4X4D2zic0sqSO45VlwEVQElBQEKaDjC5j0GRWYfEyHXFXdz1Mx2UhXkzn4kWXmRxWWESSnBZWYTPMycOSHkE2KEM6B10dZdNzGQTZmahxWBw0vTCgIphGQcbTWL+u6ul0p/+8qlevu7r782HfzDLJe1WvXtW3fvWrX1XxHwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBnpigCABhJSvdz+s+Cvi7pK5/iSRV97dX/Lp47QJgBOjG3vKH/vKyv6U7NUV9r6nCnRkFNlAhLXbjSo16ERurXVX1tabEeeF1DmCEmQX7DNr4kHGhxXqDQxlqM5/Wf2/qajyA3YkmvDsqinsqgcUlvtttnaLGpG9UGNQ/arOR1x7vKuh4tUXhjZxlLB12MOZf1UVuGlvRU4Ma17Vigi7phVaiNoOvOvvLxEx7uMOobH0Hej8Q6drGiF7MQ6DMBG1bRo5fbV7hTACZdlGWEXQj81Kq92snZKwTSiRzr/Muk4WKcwuz7snPLeaxmgIkU5Lw1ztIK8KYWxlKKfIhBuaL8Izvy+hmPrPUcRMvOUDtg4jCju+0uP5WGtUTUR+ai7DLR205ZmYm4MN/IiHrJ5mva1g0fC35f3y9RHGsIM0AYQW5aPzI8nVue1eJcpdAyEWXXuagGq6ks42QiXat3zE1LetvxCVfq0SQpXRsIMyDIXZqovjYyFiixzk5OdtViWeAQmSiXdLmsDjyvDUvaPc/5+j0p8nxmRBrWtC2YboHltXoBdhp+zi3nlAnDSlqwNTtUclvEYPL4hmM6JoA9xLC5d/omrSxCFHsvBjFDzmG5BeaW55XxYfosSCgFEqK8LZ9Cwt/v9K9VW5ZXdWMfXSveWKCuohzMb5tCoFd13q8rN394Ud9zy9fCnwrcQNc97uweMmfE5sihYYkAzNaFwM9K6txAD3dWe7x3Gl/ZSUSg1zzL3mVWWzqcrQDf+4rtCJJQ0Wku9nleuHC5tILc63sns4L7+yj/91Cpf/799L9/9GNKfeDJJPVcJry21ChhJshcmI2qIzJLwI8cRd1LY2MXZp9n1lQ2Szabi2KMFb6rsom5FAuz5FDuLgLZYEGncZDiW+eV+2x670VFIYQ5nSAf2LpY82y0nQ2Bf+vH/VZn6Q93/GvEZy8o9Yzu1z483S3fq9G7QEr3Xdty9v7kkN+5V70vnnMeqY6jjzmrdfTruuGv2OdnuVZ/W6dz2WG58XREZTQczMjKd3FCzQryQbCG+pf3lHr7hlIPH4Z5PxH1hrA/qa3p519U6pNPNX4q73xX56NW73DjdXVcdOokYxRlYwGLz/myQ1276JMMk39u5AaUzrwWmyMtFrMUeV9RfpTibrfRyWmXRaub7eZuOss4CSL2uz9pWtLPLZ3sbI9ChWtlgMtoaC/yWrfnIMx5nwSemICmW7IW0ZQd9s7WG2TnVUExpZWzw3vI4juZb+QrytLYjh+L8gP9eX/4/exFuZMlLek+aKleEq51NOLfpzLp+RtXi7n78NTEppZUY9bdz0c7qLTy9UlMXwGB00PkNH7kpig3O8xbbyv1u9vDfaudN5X6whf1oPn5Zqcu4lw8N6ojrnzk4pz5yHkchbnq5AKQCIW5ZRHQ3QGldaDcJs5WVKjQrcnGLBxIJ8rTLd/Ow3XxyssvqVe/8VU186EPdv2d4//8V7321i/VD679LPmDG51DqzhvDyX+t1tbSS5on4u8Ll1yrHe4MjTXne843Cl79tA+aVUc08rbyS1Ih/HBzi2n2SynOdqRCT4HUf7ptVfUoz++pV7/1td7irIgP5ffk9+X+5zEWfLVpGj3NI6BssPvFiLKd3vnnFNuy7XLPsk8QXtNxaCGW/MUdTB2bdy3D8XH/3f7ncQ3/ev2m+ql/AWvBOW+v968lvyGO7dPi1wcXHX+TnGyn/F7I8wwsRS0OB87jUSMb7nJP/6W6LbXXi32tZD7Mfvxj9RdIIl4/9S838UoStyE8bksiMlFN4lp8pNzucM3vhxhntxh/ThR9SyDYy/XxoPkyX3+02eDvOBzT5/PJH8DFuc1x28l4nw8dLeGuC8kH26iXE3j30eYR5+Kxz2XUqa5Es3bm7A3mYD1Pf8vmWvj5H4Nn0jePm++ey/Iazo9pzV/sam0fCeXCTHpQGUBzd0hibLUDZdtIZR9v1TnUSLMo47fIQNFu3zZHSNixQjL4cDGjvuUR1LXRvPZZ5MF40hkxW/efS/Vq8n9iSM0TucrrsUaZlvNWY8OY76+14YItJmAy1KMp206j5S7j968X8q9ohHm8WDT4567dqOnJGJsFruYVXaFqEvCbJbkc0BrEtdGc0Vd/oXED362uKkKGz/yeh25T+5PTGu+ZGlzObpvJKJlYqx9OtF5ZVY4ikjvBxNp467Yt2J8rPwm3OWIqZkQG/izJHs8rOYNu4+HayXdtgfo1mwj+b1qxptKLGlejaI/WsIf55Zn6p2Pe5mIa6Pz6dsykVO6Lyq5Xt8F7ktfU+qdnyd66N6NiprSlzo/q3793W+qLz/9VE8L+dnXf6zUPce5L8mP5KtJnCeIG5/xfoC6lbcifdJaLdt6fGA7plqLJWwEd97W70LA+h10e1KEeXxYUO6+sJPWYiF6a9hNnM2Q0m+HwkLXvUpkp7DS/bN1d86FZ8ymQr+4kfzJWmydLOCkvPCiFv0WI282ug2Nwglyr3pcbEsz87fKYhFPDK6M8T6xYZBCdLgzo+LfZyAktQTlsqGMT9N1eJmz25t2GoqvPnZriBi+/D0j0MNA0pX0m6Is7zkT2T7G8zaq4a4an4igtfpeyxmtrAwpzCWPe8p9lsj6VK5KZo18kILiWw7+PlaX8i1lUtGzukf2LDGdVtnpG/WaWDWb1M/UDQvZ2P47WhxXvj04gZZ0JD1Jt3Vj/bVgh5QiyJ2MyAUryJkeUjAV9Glu++Im23LRbSi6ZJdX++bfZbP2QaW16Bl50djQfjdlozCnZbSfemJ800WHDngpYP3x30PZTO7tJmiAyTc7ah+iy+kkv9JJ/Ple+Bb7qfNKfWWp3yknpnyGtXl+epdFa/7NBJ/L8XBhXRVms/uBjkCmMnuyEYVLJxqZFPYt1e1svmQNSia4CqcavTnPrhIw7xttec86rZMTd42JuM1Up4ycTkee3zh7LtdHlK57f6ew+W2fnCkHKxPTCbSXRbrnm8klEZDWo8bu6Uf+6U6nVXn9kfC3z1xo9x/7CdyoCXLvci7adhNy8Umj7peH7QqaUgCQlVAVrVDnhpyTILG1Qxdkt/w08nJyVFp5nF7kR3EhzACDH+bnrXB02sdCLLX3rfVW6RDu5XuEVtNF002gWwWteU/v349HkMcIhBlgNMU9jSA2hND3jERx6aWJAUaQEWYABDoSEGSEGWBiBDpJpAmCjDADwBAEuqhkmT2CjDADQHQCLUdgXRlyLhYQZH/YXQ5gvER53lOUxbrdqlu4ZkWj/C3LjSueOVnnY2AxA4ARZpfVq4IsplhK8FwRfNfN6pei3HYUixkABmwtu4jyV
        iJRFoxbYsYxRyt8FIQZYNJxDZtzO93ELDQpZZgfQJgBJp6cxz15ig1hBoDkFm1FuW2Vu21D7PrTOAfPTcz3+Ch+MPkHME74xzJLRMbmqX0xSvfFQpbwO9fl21V7rh8gzACgxXRDDT9cbSaaDftHEFwZAOOGnEvodyJMCKqIMhYzAHS3nNNuE+rKWtZHLiHMAIBAI8gIMwBkLtJ5ZfzP+RRPETfFVWUWqOCyQJgBICOxluusOh0OJ+d0VpU5TaVKYQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDk8X9kxNVOrt/0cwAAAABJRU5ErkJggg=="
            style="width: 200px;object-fit: cover;position: relative;">
    </div>
    <p>Bogotá D.C
    <?php
       setlocale(LC_TIME, "spanish");
       $mi_fecha =  Carbon\Carbon::now();
       $mi_fecha = str_replace("/", "-", $mi_fecha);
       $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
       $Mes_Anyo = strftime("%d de %B de %Y", strtotime($Nueva_Fecha));
       echo $Mes_Anyo;
    ?>
    </p>
    <p>Señor  {{strtoupper($ContractPdf->Developer->namecandidate)}} {{strtoupper($ContractPdf->Developer->lastnamecandidate)}} <strong>CONTRATISTA</strong></p>
    <p>Ciudad</p>
    <p>&nbsp;</p>
    <h3 style="text-align: justify;">TERMINACIÓN CONTRATO DE PRESTACIÓN DE SERVICIOS PROFESIONALES INDEPENDIENTES DE FECHA
    <?php
       setlocale(LC_TIME, "spanish");
       $mi_fecha = strtoupper($ContractPdf->end_date);
       $mi_fecha = str_replace("/", "-", $mi_fecha);
       $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
       $Mes_Anyo = strftime("%d de %B de %Y", strtotime($Nueva_Fecha));
       echo $Mes_Anyo;
    ?> </h3>
    <p><strong>&nbsp;</strong></p>
    <p style="text-align: justify;">Cordial Saludo;</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">Por medio del presente, de acuerdo a lo convenido por las partes en el
        <strong>CONTRATO DE PRESTACIÓN DE SERVICIOS PROFESIONALES INDEPENDIENTES </strong>de fecha
      <?php
       setlocale(LC_TIME, "spanish");
       $mi_fecha = strtoupper($ContractPdf->end_date);
       $mi_fecha = str_replace("/", "-", $mi_fecha);
       $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
       $Mes_Anyo = strftime("%d de %B de %Y", strtotime($Nueva_Fecha));
       echo $Mes_Anyo;
    ?>, teniendo en cuenta la baja carga laboral presentada por IMUKO SAS en este momento y luego de
        discutir la situación con el CONTRATISTA, esta empresa ha decidido de manera de mutuo acuerdo,
        <strong>TERMINAR </strong>su contrato de prestación de servicios a partir de la fecha, por las razones
        aquí indicadas y dentro del periodo de prueba pactado en el respectivo contrato.</p>
    <p style="text-align: justify;">Por consiguiente, el contrato quedará finalizado a partir de la fecha y la
        empresa procederá al pago de sus servicios prestados desde su inicio hasta este momento y adicionalmente
        le solicita la entrega de información y asuntos a su cargo.</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">La empresa agradece a usted por la prestación de sus servicios profesionales
    </p>

    <p style="text-align: justify;">Sin otro particular me suscribo de usted.</p>
    <p style="text-align: justify;">Cordialmente</p>
     <br>
    <img src="firmatatiana.jpg" width="285">
    <p style="margi
    <img src="firmatatiana.jpg" margin-block-end="-53px;" width="285">
    <p><strong>___________________________________</strong></p>
     <p>Resentante Legal&nbsp;<strong>IMUKO S.A.S</strong></p>
            <p><strong>NIT </strong>901250358 - 1</p>

    </div>
</body>

</html>

