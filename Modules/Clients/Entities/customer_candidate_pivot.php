<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\Contract;

class customer_candidate_pivot extends Model
{
   protected $fillable = [
    'customer_id',
    'developer_id',
    'created_at',
    'updated_at',
    'status'
   ];

   protected $table = "customer_candidate_pivot";

}
