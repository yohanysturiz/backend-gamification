<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\Customer;
use Modules\GeneralData\Entities\City\CityModel;

class ContractLogs extends Model
{
    protected $table = "contract_logs";

    protected $fillable = [
        'uuid',
        'user_id',
        'contract_id',
        'version',
        'old_data',
    ];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->uuid = Uuid::uuid4();
        });
    }
}
