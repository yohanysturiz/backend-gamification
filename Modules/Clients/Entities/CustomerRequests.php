<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Developer\Entities\Developer;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;
use Modules\GeneralData\Entities\City\CityModel;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Modules\GeneralData\Entities\Country\Country;

class CustomerRequests extends Model
{
    protected $fillable = ['language_id', 'language_level_id', 'timezone_id', 'uuid', 'first_name', 'last_name', 'email', 'phone', 'description', 'status', 'experience_years', 'contract_type', 'city_id', 'remote', 'date_end', 'site'];

    protected $table = "customer_requests";

    protected $casts = [
        'created_at' => 'date:Y-m-d',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->uuid = Uuid::uuid4();
            $query->end_date = Carbon::now()->addDays(15);
        });
    }

    //Retonar los candidatos seleccionados para esta solicitud
    public function developers()
    {
        return $this->belongsToMany(Developer::class, 'customer_request_developer', 'customer_id', 'developer_id');
    }

    public function lenguages()
    {
        return $this->belongsToMany(ProgrammingLenguagueModel::class);
    }

    public function city()
    {
        return $this->belongsTo(CityModel::class, 'city_id');
    }

}
