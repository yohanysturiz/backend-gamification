<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Ramsey\Uuid\Uuid;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\Contract;
use Modules\GeneralData\Entities\City\CityModel;
use Modules\User\Entities\User;

class Customer extends Model
{
    protected $fillable = [
        'uuid',
        'user_id',
        'code_company',
        'company_name',
        'type_company',
        'rut',
        'responsable',
        'type_document_representative_legal',
        'number_document_representative_legal',
        'city_expedition_name_representative_legal',
        'country_representative_legal',
        'city_resident_name_representative_legal',
        'email_legal_representative',
        'phone',
        'email_company',
        'city_id',
        'foundation_date',
        'description'
    ];


    protected $table = "customer";

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->uuid = Uuid::uuid4();
        });
    }


    /**
     * Relations
     *
     */

    public function developers()
    {
        return $this->belongsToMany(Developer::class, 'customer_candidate_pivot')->withPivot('developer_id', 'status')->withTimestamps();
    }

    public function emails(): BelongsToMany
    {
        return $this->belongsToMany(CustomerEmail::class);
    }

    public function Contract()
    {
        return $this->hasMany(Contract::class, 'client_id');
    }

    public function city()
    {
        return $this->belongsTo(CityModel::class, 'city_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeSearch($query, $consult)
    {
        return $query->where("company_name", "LIKE", "%" . $consult . "%")
            ->orWhere("rut", "LIKE", "%" . $consult . "%");

    }
}

