<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\GeneralData\Entities\City\CityModel;
use Modules\GeneralData\Entities\Language;
use Modules\GeneralData\Entities\LanguageLevel;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;

class CustomerRequestLanguage extends Model
{
    protected $fillable = ['language_id', 'language_level_id'];
    protected $table = "customer_request_languages";

    /**
     * @return BelongsToMany
     */
    public function languages(): BelongsToMany
    {
        return $this->belongsTo(Language::class);
    }

    public function languageLevels(): BelongsTo
    {
        return $this->belongsTo(LanguageLevel::class);
    }

}
