<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Developer\Entities\Developer;

class CustomerEmail extends Model
{
    protected $fillable = ['customer_id', 'email'];
    protected $table = "customer_emails";

    public function customer(): BelongsToMany
    {
        return $this->belongsToMany(Customer::class);
    }
}
