<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\Customer;
use Modules\GeneralData\Entities\City\CityModel;
use Illuminate\Notifications\Notifiable;

class Contract extends Model
{

    use Notifiable;

    protected $table = "contract";

    protected $fillable = [
        'uuid',
        'client_id',
        'user_id',
        'hash',
        'type_contract',
        'contract_duration',
        'candidate_signed',
        'contract_value',
        'contract_value_number',
        'start_date',
        'end_date',
        'position',
        'city_id',
        'type_document',
        'number_document',
        'programming_lenguage',
        'city_residence',
        'type',
        'date_firm',
        'content_contract'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->uuid = Uuid::uuid4();
        });
    }

    // Scope
    public function scopeFiltertag($query, $term){
        if($term != null) {
            $query->where(function ($query) use ($term) {
                $query = $query->orWhere('namecandidate','like',"%$term%");
                $query = $query->orWhere('lastnamecandidate','like',"%$term%");
                $query = $query->orWhere('emailcandidate','like',"%$term%");
            });
        }
    }


    public function Customer()
    {

        return $this->belongsTo('Modules\Clients\Entities\Customer', 'client_id');
    }
    public function Developer()
    {

        return $this->belongsTo('Modules\Developer\Entities\Developer', 'user_id');
    }

    public function Cityresidence()
    {

        return $this->belongsTo(CityModel::class, 'residencecity');
    }

    public function Cityexpedition()
    {

        return $this->belongsTo(CityModel::class, 'city_id');
    }


    public function routeNotificationForSlack($notification)
    {
        return config('logging.channels.slack.url');
    }

}
