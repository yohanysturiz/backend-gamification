<?php

namespace Modules\Clients\Entities;
use Ramsey\Uuid\Uuid;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\Contract;

use Illuminate\Database\Eloquent\Model;

class customer_requests_programming_lenguague_model extends Model
{
    protected $fillable = ['customer_requests_id','programming_lenguague_model_id'];


    protected $table = "customer_requests_programming_lenguague_model";

}
