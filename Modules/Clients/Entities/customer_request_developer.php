<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Developer\Entities\Developer;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;
use Modules\GeneralData\Entities\City\CityModel;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Modules\GeneralData\Entities\Country\Country;



class customer_request_developer extends Model
{
    protected $table = "customer_request_developer";
}
