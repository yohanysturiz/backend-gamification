<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer', function (Blueprint $table) {
            $table->string('type_document_representative_legal')->nullable()->after('responsable');
            $table->string('number_document_representative_legal')->nullable()->after('type_document_representative_legal');
            $table->string('city_expedition_name_representative_legal')->nullable()->after('number_document_representative_legal');
            $table->string('country_representative_legal')->nullable()->after('city_expedition_name_representative_legal');
            $table->string('city_resident_name_representative_legal')->nullable()->after('country_representative_legal');
            $table->string('email_legal_representative')->nullable()->after('city_resident_name_representative_legal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
