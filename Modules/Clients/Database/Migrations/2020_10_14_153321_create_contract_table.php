<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique();
            $table->integer('client_id');
            $table->integer('user_id');
            $table->string('contract_value', 100)->nullable();     /* Valor del contrato  */
            $table->string('contract_value_number',20)->nullable();     /* Valor del contrato numero */
            $table->datetime('start_date')->nullable();  /* fecha inicial del contrato  */
            $table->datetime('end_date')->nullable();     /* fecha final del contrato  */
            $table->string('position')->nullable();   /* cargo  */
            $table->enum('status', ['pendiente', 'aprobado','vencido','cancelado'])->nullable()->default('pendiente'); /*Status*/
            $table->integer("city_id")->nullable();
            $table->integer('residencecity')->nullable();   /* Ciudad recidencia  */
            $table->integer("company_id")->nullable();
            $table->string("type_document")->nullable();      /* Tipo de documento */
            $table->string('number_document',20)->nullable();     /* document*/
            $table->string('type_contract',40)->nullable();     /* Tipo contrato*/
            $table->string('contractduration')->nullable();   /* Duración de Contrato*/
            $table->string('lenguaje_programación',350)->nullable();   /* Lenguaje de programción*/
            $table->timestamps();

            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract');
    }
}
