<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique();
            $table->integer('user_id');
            $table->string('code_company', 100);
            $table->string('company_name', 100);
            $table->string('type_company', 100)->nullable();
            $table->string('rut', 100)->nullable();
            $table->string('responsable', 100)->nullable();
            $table->string('phone', 100)->nullable();
            $table->string('email_company', 100)->nullable();
            $table->integer('city_id')->nullable();
            $table->date('foundation_date')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();

            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
