<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsCustomersRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_requests', function (Blueprint $table) {
            $table->unsignedBigInteger('language_id')->nullable()->after("id");
            $table->unsignedBigInteger('language_level_id')->nullable()->after("id");
            $table->unsignedBigInteger('timezone_id')->nullable()->after("id");
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('language_level_id')->references('id')->on('language_levels');
            $table->foreign('timezone_id')->references('id')->on('timezones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_requests', function (Blueprint $table) {
            $table->dropColumn('language_id');
            $table->dropColumn('language_level_id');
            $table->dropColumn('timezone_id');
        });
    }
}
