<?php

namespace Modules\Clients\Events;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\Contract;

class Datefinished
{
    use SerializesModels;

   /**
     * @var Contract
     */
    public $contract;

    /**
     * @var Request
     */
    public $request;

    /**
     * Create a new event instance.
     *
     * @param Contract $developer
     * @param Request $request
     */
    public function __construct()
    {

    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
