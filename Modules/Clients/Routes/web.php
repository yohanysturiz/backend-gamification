<?php
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Model\SendEmail;
use SendinBlue\Client\Model\SendSmtpEmail;
use GuzzleHttp\Client;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('clients')->group(function() {
    Route::get('/', 'CustomerRequestController@index');
});

Route::prefix('export/contracts')->group(function() {
    Route::get('/', 'CustomerController@exportContracts');
});

Route::prefix('export/contracts/clients')->group(function() {
    Route::get('/', 'CustomerController@exportContractsClient');
});





