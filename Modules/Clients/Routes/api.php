<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Crear Solicitud
Route::post('clients', 'CustomerRequestController@store');
Route::post('clients/create', 'CustomerController@store');

Route::post('update-user/{email}/{password}', 'CustomerRequestController@updateuser');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('clients', 'CustomerRequestController@index');
    Route::get('clients/latest', 'CustomerRequestController@getLastProcess');
    Route::post('clients/{id}', 'CustomerRequestController@processRequest');
    Route::post('client/assing-developer/{id}', 'CustomerController@AsignDeveloper');
    Route::post('clients/{id}/developers', 'CustomerRequestController@saveDevelopersRequest');
    Route::delete('clients/{id}/developers/', 'CustomerRequestController@saveDevelopersRequest');
    Route::get('clients/get-requests/{email}/', 'CustomerRequestController@getClientRequests');
    Route::post('client/add-email/{id}', 'CustomerController@addEmail');
    Route::get('client/emails/{id}', 'CustomerController@getCustomerEmails');
    Route::get('client/delete-email/{id}', 'CustomerController@getCustomerEmailDelete');

    // Contract
    Route::post('contrac-update-date/{id}', 'CustomerController@ContratorUpdatedate');
    Route::post('signature-contract/{id}', 'CustomerController@signatureContract');
    Route::get('descargarpdf-contrat-profile/{id}', 'CustomerController@descargarpdfcontract');
    Route::get('clients/get-contracts/{email}/', 'CustomerController@getClientContracts');

    // Customer Profile
    Route::get('customer/profile/{uuid}', 'CustomerController@profile');

    //change password
    Route::post('changepassword/{id}', 'CustomerController@changepassword');

    // Client
    Route::put('clients/update/{uuid_client}', 'CustomerController@updateClient');

    Route::get('contract', 'CustomerController@tablaIndex');
});

/* Route::group(['middleware' => ['client:api']], function () {
  Route::get('descargarpdf-contrat/{id}', 'CustomerController@descargarpdfcontract');
}); */

Route::group(['middleware' => ['analyst:api']], function () {
    /*Modulo Clients*/
    Route::post('client/create', 'CustomerController@store');
    Route::get('customers', 'CustomerController@index');
    Route::get('customers/{id}', 'CustomerController@show');
    Route::post('delete/contract-client/{id}', 'CustomerController@deleteContractClient');
    Route::post('clients-update/{id}', 'CustomerController@updateClient');
    Route::post('delete-clients/{id}', 'CustomerController@deleteClient');

    /*Contract */
    Route::get('overcome-contract', 'CustomerController@tablaIndexOverCome');
    Route::post('contrac-update/{id}', 'CustomerController@updateContrator');
    Route::get('contract-pdf/{id}', 'CustomerController@generateContratorPDF');
    Route::get('descargarpdf-contrat/{id}', 'CustomerController@descargarpdfcontract');
    Route::post('contrac-update-oneprofile/{id}', 'CustomerController@updateContratorprofile');
    Route::post('update-renovate-contract/{id}', 'CustomerController@renoveContract');


    Route::get('unlink-developer/{id}/{customer_id}', 'CustomerController@destroy');
    //Contract Clients
    Route::get('contract_client_table', 'CustomerController@table_contract_Client');
});


Route::get('contract/{id}', 'CustomerController@showContract');
Route::get('contract-pdf/{id}', 'CustomerController@generateContratorPDF');
Route::get('clients/{id}', 'CustomerRequestController@getCustomerById');
Route::post('clients/cards/{id}/{solicitud}', 'CustomerRequestController@getcardcandidate');

