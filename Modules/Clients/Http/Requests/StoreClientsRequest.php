<?php

namespace Modules\Clients\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreClientsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:20',
            'last_name' => 'nullable|string',
            'email' => 'required|email',
            'phone' => 'nullable|numeric',
            'description' => 'nullable|string|max:180',
            'status' => 'required|numeric',
            'programming_lenguage_id' => 'required',
            'language_id' => 'required',
            'language_level_id' => 'required',
            'timezone_id' => 'required',
            'experience_years' => 'required',
            'site' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Name is required',
            'remote.boolean' => 'remote debe ser verdadero o falso',

        ];

    }

}
