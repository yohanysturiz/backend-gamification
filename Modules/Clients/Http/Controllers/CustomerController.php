<?php

namespace Modules\Clients\Http\Controllers;

use App\Exports\ContractClientExport;
use App\Exports\ContractExport;
use App\Exports\PaymentAccountExport;
use App\User as AppUser;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Clients\Entities\CustomerEmail;
use Modules\User\Entities\User;
use Modules\Clients\Entities\Customer;
use Modules\Clients\Entities\Contract;
use Modules\Clients\Entities\ContractLogs;
use Modules\Clients\Entities\customer_candidate_pivot;
use Modules\Developer\Entities\Developer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;
use SendinBlue\Client\Model\SendSmtpEmail;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Api\ContactsApi;
use GuzzleHttp\Client as GuzzleClient;
use Carbon\Carbon;
use Google\Service\Dfareporting\Resource\Cities;
use Modules\Clients\Events\Datefinished;
use Illuminate\Support\Facades\Storage;
use Modules\Document\Entities\Document;
use Illuminate\Support\Facades\Auth;
use Modules\Clients\Entities\CustomerRequests;
use Modules\GeneralData\Entities\City\City;
use Modules\GeneralData\Entities\City\CityModel;
use Modules\GeneralData\Entities\Country\Country;
use phpDocumentor\Reflection\PseudoTypes\False_;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CustomerController extends Controller
{
    protected $config;
    protected $smtpApiInstance;
    protected $contactApiInstance;

    public function __construct()
    {
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', config('app.sendin'));
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('partner-key', config('app.sendin'));

        // Configure SMTP API key authorization: api-key
        $this->smtpApiInstance = new SMTPApi(new GuzzleClient(), $this->config);
        $contactApiInstance = new ContactsApi(new GuzzleClient, $this->config);
        //return json_encode($ContractUpdate);
    }

    public function index(Request $request)
    {

        $arrRequest = Customer::orderBy('id', 'DESC')->Search($request->input('consult'))->paginate(15);

        foreach ($arrRequest as $key => $customer) {
            $customer->city_expedition_name_representative_legal = $this->getCity($customer->city_expedition_name_representative_legal);
            $customer->city_resident_name_representative_legal = $this->getCity($customer->city_resident_name_representative_legal);
        }

        return [
            'pagination' => [
                'total' => $arrRequest->total(),
                'current_page' => $arrRequest->currentPage(),
                'per_page' => $arrRequest->perPage(),
                'last_page' => $arrRequest->lastPage(),
                'from' => $arrRequest->firstItem(),
                'to' => $arrRequest->lastPage(),

            ],
            'arrRequest' => response()->json($arrRequest, 200)
        ];
    }

    public function getCity($city_id)
    {
        if (empty($city_id)) return;

        $city = CityModel::find($city_id);

        if (empty($city)) return;

        return $city->name;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('clients::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validEmailExist = $this->validEmailExist($request->email_company);
        if ($validEmailExist) return response()->json("Ya se encuentra una cuenta con este email", 400);

        $insUser = $this->createUser($request);

        $customer = $request->input();
        $customer["user_id"] = $insUser->id;
        $customer["responsable"] = $request->name_legal_representative;
        $customer["code_company"] = substr($insUser->uuid, -6);

        unset($request['name_legal_representative']);
        unset($request["password"]);
        unset($request["confirm_password"]);

        $customer = Customer::create($customer);

        return response()->json($insUser, 201);
    }

    protected function createUser($request)
    {
        $roleToAssign = Role::findByName('client', 'api');
        $arrUser = [
            'first_name' => $request->name_legal_representative,
            'last_name' => $request->company_name,
            'email' => $request->email_company,
            'password' => Hash::make($request->password),
            'status' => 1
        ];

        $insUser = User::create($arrUser)->assignRole($roleToAssign);
        return $insUser;
    }

    public function AsignDeveloper(Request $request, $id)
    {
        $customer = Customer::where('uuid', $id)->first();
        $developer = Developer::find($request->candidate_id);
        $customer_candidate = customer_candidate_pivot::where('customer_id', $customer->id)->where('developer_id', $developer->id)->first();
        $developer->user = $developer->user();
        // $user = User::find($developer->user_id);

        //  Aqui editar datos del usuario.
        $user = $developer->user();
        $user->type_document = $request->type_document_representative_legal;
        $user->number_document = $request->number_document_representative_legal;
        $user->city_expedition_document = $request->city_expedition_name_representative_legal;
        $user->save();


        if (!empty($customer_candidate)) return response()->json($developer, 201);

        $customer->developers()->attach($developer->id, ['status' => 'true']);

        return response()->json($developer, 201);
    }

    /**
     * Add Email
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function addEmail(Request $request, $id): JsonResponse
    {
        $customer = Customer::find($id);
        if ($customer) {
            CustomerEmail::create([
                'customer_id' => $id,
                'email' => $request->get('email'),
            ]);
            return response()->json(['success' => true, 'message' => 'Ingreso satisfactorio'], 201);
        } else {
            return response()->json(['success' => false, 'message' => 'No se pudo ingresar asignar el email'], 201);
        }
    }

    /**
     * Get Customer Emails
     * @param $id
     * @return JsonResponse
     */
    public function getCustomerEmails($id): JsonResponse
    {
        $emails = CustomerEmail::where('customer_id', $id)->get();
        if ($emails) {
            return response()->json(['success' => true, 'emails' => $emails], 201);
        } else {
            return response()->json(['success' => false, 'message' => []], 201);
        }
    }

    /**
     * Delete Customer Email
     * @param $id
     * @return JsonResponse
     */
    public function getCustomerEmailDelete($id): JsonResponse
    {
        $email = CustomerEmail::find($id)->delete();
        if ($email) {
            return response()->json(['success' => true, 'message' => 'Correo eliminado'], 201);
        } else {
            return response()->json(['success' => false, 'message' => 'No se pudo eliminar el correo'], 201);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $customer = Customer::where('uuid', $id)->with('developers')->with('Contract', 'user')->first();
        $customer->country = Country::where("id", $customer->country_representative_legal)->first();
        $customer->city = CityModel::where("id", $customer->city_resident_name_representative_legal)->first();

        return response()->json($customer, 200);
    }

    /**
     * Profile the specified resource.
     * @param string $uuid
     * @return Response
     */
    public function profile($uuid)
    {
        $insUser = User::where('uuid', $uuid)->first();
        /* $insCustomer = Customer::where('user_id', $insUser->id)->with('developers', 'city')->first(); */
        $insCustomer = Customer::where('user_id', $insUser->id)->with('developers')->first();
        $insCustomer->country = Country::where("id", $insCustomer->country_representative_legal)->first();
        $insCustomer->city = CityModel::where("id", $insCustomer->city_resident_name_representative_legal)->first();
        $insContract = Contract::where('client_id', $insCustomer->id)->where('user_id', 0)->get();
        $customerRequest = CustomerRequests::where('email', $insCustomer->email_company)->with('lenguages')->get();

        $insCustomer->contract = $insContract;
        $insCustomer->requests = $customerRequest;

        return response()->json($insCustomer, 200);
    }

    public function showContract($id)
    {
        $insContract = Contract::with('developer', 'Customer')->where("uuid", $id)->firstOrFail();
        switch ($insContract->type_contract) {
            case "0":
                $insContract->type_contract = "prestación de servicios";
                break;
            case "1":
                $insContract->type_contract = "prestación de servicios";
                break;
            default:
                $insContract->type_contract = "prestación de servicios";
                break;
        }
        return response()->json($insContract, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('clients::edit');
    }


    public function validEmailExist($email)
    {
        $user = User::where("email", $email)->first();
        if ($user) return true;

        $customer = Customer::where("email_company", $email)->first();
        if ($customer) return true;

        return false;
    }


    public function tablaIndex(Request $request)
    {
        event(new Datefinished());
        $user = Auth::user();
        $role = Auth::user()->getRoleNames()[0];

        if ($role === 'administrator' || $role === 'analyst') {
            $arrRequest = Contract::orderBy('id','DESC')
                ->with('Customer', 'Developer')
                ->where('type', "developer")
                ->where('deleted_at', null);


            if ($request->input('term') != "") {
                $developer_ids = Developer::where('namecandidate', 'like', '%' . $request->input('term') . '%')->pluck('id');
                $arrRequest->whereIn('user_id', $developer_ids);
            }

            if ($request->input('status') != "") {
                $arrRequest->where('status', $request->input('status'));
            }

            if ($request->input('signature') != "") {
                $arrRequest->where('candidate_signed', $request->input('signature'));
            }

            $arrRequest = $arrRequest->paginate(30);
        }

        if ($role === 'candidate') {
            $developer = Developer::where("user_id", $user->id)->first();
            $arrRequest = Contract::orderBy('id','DESC')->with('Customer', 'Developer')->where('type', "developer")->where("user_id", $developer->id)->paginate(30);
        }

        foreach ($arrRequest as $key => $customer) {
            $user = User::find($arrRequest[$key]->developer->user_id);
            $arrRequest[$key]->user = $user;
        }

        return [
            'pagination' => [
                'total' => $arrRequest->total(),
                'current_page' => $arrRequest->currentPage(),
                'per_page' => $arrRequest->perPage(),
                'last_page' => $arrRequest->lastPage(),
                'from' => $arrRequest->firstItem(),
                'to' => $arrRequest->lastPage(),

            ],
            'arrRequest' => response()->json($arrRequest, 200)
        ];
    }

    /**
     * Generate a pdf with information paymentAccount of the developer
     * @return BinaryFileResponse
     */
    public function exportContracts()
    {
        return Excel::download(new ContractExport, 'contratos.xlsx');
    }

    /**
     * Generate a pdf with information paymentAccount of the developer
     * @return BinaryFileResponse
     */
    public function exportContractsClient()
    {
        return Excel::download(new ContractClientExport, 'contratos_clientes.xlsx');
    }


    public function tablaIndexOverCome(Request $request)
    {
        event(new Datefinished());

        $arrRequest = DB::select('select TIMESTAMPDIFF(DAY, c.end_date , now()) AS end_date, c.start_date as start_date
    ,c.id as id ,c.uuid as uuid ,
    c.client_id as client_id, c.contract_value as contract_value, c.contract_value_number as contract_value_number,
    c.status as status ,
    c.type_contract as type_contract,c.number_document as number_document, c.date_firm as date_firm,c.hash as hash,
    c.candidate_signed as candidate_signed ,c.type_document as type_document , d.namecandidate as
    namecandidate,d.lastnamecandidate as lastnamecandidate , c2.company_name as client_id, c.end_date as
    fechavencimiento from contract c inner join developers d on c.user_id = d.id inner join customer c2 on c.client_id =
    c2.id order by c.id Desc');


        return [
            'arrRequest' => response()->json($arrRequest, 200)
        ];

    }


    /**
     * Update
     * @param int $uuid_client
     * @return Response
     */
    public function updateClient(Request $request, $uuid_client)
    {
        $updateForm = $request->all();
        $insclient = Customer::where("uuid", $uuid_client)->first();
        Customer::where("uuid", $uuid_client)->update($updateForm);

        return response()->json($insclient, 200);
    }


    public function updateContrator(Request $request, $id)
    {
        $contractForm["position"] = $request->position;
        $contractForm["contract_value"] = $request->contract_value_letter;
        $contractForm["contract_value_number"] = $request->contract_value_number;
        $contractForm["start_date"] = $request->start_date;
        $contractForm["end_date"] = $request->end_date;
        $contractForm["programming_lenguage"] = $request->programming_lenguage;
        $contractForm["contract_duration"] = $this->calculateDaysOfContract($contractForm);

        $insContract = Contract::where("uuid", $id)->first();

        Contract::where("uuid", $id)->update($contractForm);

        if ($request->end_date >= Carbon::now() && $insContract->candidate_signed) {
            Contract::where('uuid', $id)->update(['status' => "aprobado", 'date_firm' => Carbon::now()]);
        }

        return response()->json($insContract, 200);
    }

    /**
     * Function for calculate days duration of contract
     */
    protected function calculateDaysOfContract($data)
    {
        $start_date = new Carbon($data["start_date"]);
        $end_date = new Carbon($data["end_date"]);
        $monts = ($start_date->diff($end_date)->days < 1)
            ? 'today'
            : $end_date->diffInMonths($start_date);

        if ($monts == 0) {
            $days = ($start_date->diff($end_date)->days < 1)
                ? 'today'
                : $end_date->diffInDays($start_date);

            return "{$days} Días";
        }

        if ($monts == 1) return "{$monts} Mes";

        return "{$monts} Meses";
    }

    public function createContratctVersion($Contractultimamte)
    {
        $arrData = [
            'user_id' => $Contractultimamte->user_id,
            'contract_id' => $Contractultimamte->id,
            "version" => 1,
            "old_data" => $Contractultimamte,
        ];
        $ContractLogs = ContractLogs::create($arrData);

        return $ContractLogs;
    }


    public function generateContratorPDF($id, $base64 = false)
    {
        $ContractPdf = Contract::where('uuid', $id)->with('Customer', 'Developer', 'Cityresidence', 'Cityexpedition')->first();
        $user = User::where('id', $ContractPdf->user_id)->first();

        if ($ContractPdf->type == "developer") {
            $user = User::where("email", $ContractPdf->developer->emailcandidate)->first();
        } else {
            $customer = Customer::where("email_company", $ContractPdf->customer->email_company)->first();
            $user = User::where("email", $customer->email_company)->first();
        }

        $full_name = $user->first_name . " " . $user->last_name;
        $format_start_contract = Carbon::parse($ContractPdf->start_date);
        $ContractPdf->format_start_contract = "{$format_start_contract->day} de {$format_start_contract->monthName} del {$format_start_contract->year}";

        $content_contract = $ContractPdf->content_contract;
        $content_contract = str_replace('$full_name',mb_strtoupper($full_name) , $content_contract);
        $content_contract = str_replace('$type_document', mb_strtoupper($user->type_document), $content_contract);
        $content_contract = str_replace('$number_document', $user->number_document, $content_contract);
        $content_contract = str_replace('$city_expedition_doc', $user->city_expedition_document, $content_contract);
        $content_contract = str_replace('$city_resident', $user->city_expedition_document , $content_contract);
        $content_contract = str_replace('$lenguage_programming', mb_strtoupper($ContractPdf->programming_lenguage), $content_contract);
        $content_contract = str_replace('$contract_duration', $ContractPdf->contract_duration, $content_contract);
        $content_contract = str_replace('$contract_value_letter', mb_strtoupper($ContractPdf->contract_value), $content_contract);
        $content_contract = str_replace('$contract_value_number', number_format($ContractPdf->contract_value_number, 2, ',', '.'), $content_contract);
        $content_contract = str_replace('$start_date', $ContractPdf->format_start_contract, $content_contract);

        if ($ContractPdf->candidate_signed == 1) {
            $insDocumentFirm = Document::where("type_document", "firma_digital")->where("user_id", $user->id)->first();
            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(10));
            $ContractPdf->firm = $file_firm;

            $format_start_contract = Carbon::parse($ContractPdf->start_date);
            $ContractPdf->format_start_contract = "{$format_start_contract->day} de {$format_start_contract->monthName} del {$format_start_contract->year}";

            $format_date_firm = Carbon::parse($ContractPdf->date_firm);
            $ContractPdf->format_date_firm = "{$format_date_firm->day} de {$format_date_firm->monthName} del {$format_date_firm->year}";
        }

        switch ($ContractPdf->type_contract) {
            case "0":
                $name = "Contracto_Prestacion_de_Servicio" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.Contract_clients_servicios', compact('ContractPdf'))->save($name);
                break;
            case "1":
                $name = "Contracto_Prestación_de_Servicios" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.contract-pdf', compact('content_contract', 'ContractPdf', 'user'))->save($name);
                break;
            case "Termino Indefinido":
                $name = "Contracto_Termino_Indefinido" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.termino-indefinido', compact('ContractPdf'))->save($name);
                break;
            case "Mandato":
                $name = "Contracto_Mandato" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.mandato', compact('ContractPdf'))->save($name);
                break;
            default:
                break;
        }

        if ($base64) return base64_encode(file_get_contents($name));

        return $pdf->stream($name);
    }


    public function updateContratorprofile(Request $request, $id)
    {
        $ContractUpdateprofile = Contract::where('uuid', $id)->with('Developer')->first();
        $ContractUpdateprofile->status = "cancelado";
        $ContractUpdateprofile->save();

        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $ContractUpdateprofile->developer->emailcandidate));
        $sendSmtpEmail['templateId'] = 13;

        $nombrecontrato = 'Terminacion_De_Contrato.pdf';
        $sendSmtpEmail['attachment'] = array(array(
            "name" => $nombrecontrato,
            "content" => $this->Terminacioncontrato($ContractUpdateprofile->uuid)
        ));

        try {
            // $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $ContractUpdateprofile;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }


//OBTENER CONTRATOS DE CLIENTE PARA EL DASHBOARD

    public function getClientContracts($email)
    {
        $customerId = Customer::where('email_company', $email)->value('id');
        $contracts = Contract::where('client_id', $customerId)->with('Developer')->get();
        return response()->json($contracts, 200);
    }

    /**************************************************************/


    public function ContratorUpdatedate(Request $request, $id)
    {
        $affected = Contract::where('uuid', $id)->update(['status' => "vencido"]);
        return response()->json($affected, 200);
    }


    public function signatureContract(Request $request, $id)
    {

        $contract = Contract::where('uuid', $id)
            ->where("status", "pendiente")
            ->where("hash", $request->hash)
            ->first();

        if (!empty($contract)) {
            response()->json(["msg" => "El hash es incorrecto o el contrato ya no esta disponible"], 400);
        }
        $new_data = $request->all();
        $new_data["city_residence"] = $request->residencecity;
        $new_data["candidate_signed"] = false;
        $contract->update($new_data);
        $contract = Contract::where('uuid', $id)->update(['status' => "aprobado", 'date_firm' => Carbon::now()]);

        return response()->json($contract, 200);
    }


    public function Terminacioncontrato($id)
    {
        $ContractPdf = Contract::where('uuid', $id)->with('Customer', 'Developer', 'Cityresidence', 'Cityexpedition')->first();
        $name = "Contracto_Desarrolladores" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
        $pdf = PDF::loadView('clients::pdf.terminaciondecontrato', compact('ContractPdf'))->save($name);
        return base64_encode(file_get_contents($name));
    }


    public function descargarpdfcontract($id)
    {
        return $this->generateContratorPDF($id, false);
    }


    public function destroy($id, $customer_id)
    {
        // $contract_pivot = customer_candidate_pivot::where('developer_id', $id)->where('customer_id', $customer_id)->update(['status' => 'inactivo']);
        $contract_pivot = customer_candidate_pivot::where('developer_id', $id)->where('customer_id', $customer_id)->delete();
        // $contractdelete = Contract::where('user_id', $id)->update(['status' => "deleted"]);

        return response()->json($contract_pivot, 200);
    }

    public function changepassword($id)
    {
        $user = Auth::user();
        $updatepassword = User::where('id', $user->id)->update(['password' => Hash::make($id)]);
        return response()->json($updatepassword, 200);
    }


    public function table_Contract_Client(Request $request)
    {
        event(new Datefinished());
        $arrRequest = Contract::orderBy('id', 'DESC')
            ->with('Customer', 'Developer')
            ->where('type', "client")
            ->paginate(10);

        foreach ($arrRequest as $key => $customer) {
            $user = User::find($arrRequest[$key]->user_id);
            $arrRequest[$key]->user = $user;
        }

        return [
            'pagination' => [
                'total' => $arrRequest->total(),
                'current_page' => $arrRequest->currentPage(),
                'per_page' => $arrRequest->perPage(),
                'last_page' => $arrRequest->lastPage(),
                'from' => $arrRequest->firstItem(),
                'to' => $arrRequest->lastPage(),

            ],
            'arrRequest' => response()->json($arrRequest, 200)
        ];
    }

    // Utils /

    /**
     * Send email notification by create contract for customer
     */
    protected function sendEmailCreateContract($customer, $insContractclients)
    {
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $customer->email_company));
        $sendSmtpEmail['templateId'] = 12;

        if ($insContractclients->type_contract = "Prestacion de Servicio Client") {
            $nombrecontrato = 'Contrato_Prestacion_de_servicios_cliente.pdf';
        }

        $sendSmtpEmail['attachment'] = array(array(
            "name" => $nombrecontrato,
            "content" => $this->generateContratorPDF($insContractclients->uuid, 1)
        ));

        $sendSmtpEmail['params'] = array(
            'nombre' => $customer->company_name,
            'link' => config("app.url_client", 'https://app.imuko.co') . "/#/candidate-signature/{$insContractclients->uuid}",
            'hash' => $insContractclients->hash
        );

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            // return $result;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return;
    }

    /**
     * Send email notification by create contract for customer
     */
    protected function sendEmailAsignDev($insContract)
    {
        $contract_name = $this->getTypeContract($insContract->type_contract);
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $insContract->developer->emailcandidate));
        $sendSmtpEmail['templateId'] = 12;

        $sendSmtpEmail['attachment'] = array(array(
            "name" => $contract_name,
            "content" => $this->generateContratorPDF($insContract->uuid, true)
        ));

        $sendSmtpEmail['params'] = array(
            'nombre' => $insContract->developer->namecandidate,
            'link' => config("app.url_client", 'https://app.imuko.co') . "/#/candidate-signature/{$insContract->uuid}",
            'hash' => $insContract->hash
        );

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return;
    }


    protected function getTypeContract($type)
    {
        switch ($type) {
            case 'Termino Indefinido':
                $name = 'Contrato_Termino_Indefinido.pdf';
                break;
            case 'Mandato':
                $name = 'Contrato_Mandato.pdf';
                break;
            case 'Prestacion de Servicio Client':
                $name = 'Contrato_Prestacion_de_servicios_cliente.pdf';
                break;
            default:
                $name = "Contrato_Prestación_de_Servicios.pdf";
                break;
        }

        return $name;
    }


    public function deleteContractClient($id)
    {
        $deleteClient = Contract::where('uuid', $id)->delete();
        return response()->json($deleteClient, 200);
    }


    public function deleteClient($id)
    {
        $deleteClient = customer_candidate_pivot::where('customer_id', $id)->first();
        if (!empty($deleteClient)) {
            $data = ['type' => 'error', 'title' => 'No se puede realizar la eliminacion el cliente posee candidatos asignados.',
                'msg' => 'Desvincule el desarrollador y elimina'];
            return response()->json($data, 200);
        }
        $delete = Customer::where('id', $id)->delete();
        return response()->json($delete, 200);
    }


    public function renoveContract(Request $request ,$id )
    {
        $updateFormrenove["position"] = $request->position;
        $updateFormrenove["contract_value"] = $request->contract_value;
        $updateFormrenove["contract_value_number"] = $request->contract_value_number;
        $updateFormrenove["start_date"] = $request->start_date;
        $updateFormrenove["end_date"] = $request->end_date;
        $updateFormrenove["status"] = "pendiente";
        $updateFormrenove["contract_duration"] = $this->calculateDaysOfContract($updateFormrenove);

        Contract::where("uuid", $id)->update($updateFormrenove);
        $insContractrenove = Contract::where("uuid", $id)->first();
        // $this->createContratctVersion($insContractrenove);

        return response()->json($insContractrenove, 200);

    }

}
