<?php

namespace Modules\Clients\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Clients\Entities\customer_request_developer;
use Modules\Clients\Entities\CustomerRequests;
use Modules\Clients\Http\Requests\StoreClientsRequest;
use Modules\Developer\Entities\Developer;
use Modules\Developer\Http\Controllers\DeveloperController;
use App\Http\Controllers\EmailController;
use Modules\GeneralData\Entities\Country\Country;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Api\ContactsApi;
use GuzzleHttp\Client as GuzzleClient;
use SendinBlue\Client\Model\SendSmtpEmail;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Hash;
use Modules\Clients\Entities\Customer;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;
use Modules\GeneralData\Entities\ProgrammingLenguage\FrameworkLenguageModel;

class CustomerRequestController extends Controller
{
    protected $config;
    protected $smtpApiInstance;
    protected $contactApiInstance;

    public function __construct()
    {
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', config('app.sendin'));
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('partner-key', config('app.sendin'));

        // Configure SMTP API key authorization: api-key
        $this->smtpApiInstance = new SMTPApi(new GuzzleClient(), $this->config);
        $contactApiInstance = new ContactsApi(new GuzzleClient, $this->config);
        //return json_encode($ContractUpdate);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $arrRequest = CustomerRequests::orderBy('id', 'DESC')->with('lenguages', 'city')->paginate(10);

        return [
            'pagination' => [
                'total' => $arrRequest->total(),
                'current_page' => $arrRequest->currentPage(),
                'per_page' => $arrRequest->perPage(),
                'last_page' => $arrRequest->lastPage(),
                'from' => $arrRequest->firstItem(),
                'to' => $arrRequest->lastPage(),

            ],
            'arrRequest' => response()->json($arrRequest, 200)
        ];
    }

    public function getLastProcess()
    {
        $arrRequest = CustomerRequests::with('lenguages')->orderby('id', 'desc')->limit(5)->get();
        return response()->json($arrRequest, 200);
    }

//*******************GET REQUESTS DATA FOR CLIENT'S DASHBOARD***************//

    public function getClientRequests($email)
    {
        $arrRequests = CustomerRequests::where('email', $email)->with('lenguages', 'developers', 'city')->get();
        return response()->json($arrRequests, 200);
    }

//*******************************************************************//


    public function processRequest(Request $request, $id)
    {
        $developer = new DeveloperController;
        $emailer = new EmailController;
        $selecteds = $request->input('candidates');
        $customer = CustomerRequests::where('uuid', $id)->first();
        $candidates = Developer::find(explode(",", $selecteds));
        $pdfs = array();
        // for ($i=0; $i < count($candidates); $i++) {
        //     $pdf = array(
        //         "name" => "cv-".$candidates[$i]->uuid.".pdf",
        //         "content" => $developer->SafeCvBase64($candidates[$i]->uuid)
        //     );
        //     array_push($pdfs, $pdf);
        // }
        $enviaremails = array();
        for ($j = 0; $j < count($candidates); $j++) {
            $enviar = array(
                "email" => $candidates[$j]->emailcandidate,
            );
            array_push($enviaremails, $enviar);
        }
        $emailer->EmailDeveloperSelected($customer, 4, $pdfs, $enviaremails);

        return response()->json($pdfs, 200);
    }

    public function getCustomerById($id)
    {
        $customer = CustomerRequests::where('uuid', $id)->with('lenguages', 'developers.lenguages', 'city')->first();

        if ($customer->city)
            $customer->country = Country::find($customer->city->country_id);

        return response()->json($customer, 200);
    }

    public function getcardcandidate($id, $solicitud)
    {
        $customerid = CustomerRequests::where('uuid', $solicitud)->with('lenguages', 'developers', 'city')->first();
        $developer = Developer::where('uuid', $id)->first();
        $customer = customer_request_developer::where('customer_id', $solicitud)->where('developer_id', $developer->id)->first();
        $actu = DB::table('customer_request_developer')->where('id', $customer->id)->update(['status' => "seleccionado"]);
        $developer = Developer::where('uuid', $id)->first();
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => "admon@imuko.co"));
        $sendSmtpEmail['templateId'] = 15;

        $sendSmtpEmail['params'] = array('USERNAME' => $customerid["first_name"] . ' ' . $customerid["last_name"], 'NOMBRE' => $developer->namecandidate, 'APELLIDOS' => $developer->lastnamecandidate);

        try {
            $this->getemaildesarrolador($developer->uuid, $customerid["uuid"]);
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $result;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return response()->json($actu, 200);
    }

    public function getemaildesarrolador($id, $solicitud)
    {
        $developeremail = Developer::where('uuid', $id)->first();
        $customerid = CustomerRequests::where('uuid', $solicitud)->with('lenguages', 'developers', 'city')->first();
        $link = config("app.url_client", 'https://app.imuko.co') . "/#/selected-candidates/" . $solicitud;
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $developeremail->emailcandidate));
        $sendSmtpEmail['templateId'] = 12;

        $sendSmtpEmail['params'] = array('NOMBRE' => $developeremail->namecandidate . ' ' . $developeremail->lastnamecandidate, 'LINK' => $link);
        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $result;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return response()->json($developeremail, 200);
    }


    public function saveDevelopersRequest(Request $request, $id)
    {

        $customer = CustomerRequests::where('uuid', $id)->first();
        $developers = $request->input('selecteds');
        $action = $request->input('action');
        if ($action && $action == "add") {
            for ($i = 0; $i < count($developers); $i++) {
                $actionDevelopersCustomer = $customer->developers()->attach($developers[$i]['id']);
            }
        } else {
            $candidate = $request->input('candidate');
            $actionDevelopersCustomer = $customer->developers()->detach($candidate);
        }

        return response()->json($actionDevelopersCustomer, 201);
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('clients::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StoreClientsRequest $request)
    {
        $insCustomerRequest = $this->customer_request($request->all(), $request->programming_lenguage_id);

        return response()->json(["data" => $insCustomerRequest], 200);
    }

    public function customer_request($all, $programming_lenguage_id)
    {
        $insCustomerRequest = CustomerRequests::create($all);
        $arrLenguagesId = explode(",", $programming_lenguage_id);
        $insCustomerRequest->lenguages()->attach($arrLenguagesId);

        $this->sendMail($insCustomerRequest, $arrLenguagesId);

        return $insCustomerRequest;
    }

    public function sendMail($insCustomerRequest, $arrLenguagesId)
    {
        $lenguaje = ProgrammingLenguagueModel::select('name')->whereIn('id', $arrLenguagesId)->pluck("name")->toArray();
        $framework = FrameworkLenguageModel::select('name')->whereIn('programming_lenguage_id', $arrLenguagesId)->pluck("name")->toArray();
        $all = implode(',', $lenguaje);
        $frameworkAll = implode(',', $framework);

        /** Convertir esto en un evento (Email) */
        $emailer = new EmailController;
        $emailer->RequestEmailResponse($insCustomerRequest, 2, 3, $all, $frameworkAll);

        return;
    }

    public function customer($company, $email, $phone, $city_id, $description)
    {
        $customer = new Customer;
        $customer->company_name = $company;
        $customer->type_company = "";
        $customer->rut = "";
        $customer->user_id = 1;
        $customer->responsable = "";
        $customer->email_company = $email;
        $customer->phone = $phone;
        $customer->city_id = $city_id;
        $customer->code_company = "";
        $customer->description = $description;
        $customer->save();

        return $customer;
    }


    public function show($id)
    {
        return view('clients::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('clients::edit');
    }

    public function updateuser($email, $password)
    {
        $affected = User::where('email', $email)->update(['password' => Hash::make($password)]);
        return response()->json($affected, 200);

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
