<?php

namespace Modules\Organizations\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Organizations\Entities\OrganizationUser;
use Modules\Organizations\Http\Requests\StoreOrganizationUserRequest;

class OrganizationUserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return OrganizationUser::all();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('organizations::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreOrganizationUserRequest $request)
    {
        return OrganizationUser::create($request->all());
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function createOrganizationUser($org_id, $user_id)
    {
        return OrganizationUser::create(
            [
                "organization_id" => $org_id,
                "user_id" => $user_id,
                "status" => 1
            ]
        );
    }

    public function organizationUserFind($user_id){
        {
            $organization_user = OrganizationUser::where("user_id", $user_id)->first();
            
            if ($organization_user) {
                return response()->json([$organization_user], 200);
            }

            return response()->json([], 404);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('organizations::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('organizations::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $updateOrganization = OrganizationUser::find($id)->update($request->all());
        return response()->json($updateOrganization, 202);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $organizationDelete = OrganizationUser::find($id);
        
        if ($organizationDelete){
            $organizationDelete->status = 4;
            $organizationDelete->save();
        }

        return response()->json($organizationDelete, 200);
    }
}
