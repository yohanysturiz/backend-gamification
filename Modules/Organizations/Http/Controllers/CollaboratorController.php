<?php

namespace Modules\Organizations\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Gamification\Entities\Target;
use Modules\Gamification\Entities\TargetsUser;
use Modules\Gamification\Entities\Team;
use Modules\Gamification\Entities\UsersTeam;
use Psy\Command\WhereamiCommand;
use Illuminate\Database\Eloquent\Relations\belongsToMany;
use Modules\User\Entities\User;

class CollaboratorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('organizations::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('organizations::create');
    }

    public function teamBelong($user_id)
    {
        $teams = new Collection();
        $belongs = UsersTeam::where('user_id', $user_id)->get();

        foreach($belongs as $belong){
            $team = Team::find($belong->team_id);
            $teams->push($team);
        }
        
        return $teams;
    } 

    public function targetUser($user_id)
    {
        $targets = new Collection();
        $userTargets = TargetsUser::where('user_id', $user_id)->get();
        
        foreach($userTargets as $userTarget){
            $target = Target::find($userTarget->target_id);
            $target["complete_percentege"] = 0;
            $target["complete_status"] = "exception";

            if (isset($userTarget->complete_percentege)) {
                $target["complete_percentege"] = $userTarget->complete_percentege;

                if ($target["complete_percentege"] >= 30) $target["complete_status"] = "warning";
                if ($target["complete_percentege"] >= 60) $target["complete_status"] = "primary";
                if ($target["complete_percentege"] == 100) $target["complete_status"] = "success";

            }
            
            $targets->push($target);
        }
        return $targets;
    }

    public function members($teamId){
        $teams = new Collection();
        $members = UsersTeam::where('team_id', $teamId)->get();
        foreach($members as $member){
            $team = User::find($member->user_id);
            $teams->push($team);
        }
        return $teams;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('organizations::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('organizations::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
