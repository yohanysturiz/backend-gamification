<?php

namespace Modules\Organizations\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Organizations\Entities\Organization;
use Modules\Gamification\Entities\Team;
use Modules\Gamification\Entities\Target;
use Modules\Organizations\Entities\OrganizationUser;
use Modules\Organizations\Http\Requests\StoreOrganizationRequest;
use Modules\User\Repositories\User\UserRepositoryInterface;

class OrganizationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return Organization::all();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('organizations::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreOrganizationRequest $request, UserRepositoryInterface $userRepository)
    {
        $user_data = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "password" => $request->password,
            "country_id" => $request->country_id,
            "phone" => $request->phone,
        ];

        $user = $userRepository->createUser(
            $user_data,
            "client"
        );

        $org = Organization::create(
            [
                "name" => $request->name,
                "industry_id" => $request->industry_id,
                "licenses" => $request->licenses,
                "plan_id" => $request->plan_id,
                "user_id" => $user->id,
                "status" => 1,
            ]
        );

        $user["org"] = $org;

        return response()->json($user, 200);
    }

    public function organizationFind($org_id, UserRepositoryInterface $userRepository)
    {
        $organization = Organization::find($org_id);
        $user_admin = $userRepository->getUserById($organization->user_id);
        
        $organization["admin"] = $user_admin;

        return response()->json($organization, 200); 
    }

    /**
     * Show the general statistics of organization.
     * @param int $org_id
     * @return Json
     */
    public function generalStatistics($org_id) {
        $users_count = OrganizationUser::where("organization_id", $org_id)->count();
        $teams_count = Team::where("client_id", $org_id)->count();
        $targets_count = Target::where("client_id", $org_id)->count();
        $overall_ytd = 10;


        $statistics = [
            "users" => $users_count,
            "teams" => $teams_count,
            "targets" => $targets_count,
            "overall_ytd" => $overall_ytd,
        ];
        
        return response()->json($statistics, 200);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('organizations::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('organizations::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $updateOrganization = Organization::find($id)->update($request->all());
        return response()->json($updateOrganization, 202);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $organizationDelete = Organization::find($id);
        
        if ($organizationDelete){
            $organizationDelete->status = 4;
            $organizationDelete->save();
        }

        return response()->json($organizationDelete, 200);
    }
}
