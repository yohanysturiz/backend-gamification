<?php

namespace Modules\Organizations\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Organizations\Entities\OrgPersonalization;
use Modules\Organizations\Http\Requests\OrgPersonalizationRequest;

class OrgPersonalizationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('organizations::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('organizations::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(OrgPersonalizationRequest $request)
    {
        
        $org_id = $request->input('org_id');
        $folder_path = 'imagenes';
        $org_personalization_list = new Collection();

        
        $files = [
            "logo" => $request['logo'],
            "image_login" => $request['image_login'],
            "favicon" => $request['favicon']
        ];


        foreach ($files as $key => $value) {

            if (!empty($value)){
                $image_base64 = $value;

                $name_image = uniqid(). '.jpg';

                $decode_image = base64_decode($image_base64);
    
                $url = $folder_path. '/' .$name_image;
    
                Storage::disk('local')->put( $url, $decode_image);
    
                $data = [
                    $key => Storage::url($folder_path. '/' .$name_image),
                    'org_id'=> $org_id
                ];
                
                $org_personalization_list->push(OrgPersonalization::create($data));
            }      
        }
        return $org_personalization_list;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('organizations::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('organizations::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
