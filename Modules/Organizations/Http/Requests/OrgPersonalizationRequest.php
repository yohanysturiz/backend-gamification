<?php

namespace Modules\Organizations\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrgPersonalizationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'org_id'=>'required',
            'logo'=>'nullable',
            'image_login'=>'nullable',
            'favicon'=>'nullable',
            'color_principal'=>'nullable',
            'color secundary'=>'nullabled'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
