<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrgPersonalizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('org_personalization', function (Blueprint $table) {
            $table->id();
            $table->integer('org_id');
            $table->string('logo')->nullable();
            $table->string('image_login')->nullable();
            $table->string('favicon')->nullable();
            $table->string('color_principal')->nullable();
            $table->string('color_secondary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('org_personalization');
    }
}
