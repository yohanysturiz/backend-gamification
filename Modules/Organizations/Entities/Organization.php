<?php

namespace Modules\Organizations\Entities;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = "organizations";
    protected $fillable = ['name','rut','address','user_id','industry_id','licenses','plan_id','status'];
}
