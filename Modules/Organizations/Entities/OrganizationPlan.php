<?php

namespace Modules\Organizations\Entities;

use Illuminate\Database\Eloquent\Model;

class OrganizationPlan extends Model
{
    protected $table = "organizations_plan";
    protected $fillable = ['organization_id', 'name', 'type', 'licenses', 'status', 'description'];
}
