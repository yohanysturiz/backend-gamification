<?php

namespace Modules\Organizations\Entities;

use Illuminate\Database\Eloquent\Model;

class OrgPersonalization extends Model
{
    protected $table = "org_personalization";
    protected $fillable = ['org_id','logo','image_login','favicon','color_principal','color_secondary'];
}
