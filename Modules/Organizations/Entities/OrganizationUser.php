<?php

namespace Modules\Organizations\Entities;

use Illuminate\Database\Eloquent\Model;

class OrganizationUser extends Model
{
    protected $table = "organizations_users";
    protected $fillable = ['organization_id','user_id','status'];
}
