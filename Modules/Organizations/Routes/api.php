<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# Organization
Route::prefix('v2/organization')->group(function () {
    Route::post('/create', 'OrganizationsController@store');
    Route::get('/list', 'OrganizationsController@index');
    Route::get('/{org_id}', 'OrganizationsController@organizationFind');
});

# Organization Panel Statistics
Route::prefix('v2/organization/{org_id}/panel')->group(function () {
    Route::get('/general/statistics', 'OrganizationsController@generalStatistics');
});

Route::put('organization/{id}', 'OrganizationsController@update');
Route::delete('organization/{id}', 'OrganizationsController@destroy');

# Organization Users
Route::get('organization_user', 'OrganizationUserController@index');
Route::get('organization_user/{user_id}', 'OrganizationUserController@organizationUserFind');
Route::post('organization_user', 'OrganizationUserController@store');
Route::put('organization_user/{id}', 'OrganizationUserController@update');
Route::delete('organization_user/{id}', 'OrganizationUserController@destroy');

Route::post('org', 'OrgPersonalizationController@store');

Route::get('team_belong/{id}', 'CollaboratorController@teamBelong');
Route::get('target_user/{id}', 'CollaboratorController@targetUser');
Route::get('companions/{id}', 'CollaboratorController@members');

Route::get('organization_plan', 'OrganizationPlanController@index');
Route::get('organization_plan/{id}', 'OrganizationPlanController@organizationPlanFind');
Route::post('organization_plan', 'OrganizationPlanController@store');
Route::put('organization_plan/{id}', 'OrganizationPlanController@update');