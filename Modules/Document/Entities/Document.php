<?php

namespace Modules\Document\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = "user_document_pivot";

    protected $fillable = ['uuid', 'user_id', 'process_id', 'filename', 'type_document', 'url', 'description', 'status'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

}
