<?php

namespace Modules\Document\Http\Repositories;

use Modules\Document\Entities\Document;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Document\Http\Repositories\DocumentRepositoryInterface;

class DocumentRepository implements DocumentRepositoryInterface
{
    protected $model;

    /**
     * DocumentRepository constructor.
     *
     * @param Document $document
     */
    public function __construct(Document $document)
    {
        $this->model = $document;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getAllByUser($user_id)
    {
        $arrDocuments = $this->model->where("user_id", $user_id)->whereNotIn('type_document', ["certificate_payment_social_security"])->get();
        
        return $arrDocuments;
    }

    public function create(array $data)
    {
        if($this->validExistDocument($data)) throw new \ErrorException('Document already exist');
        
        return $this->model->create($data);
    }

    public function update(array $data, $uuid)
    {
        return $this->model->where('uuid', $uuid)
            ->update($data);
    }

    public function delete($uuid)
    {
        $insDocument = $this->model->where("uuid", $uuid)->first();
        if (null == $insDocument) throw new \ErrorException('Document not exist');

        Storage::disk(config('imuko.disk'))->delete($insDocument->type_document."/".$insDocument->filename);
        
        return $insDocument->delete();
    }

    public function find($uuid)
    {
        $document = $this->model->find("uuid", $uuid);
        if (null == $document) throw new ModelNotFoundException("Document not found");

        return $document;
    }

    private function validExistDocument($data)
    {
        $insDocument = $this->model->where("filename", $data["filename"])
        ->where("user_id", $data["user_id"])
        ->where("type_document", $data["type_document"])
        ->first();

        if (empty($insDocument)) return false;

        return true;
    }
}