<?php

namespace Modules\Document\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Document\Entities\Document;
use Modules\User\Entities\User;
use Modules\Document\Http\Repositories\DocumentRepositoryInterface;

use SendinBlue\Client\Model\SendSmtpEmail;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Api\ContactsApi;
use GuzzleHttp\Client as GuzzleClient;

class DocumentController extends Controller
{
    use \Modules\Developer\Http\Controllers\Traits\TraitUserUtils;

    /** @var DocumentRepositoryInterface */
    private $repository;

    protected $config;
    protected $smtpApiInstance;
    protected $contactApiInstance;

    public function __construct(DocumentRepositoryInterface $repository)
    {
        $this->repository = $repository;

        $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', config('app.sendin'));
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('partner-key', config('app.sendin'));

        // Configure SMTP API key authorization: api-key
        $this->smtpApiInstance = new SMTPApi(new GuzzleClient(), $this->config);
        $contactApiInstance = new ContactsApi(new GuzzleClient, $this->config);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * Body: file, user_uuid, type
     * Route: /api/document/upload-file-profile
     */
    public function store(Request $request, $user_uuid)
    {
        $insUser = $this->getUser($user_uuid);
        $ext = pathinfo($request->file("file")->getClientOriginalName(), PATHINFO_EXTENSION);
        $name_document = $request->type. "-" . $insUser->uuid . ".". $ext;
        $path = $request->file("file")->storeAs($request->type, $name_document, config('imuko.disk'));
        try {
            $insDocument = $this->repository->create(
                [
                    "filename" => basename($path),
                    "url" => Storage::disk(config('imuko.disk'))->url($path),
                    "user_id" => $insUser->id,
                    "type_document" => $request->type,
                    "status" => 0
                ]);

            if ($request->type == "carta_renuncia") {
                $this->sendEmailCartaRenuncia($insUser);
            }

            return response()->json($insDocument , 201);
        } catch (\Exception $e) {
            $msg = "Document already exist";
            if ($msg == $e->getMessage()) return response()->json("Ya tienes una firma cargada, pulsa enviar" , 401);

            return response()->json("Fallo al subir el documento" , 500);
        }
    }

    /**
     * Get the specified document.
     * @param int $id
     * @return Response
     */
    public function getByTypeDocument($id) {

        $insDocument = Document::where('filename', $id)->first();

        if($insDocument) {
            $file = Storage::disk(config('imuko.disk'))->get("{$insDocument->type_document}/" . $insDocument->filename, now()->addSeconds(10));
        }

        return $file;
    }

    /**
     * Get all Files for Profile Developer.
     * @param string $email
     * @return Response
     */
    public function getAllFileProfile(Request $request, $user_uuid)
    {
        $insUser = $this->getUser($user_uuid);

        $arrTypeDocument = config('imuko.document_list_candidate');
        $documents = $this->repository->getAllByUser($insUser->id);

        if($request->get('type_user') == "client")
            $arrTypeDocument = config('imuko.document_list_client');

        foreach ($documents as $key => $value) {
            $index =  array_search($value->type_document, $arrTypeDocument);
            if($index >= 0) unset($arrTypeDocument[$index]);
        }

        $data = [
            "documents" => $documents,
            "list_document" => $arrTypeDocument
        ];

        return response()->json($data , 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($uuid)
    {
        $this->repository->delete($uuid);

        return response()->json(["msg" => "documento eliminado"] , 200);
    }

    /** send email carta renuncia notificacion for admin */
    public function sendEmailCartaRenuncia($data){
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => "yohanysturiz@crehana.com"));
        $sendSmtpEmail['templateId'] = 12;

        $sendSmtpEmail['params'] = array(
            'name' => $data->first_name . " " . $data->last_name,
        );

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    
    }
}
