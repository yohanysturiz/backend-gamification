<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/document-verify', function (Request $request) {
    return response()->json(201);
});


Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/get-document/{id}', 'DocumentController@getByTypeDocument');
    Route::get('/document/get-files-profile/{user_uuid}', 'DocumentController@getAllFileProfile');
    Route::post('/document/upload-file-profile/{user_uuid}', 'DocumentController@store');
    Route::delete('/document/delete-file-profile/{uuid}', 'DocumentController@destroy');
});
