<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Register User
Route::post('/register', 'RegisterController@store');

// Register V2
Route::prefix('v2/auth')->group(function () {
    Route::post('/login', 'AuthController@authenticate');
});

// Register Organization Talent V2
Route::prefix('v2/auth/organization/{org_id}/user')->group(function () {
    # Get all users organizations
    Route::post('/create', 'RegisterController@createCollaborator');
});

// Login
Route::post('/login', 'AuthController@authenticate');
Route::post('/login/social/google','AuthController@prueba');
Route::post('/auth/{provider}','AuthController@provider');

// logout
Route::get('/logout', 'AuthController@logout');


Route::middleware('api')->post('/recoverypassword', 'AuthController@RecoverPassword');
Route::middleware('api')->get('/gettokenrecovery/{token}', 'AuthController@GetTokenRecoverPassword');
Route::middleware('api')->post('/updatepassword', 'AuthController@UpdatePassword');
