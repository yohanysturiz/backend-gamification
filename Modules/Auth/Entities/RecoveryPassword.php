<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;

class RecoveryPassword extends Model
{
    protected $fillable = ['email', 'remember_token', 'state'];
    protected $table = 'recovery_password';
}
