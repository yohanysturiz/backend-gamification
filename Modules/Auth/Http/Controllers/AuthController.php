<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\EmailController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Developer\Entities\Developer;
use Modules\Organizations\Entities\Organization;
use Modules\Organizations\Entities\OrganizationUser;
use Laravel\Socialite\Facades\Socialite;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Modules\Auth\Entities\RecoveryPassword;
use Modules\User\Entities\User;
use Spatie\Permission\Models\Role;


class AuthController extends Controller
{

    public function __construct(Developer $developer)
    {
        $this->developer = $developer;
    }


    public function provider(Request $request, $provider)
    {
        try {

            $typeAuthenticate = $request->input('typeOfAuthorization');
            $driver = Socialite::driver($provider);
            $code = $request->input('code');
            $authorization = $driver->getAccessTokenResponse($code);
            $access_token = $authorization['access_token'];
            $socialAccount = 'id_' . $provider;


            if ($access_token && $driver) {
                $user = $driver->userFromToken($access_token);
                if ($typeAuthenticate == "associate") {
                    $currentUser = User::where('uuid', $request->input('userId'))->first();
                    if (!$currentUser) return ResponseBuilder::error(401, null, null, 401);
                    $currentUser->{$socialAccount} = $user->getId();
                    $currentUser->save();
                    return ResponseBuilder::success(["id" => $user->getId()]);
                }
                if ($typeAuthenticate == "register") {
                    return $this->registrationWithProvider($user, $socialAccount);
                }

                return $this->loginWithProvider($user, $socialAccount);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function loginWithProvider($user, $socialAccount)
    {
        $checkUserSystem = User::where('email', $user->getEmail())
            ->orWhere($socialAccount, $user->getId())->first();

        if ($checkUserSystem) {
            return $this->handleAuthenticate($checkUserSystem);
        } else {
            return ResponseBuilder::error(401, null, null, 401);
        }
    }


    public function registrationWithProvider($user, $socialAccount)
    {
        try {
            // Validate the value...
            $validEmailExist = $this->developer->validEmailExist($user->getEmail());
            if ($validEmailExist != false) return response()->json("El email ya pertenece a otra cuenta", 400);
            $roleToAssign = Role::findByName('candidate', 'api');
            $idUser = $user->getId();
            $dataUserNew = [
                'first_name' => $user->getName(),
                'last_name' => '',
                'email' => $user->getEmail(),
                'password' => Hash::make($idUser),
                'status' => 1,
                "$socialAccount" => $idUser
            ];
            $userCreate = User::create($dataUserNew)->assignRole($roleToAssign);
            $dataUserNew['user_id'] = $userCreate->id;
            $dataUserNew['namecandidate'] = $user->getName();
            $dataUserNew['lastnamecandidate'] = '';
            $dataUserNew['emailcandidate'] = $user->getEmail();

            $this->developer->create($dataUserNew);
            return $this->loginWithProvider($user, $socialAccount);

        } catch (Throwable $e) {

            return false;
        }


        //$roleToAssign = Role::findByName('candidate', 'api');


    }

    public function handleAuthenticate($user)
    {
        $user->roles = $user->roles()->first()->name;

        if ($user->roles == "collaborator") {
            // $user->contract_diff = 0;
            // $developer = Developer::with('Contract')->where("emailcandidate", $user->email)->where('statuscandidate', 'activo')->first();
            // if ($developer && count($developer->contract) > 0) {
            //     $developer->contract[0]->end_date;
            //     $edate = $developer->contract[0]->end_date;
            //     $mytime = Carbon::now();
            //     $end_date = Carbon::parse($edate);
            //     $diff = $end_date->diffInDays($mytime);
            //     if ($edate < $mytime) {
            //         $user->contract_diff = $diff;
            //     }
            // }

            $user_org = OrganizationUser::where("user_id", $user->id)->first();

            if ($user_org != null) {
                $org = Organization::where("id", $user_org->organization_id)->first();
                $user["org"] = $org;
            }
        }

        if ($user->roles == "client") {
            $org = Organization::where("user_id", $user->id)->first();
            if ($org != null) $user["org"] = $org;
        }
        
        return ResponseBuilder::success(["user" => $user]);
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::guard('web')->attempt($credentials)) {
            $user = Auth::guard('web')->user();
            return $this->handleAuthenticate($user);
        }

        return ResponseBuilder::error(101);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function RecoverPassword(Request $request)
    {
        $emailer = new EmailController;
        $usr = User::where('email', $request->email)->first();
        if ($usr != null) {
            $rp = new RecoveryPassword();
            $rp->email = $usr->email;
            $rp->remember_token = Str::random(35);
            $rp->save();
            $emailer->EmailRecoveryPassword($usr, $rp, 5, 2);

            return ResponseBuilder::success([$usr], 200);
        }
        return ResponseBuilder::error(500);
    }

    public function GetTokenRecoverPassword($token)
    {
        $rp = RecoveryPassword::where('remember_token', $token)->where('state', 'activo')->first();
        return response()->json($rp, 200);
    }

    public function UpdatePassword(Request $request)
    {
        $emailer = new EmailController;
        $rp = RecoveryPassword::where('remember_token', $request->token)->where('state', 'activo')->first();
        if ($rp != null) {
            $usr = User::where('email', $request->email)->where('status', '1')->first();
            $usr->password = Hash::make($request->password);
            $usr->save();
            $rp->state = 'inactivo';
            $rp->save();
            $emailer->EmailPasswordChangeConfirm($usr, 6);
            return response()->json(200);
        }
        return response()->json(500);
    }

    public function logout()
    {
        Auth::guard('web')->logout();

        return ResponseBuilder::success();
    }
}
