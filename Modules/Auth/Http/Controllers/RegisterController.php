<?php

namespace Modules\Auth\Http\Controllers;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Auth\Http\Requests\UserStoreRequest;
use Modules\User\Entities\User;
use Spatie\Permission\Models\Role;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Modules\Organizations\Entities\Organization;
use Modules\Organizations\Entities\OrganizationUser;
use Modules\Organizations\Http\Controllers\OrganizationUserController;

class RegisterController extends Controller
{
    private $collaboratorController;

    public function __construct(OrganizationUserController $collaboratorController)
    {
        $this->collaboratorController = $collaboratorController;
    }

    public function validEmail($email)
    {
        return User::where("email", $email)->firstOr(function () {
            return false;
        });
    }

    public function validlicenses($org_id)
    {
        $org = Organization::find($org_id);
        $org_user = OrganizationUser::where('organization_id', $org_id)->count();

        if ($org_user >= $org->licenses) {
            return false;
        }

        return true;
    }

    /**
     * createCollaborator a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function createCollaborator(UserStoreRequest $request, $org_id)
    {
        $validLicenses = $this->validlicenses($org_id);
        $validEmail = $this->validEmail($request->email);

        if ($validEmail) {
            return ResponseBuilder::error(
                400,
                null,
                [
                    "message" => "Email already exist"
                ],
            );
        }
        if (!$validLicenses) {
            return ResponseBuilder::error(
                400,
                null,
                [
                    "message" => "You already exceeded the maximum number of licenses"
                ],
            );
        }


        $roleToAssign = Role::findByName('collaborator', 'api');

        $user = $this->store($request);

        $user->assignRole($roleToAssign);
        $this->collaboratorController->createOrganizationUser($org_id, $user["id"]);

        return ResponseBuilder::success(
            [
                "id" => $user["id"],
                "uuid" => $user["uuid"]->toString(),
                "api_token" => $user["api_token"],
                "first_name" => $user["first_name"],
                "last_name" => $user["last_name"],
                "email" => $user["email"],
                "phone" => $user["phone"],
                "rol" => $user["rol"]
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(UserStoreRequest $request)
    {
        $arrUser = User::create([
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "status" => 0,
            "country_id" => $request->country_id,
            "industry_id" => $request->industry_id,
            "phone" => $request->phone,
            "rol" => $request->rol
        ]);

        return $arrUser;
    }
}
