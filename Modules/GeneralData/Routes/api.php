<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->get('/generaldata', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['analyst:api']], function () {
    /*Lenguaje/Framework */
    Route::get('lenguajeindex', 'GeneralDataController@tablalenguajesindex');
    Route::get('frameworkindex', 'GeneralDataController@tablaframeworkindex');
    Route::post('create-lenguaje', 'GeneralDataController@createlenguaje');
    Route::post('create-framework', 'GeneralDataController@createframework');
    Route::get('inactive-lenguaje-framework/{id}', 'GeneralDataController@inactivelenguajeframework');
    Route::get('active-framework/{id}', 'GeneralDataController@activelenguajeframework');
    Route::get('lenguaje-delete/{id}', 'GeneralDataController@deletelenguaje');
});


// Lista de paises
Route::get('/country', 'CountryController@index');

// Agregar un nuevo pais
Route::post('/country', 'CountryController@store');

// Eliminar un pais
Route::delete('/country/{id}', 'CountryController@destroy');

// Lista de Lenguajes
Route::get('/lenguage', 'ProgrammingLenguageController@index');

// Lista de Lenguajes
Route::get('/frameworks-for-programming-id/{programming_id}', 'GeneralDataController@getFrameworkLenguagesFilterLenguage');

// Lista de Departments
Route::get('/departments/{id}', 'GeneralDataController@getDepartments');

// Lista de Cities
Route::get('/cities/{id}', 'GeneralDataController@getCities');

// Lista de Cities
Route::get('/cities', 'GeneralDataController@getAllCities');

// Lista de Cities
Route::post('/cities', 'GeneralDataController@createCity');

// Lista de Frameworks
Route::get('/frameworks', 'GeneralDataController@getFrameworkLenguages');

// Agregar un nuevo lenguage
Route::post('/lenguage', 'ProgrammingLenguageController@store');

// Eliminar un pais
Route::delete('/lenguage/{id}', 'ProgrammingLenguageController@destroy');

// Languages List
Route::get('/languages', 'GeneralDataController@getLanguages');

// Language Levels List
Route::get('/language-levels', 'GeneralDataController@getLanguageLevels');

// TimeZones
Route::get('/timezones', 'GeneralDataController@getTimeZones');

// Crear Moneda
Route::post('/currency', 'CurrencyController@store');

// Listar Monedas
Route::get('/currency', 'CurrencyController@index');

// Crear Banco
Route::post('/bank', 'BankController@store');

// Listar Banco
Route::get('/banks', 'BankController@index');

// Listar Banco
Route::get('/bank/{id}', 'BankController@find');

Route::prefix('v2')->group(function () {
    Route::resource('industry', 'IndustryController');
    Route::resource('country', 'CountryController');
});