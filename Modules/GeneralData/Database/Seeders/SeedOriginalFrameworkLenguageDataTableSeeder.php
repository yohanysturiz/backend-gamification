<?php

namespace Modules\GeneralData\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\ProgrammingLenguage\FrameworkLenguageModel;

class SeedOriginalFrameworkLenguageDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Model::unguard();
        $file = fopen(public_path('csv/frameworks.csv'), 'r');

        while (($data = fgetcsv($file)) !== false) {
            FrameworkLenguageModel::create([
                'programming_lenguage_id' => $data[0],
                'name' => $data[1],
                'slug' => $data[2],
                'backend' => $data[3], 
                'frontend' => $data[4], 
            ]);
        }
    }
}
