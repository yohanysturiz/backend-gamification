<?php

namespace Modules\GeneralData\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\Department\DepartmentModel;

class SeedOriginalDepartmentDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         // Model::unguard();
         $file = fopen(public_path('csv/departamentos.csv'), 'r');

         while (($data = fgetcsv($file)) !== false) {
            DepartmentModel::create([
                 'country_id' => $data[0],
                 'name' => $data[1],
             ]);
         }
    }
}
