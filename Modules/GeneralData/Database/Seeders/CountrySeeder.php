<?php
namespace Modules\GeneralData\Database\Seeders;

use Modules\GeneralData\Entities\Country\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
      /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
	{
	
	}

	public function run()
	{
		// 'uuid', 'name', 'description', 'status'
		Country::create([
			'name' => "Brasil",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+55"
		]);
		
		Country::create([
			'name' => "Bahamas",
			'description' => "latino america",
			'status' => 0,
			'code_phone'=> "+1"
		]);
		
		Country::create([
			'name' => "Bolivia",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+591"
		]);
		
		Country::create([
			'name' => "Argentina",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+54"
		]);
		
		Country::create([
			'name' => "Canada",
			'description' => "norte america",
			'status' => 1,
			'code_phone'=> "+1"
		]);
		
		Country::create([
			'name' => "Chile",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+56"
		]);
		
		Country::create([
			'name' => "Colombia",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+57"
		]);
		
		Country::create([
			'name' => "Costa rica",
			'description' => "centro america",
			'status' => 1,
			'code_phone'=> "+506"
		]);

		Country::create([
			'name' => "Honduras",
			'description' => "centro america",
			'status' => 1,
			'code_phone'=> "+504"
		]);
		
		Country::create([
			'name' => "Ecuador",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+593"
		]);
		
		Country::create([
			'name' => "EEUU",
			'description' => "norte america",
			'status' => 1,
			'code_phone'=> "+1"
		]);
		
		Country::create([
			'name' => "El Salvador",
			'description' => "centro america",
			'status' => 1,
			'code_phone'=> "+503"
		]);
		
		Country::create([
			'name' => "Guatemala",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+502"
		]);
		
		Country::create([
			'name' => "Guyana",
			'description' => "latino america",
			'status' => 0,
			'code_phone'=> "+592"
		]);
		
		Country::create([
			'name' => "Haiti",
			'description' => "latino america",
			'status' => 0,
			'code_phone'=> "+509"
		]);
		
		Country::create([
			'name' => "Mexico",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+52"
		]);
		
		Country::create([
			'name' => "Nicaragua",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+505"
		]);

		Country::create([
			'name' => "Panamá",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+507"
		]);
		
		Country::create([
			'name' => "Paraguay",
			'description' => "latino america",
			'status' => 0,
			'code_phone'=> "+595"
		]);
		
		Country::create([
			'name' => "Perú",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+51"
		]);
		
		Country::create([
			'name' => "Trinidad y tobago",
			'description' => "centro america",
			'status' => 0,
			'code_phone'=> "+1"
		]);
		
		Country::create([
			'name' => "Uruguay",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+598"
		]);

		Country::create([
			'name' => "Venezuela",
			'description' => "latino america",
			'status' => 1,
			'code_phone'=> "+58"
		]);

		Country::create([
			'name' => "España",
			'description' => "europa",
			'status' => 1,
			'code_phone'=> "+34"
		]);

	}
}
