<?php

namespace Modules\GeneralData\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GeneralDataDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CountrySeeder::class);
        $this->call(SeedOriginalCitiesDataTableSeeder::class);
        $this->call(SeedOriginalProgrammingLenguageDataTableSeeder::class);
        $this->call(SeedOriginalFrameworkLenguageDataTableSeeder::class);
        $this->call(SeedOriginalDepartmentDataTableSeeder::class);
        $this->call(SeedIndustry::class);
    }
}
