<?php

namespace Modules\GeneralData\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\GeneralData\Entities\Industry\IndustryModel;

class SeedIndustry extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        IndustryModel::create(['name' => 'Government']);
        IndustryModel::create(['name' => 'Technology']);
        IndustryModel::create(['name' => 'Agriculture']);
        IndustryModel::create(['name' => 'Fintech']);
        IndustryModel::create(['name' => 'Education']);
        IndustryModel::create(['name' => 'Bank']);
    }
}
