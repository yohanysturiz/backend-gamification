<?php

namespace Modules\GeneralData\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;
use Illuminate\Database\Eloquent\Model;

class SeedOriginalProgrammingLenguageDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();
        $file = fopen(public_path('csv/LenguajesProgramacion.csv'), 'r');

        while (($data = fgetcsv($file)) !== false) {
            ProgrammingLenguagueModel::create([
                'name' => $data[0],
                'slug' => $data[1],
                'status' => $data[2]
            ]);
        }
    }
}
