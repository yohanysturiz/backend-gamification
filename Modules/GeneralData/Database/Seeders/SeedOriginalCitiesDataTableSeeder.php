<?php

namespace Modules\GeneralData\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\City\CityModel;


class SeedOriginalCitiesDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $file = fopen(public_path('csv/cities.csv'), 'r');

        while (($data = fgetcsv($file)) !== false) {
           CityModel::create([
                'country_id' => $data[0],
                'name' => $data[1],
            ]);
        }
    }
}