<?php

namespace Modules\GeneralData\Http\Requests;

use App\Http\Requests\FormRequest;

class CurrencyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'symbol' => 'required|string',
            'country_id' => 'required|integer',
        ];
    }
}
