<?php

namespace Modules\GeneralData\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;
use Modules\GeneralData\Http\Requests\LenguageStoreRequest;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

/**
 * @group Lenguage
 *
 * APIs for managing general data
 */
class ProgrammingLenguageController extends Controller
{
    /**
     * Lista de Lenguajes
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": [{
     *        "uuid": "",
     *        "name": "",
     *        "description": "",
     *        "slug": "",
     *        "status": "",
     *        "backend": "",
     *        "frontend": "",
     *        "type": ""
     *    }]
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $arrLengugage = ProgrammingLenguagueModel::where("status", 1)->get();
        return response()->json($arrLengugage, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('generaldata::create');
    }

    /**
     * Crear Lenguaje de Programacion
     *
     * @bodyParam name string required Nombre del lenguaje Ejm: ASP Clasico
     * @bodyParam description string Descripcion del lenguaje Ejm: Asp lenguaje de progracion backend
     * @bodyParam slug string required Nombre para hacer el filtro o la busqueda del lenguaje Ejm: asp-clasico
     * @bodyParam status integer required Estatus del lenguaje para ser mostrado en las consultas o listas Ejm: 1
     * @bodyParam backend boolean required Boolean para identificar si es un lenguaje para el frontend o el backend Ejm: 1
     * @bodyParam frontend boolean required Boolean para identificar si es un lenguaje para el frontend o el backend Ejm: 0
     * @bodyParam type string Campo que puede ser usado para cualquier filtro Ejm: 0
     * @response {
     *     "success": true,
     *     "code": 201,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *        "uuid": "",
     *        "name": "",
     *        "description": "",
     *        "slug": "",
     *        "status": "",
     *        "backend": "",
     *        "frontend": "",
     *        "type": ""
     *    }
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(LenguageStoreRequest $request)
    {
        //
        $arrLenguage = ProgrammingLenguagueModel::create([
			'name' => $request->name,
			'description' => $request->description,
            'slug' => $request->slug,
            'status' => 1,
            'backend' => $request->backend,
            'frontend' => $request->frontend,
            'type' => $request->type,
        ]);

        return ResponseBuilder::success($arrLenguage);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('generaldata::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('generaldata::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Eliminar Lenguage
     *
     * @param string $uuid required Variable que debe ser enviada para eliminar el lenguage Ejm: ASQEQWD3131AQSA
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *
     *    }
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        //
        ProgrammingLenguagueModel::where("uuid", $id)->delete();

        return ResponseBuilder::success();

    }
}
