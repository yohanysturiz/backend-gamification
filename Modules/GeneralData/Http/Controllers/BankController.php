<?php

namespace Modules\GeneralData\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GeneralData\Entities\Banks\BanksModel;
use Modules\GeneralData\Http\Requests\BankStoreRequest;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

/**
 * @group Banks
 *
 * APIs for managing general data
 */
class BankController extends Controller
{
    /**
     * Lista de Bancos
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": [{
     *        "uuid": "",
     *        "name": "",
     *        "description": "",
     *        "status": ""
     *    }]
     * }
     *
     *
     * @param BankStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $arrBanks["banks"] = BanksModel::where("type", "bank")->get();
        $arrBanks["paypal"] = BanksModel::where("type", "paypal")->first();
        $arrBanks["nequi"] = BanksModel::where("type", "nequi")->first();

        return response()->json($arrBanks, 200);
    }

    /**
     * Obtener Banco
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *        "uuid": "",
     *        "name": "",
     *        "description": "",
     *        "status": ""
     *    }
     * }
     *
     *
     * @param BankStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function find($id)
    {
        $objBank = BanksModel::find($id);
        $objBank->currency = $objBank->currency;

        return response()->json($objBank, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('generaldata::create');
    }

    /**
     * Crear Banco
     *
     * @bodyParam name string required Nombre del pais Ejm: Escocia
     * @bodyParam description string required Descripcion del pais Ejm: Europa
     *
     * @response {
     *     "success": true,
     *     "code": 201,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *        "uuid": "ASWDA6547875ACS"
     *    }
     * }
     *
     *
     * @param BankStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(BankStoreRequest $request)
    {
        //
        $arrBank = BanksModel::create([
			'name' => $request->name,
			'type' => $request->type,
            'currency_id' => $request->currency_id,
			'country_id' => $request->country_id
        ]);

        return ResponseBuilder::success(["name" => $arrBank->name]);
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Eliminar Banco
     *
     * @bodyParam name string required Nombre del pais Ejm: Escocia
     * @bodyParam description string required Descripcion del pais Ejm: Europa
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *
     *    }
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        //
        BanksModel::where("id", $id)->delete();

        return ResponseBuilder::success();

    }
}
