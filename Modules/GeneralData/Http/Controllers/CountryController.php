<?php

namespace Modules\GeneralData\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GeneralData\Entities\Country\Country;
use Modules\GeneralData\Http\Requests\CountryStoreRequest;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

/**
 * @group Country
 *
 * APIs for managing general data
 */
class CountryController extends Controller
{
    /**
     * Lista de Paises
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": [{
     *        "uuid": "",
     *        "name": "",
     *        "description": "",
     *        "status": ""
     *    }]
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $arrCountry = Country::where('status', 1)->orderBy('name', 'ASC')->get();
        return response()->json($arrCountry, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('generaldata::create');
    }

    /**
     * Crear Pais
     *
     * @bodyParam name string required Nombre del pais Ejm: Escocia
     * @bodyParam description string required Descripcion del pais Ejm: Europa
     *
     * @response {
     *     "success": true,
     *     "code": 201,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *        "uuid": "ASWDA6547875ACS"
     *    }
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(CountryStoreRequest $request)
    {
        //
        $arrCountry = Country::create([
			'name' => $request->name,
			'description' => $request->description,
			'status' => 1
        ]);

        return ResponseBuilder::success(["uuid" => $arrCountry->uuid->toString()]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('generaldata::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('generaldata::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Eliminar Pais
     *
     * @bodyParam name string required Nombre del pais Ejm: Escocia
     * @bodyParam description string required Descripcion del pais Ejm: Europa
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *
     *    }
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        //
        Country::where("uuid", $id)->delete();

        return ResponseBuilder::success();

    }
}
