<?php

namespace Modules\GeneralData\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GeneralData\Entities\Currency\CurrencyModel;
use Modules\GeneralData\Http\Requests\CurrencyStoreRequest;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

/**
 * @group Banks
 *
 * APIs for managing general data
 */
class CurrencyController extends Controller
{
    /**
     * Lista de Monedas
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": [{
     *        "uuid": "",
     *        "name": "",
     *        "description": "",
     *        "status": ""
     *    }]
     * }
     *
     *
     * @param CurrencyStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $arrBanks = CurrencyModel::all();
        return response()->json($arrBanks, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('generaldata::create');
    }

    /**
     * Crear Moneda
     *
     * @bodyParam name string required Nombre del pais Ejm: Escocia
     * @bodyParam description string required Descripcion del pais Ejm: Europa
     *
     * @response {
     *     "success": true,
     *     "code": 201,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *        "uuid": "ASWDA6547875ACS"
     *    }
     * }
     *
     *
     * @param CurrencyStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(CurrencyStoreRequest $request)
    {
        //
        $arrCurrency = CurrencyModel::create([
			'name' => $request->name,
			'symbol' => $request->symbol,
			'country_id' => $request->country_id
        ]);

        return ResponseBuilder::success(["name" => $arrCurrency->name]);
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Eliminar Moneda
     *
     * @bodyParam name string required Nombre del pais Ejm: Escocia
     * @bodyParam description string required Descripcion del pais Ejm: Europa
     *
     * @response {
     *     "success": true,
     *     "code": 200,
     *     "locale": "en",
     *     "message": "OK",
     *     "data": {
     *
     *    }
     * }
     *
     *
     * @param CountryStoreRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        //
        CurrencyModel::where("id", $id)->delete();

        return ResponseBuilder::success();

    }
}
