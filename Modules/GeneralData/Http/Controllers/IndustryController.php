<?php

namespace Modules\GeneralData\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GeneralData\Entities\Industry\IndustryModel;

/**
 * @group Industries
 *
 * APIs for managing general data
 */
class IndustryController extends Controller
{
    public function index()
    {
        $industries = IndustryModel::all();

        return response()->json($industries, 200);
    }

    public function find($id)
    {
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('generaldata::create');
    }

    public function store(Request $request)
    {
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
    }
}
