<?php

namespace Modules\GeneralData\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Modules\GeneralData\Entities\Language;
use Modules\GeneralData\Entities\LanguageLevel;
use Modules\GeneralData\Entities\ProgrammingLenguage\FrameworkLenguageModel;
use Modules\GeneralData\Entities\Department\DepartmentModel;
use Modules\GeneralData\Entities\City\CityModel;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;
use Modules\GeneralData\Entities\TimeZone;

class GeneralDataController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('generaldata::index');
    }

    /**
     * Display a listing of the Frameworks.
     * @return Response
     */
    public function getFrameworkLenguages()
    {
        $frameworks = FrameworkLenguageModel::orderBy('name', 'ASC')->get();
        return response()->json($frameworks, 200);
    }

    /**
     * Display a listing of the Frameworks.
     * @return Response
     */
    public function getFrameworkLenguagesFilterLenguage(Request $request, $programming_id)
    {
        $arr_programming_id = explode(",", $programming_id);
        $frameworks = FrameworkLenguageModel::whereIn('programming_lenguage_id', $arr_programming_id)->orderBy('name', 'ASC')->get();
        return response()->json($frameworks, 200);
    }

    /**
     * Display a listing of the Languages.
     * @return JsonResponse
     */
    protected function getLanguages(): JsonResponse
    {
        $languages = Language::all();
        return response()->json($languages, 200);
    }

    /**
     * Display a listing of the Language Levels.
     * @return JsonResponse
     */
    protected function getLanguageLevels(): JsonResponse
    {
        $languages = LanguageLevel::all();
        return response()->json($languages, 200);
    }

    /**
     * Display a listing of the Language Levels.
     * @return JsonResponse
     */
    protected function getTimeZones(): JsonResponse
    {
        $timezones = TimeZone::all();
        return response()->json($timezones, 200);
    }

    /**
     * Display a listing of the Departments.
     * @return Response
     */
    public function getDepartments($id)
    {
        $departments = DepartmentModel::where('country_id', $id)->orderBy('name', 'ASC')->get();
        return response()->json($departments, 200);
    }

    /**
     * Display a listing of the Cities.
     * @return Response
     */
    public function getCities($id)
    {
        $cities = CityModel::where("country_id", $id)->orderBy('name', 'ASC')->get();
        return response()->json($cities, 200);
    }

    /**
     * Display a listing of the Cities.
     * @return Response
     */
    public function getAllCities()
    {
        $cities = CityModel::orderBy('name', 'ASC')->get();
        return response()->json($cities, 200);
    }

    /**
     * Display a listing of the Cities.
     * @return Response
     */
    public function createCity(Request $request)
    {
        $arrCity = CityModel::create([
            'name' => $request->name,
            'department_id' => $request->department_id,
            'status' => 1
        ]);

        return ResponseBuilder::success(["uuid" => $arrCity->uuid->toString()]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('generaldata::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('generaldata::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('generaldata::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function tablalenguajesindex()
    {
        $arrRequest = ProgrammingLenguagueModel::orderBy('id', 'DESC')->paginate(50);
        return [
            'pagination' => [
                'total' => $arrRequest->total(),
                'current_page' => $arrRequest->currentPage(),
                'per_page' => $arrRequest->perPage(),
                'last_page' => $arrRequest->lastPage(),
                'from' => $arrRequest->firstItem(),
                'to' => $arrRequest->lastPage(),

            ],
            'arrRequest' => response()->json($arrRequest, 200)
        ];
    }


    public function tablaframeworkindex()
    {

        $arrRequest = FrameworkLenguageModel::orderBy('id', 'DESC')->with('lenguaje')->paginate(50);
        return [
            'pagination' => [
                'total' => $arrRequest->total(),
                'current_page' => $arrRequest->currentPage(),
                'per_page' => $arrRequest->perPage(),
                'last_page' => $arrRequest->lastPage(),
                'from' => $arrRequest->firstItem(),
                'to' => $arrRequest->lastPage(),

            ],
            'arrRequest' => response()->json($arrRequest, 200)
        ];
    }


    public function createlenguaje(request $request)
    {

        $lenguaje = $request->input();
        $lenguaje["status"] = "1";
        $lenguaje["backend"] = "0";
        $lenguaje["frontend"] = "0";
        $lenguaje = ProgrammingLenguagueModel::create($lenguaje);
        return response()->json($lenguaje, 200);
    }


    public function deletelenguaje($id)
    {

        $lenguaje = FrameworkLenguageModel::where('programming_lenguage_id', $id)->first();
        if (!empty($lenguaje)) {
            $data = ['type' => 'error', 'title' => 'Ya se encuentra registrado el lenguaje a un framework'];
            return response()->json($data, 200);
        }
        $lenguajedelete = ProgrammingLenguagueModel::where('id', $id)->delete();
        return response()->json($lenguajedelete, 200);
    }


    public function createframework(request $request)
    {

        $lenguaje = $request->input();
        $lenguaje["status"] = "1";
        $lenguaje["backend"] = "0";
        $lenguaje["frontend"] = "0";
        $lenguaje = FrameworkLenguageModel::create($lenguaje);
        return response()->json($lenguaje, 200);
    }


    public function inactivelenguajeframework($id)
    {
        $statusframework = FrameworkLenguageModel::where('id', $id)->update(['status' => "0"]);
        return response()->json($statusframework, 200);
    }


    public function activelenguajeframework($id)
    {
        $statusframework = FrameworkLenguageModel::where('id', $id)->update(['status' => "1"]);
        return response()->json($statusframework, 200);
    }

}
