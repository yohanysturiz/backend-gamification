<?php

namespace Modules\GeneralData\Entities;

use Illuminate\Database\Eloquent\Model;

class LanguageLevel extends Model
{
    protected $fillable = ['name'];
}
