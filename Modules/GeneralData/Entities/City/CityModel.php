<?php

namespace Modules\GeneralData\Entities\City;

use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\Country\Country;
use Ramsey\Uuid\Uuid;

class CityModel extends Model
{
    protected $fillable = ['uuid', 'country_id', 'department_id', 'name', 'description', 'status'];

    protected $table = "cities";

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function country()
    {
        return $this->belongsTo(Country::class, "country_id");
    }
 
}
