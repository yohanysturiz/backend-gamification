<?php

namespace Modules\GeneralData\Entities\City;

use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\Department\DepartmentModel;
use Modules\Developer\Entities\Developer;
use Ramsey\Uuid\Uuid;

class City extends Model
{
    protected $fillable = ['uuid', 'department_id', 'name', 'description'];

    protected $table = "city";

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function department()
    {
        return $this->belongsTo(DepartmentModel::class);
    }

    public function developers()
    {
        return $this->hasMany(Developer::class);
    }
}
