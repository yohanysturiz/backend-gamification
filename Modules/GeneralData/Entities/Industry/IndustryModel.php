<?php

namespace Modules\GeneralData\Entities\Industry;

use Illuminate\Database\Eloquent\Model;

class IndustryModel extends Model
{
    protected $fillable = [
        'id',
        'name',
        'description',
        'is_active'
    ];

    protected $hidden = [
        'deleted_by',
        'updated_by',
        'created_by',
        'updated_at',
        'deleted_at',
    ];

    protected $table = "industry";
}
