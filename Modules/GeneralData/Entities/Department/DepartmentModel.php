<?php

namespace Modules\GeneralData\Entities\Department;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\GeneralData\Entities\City\City;
use Modules\GeneralData\Entities\Country\Country;
use Ramsey\Uuid\Uuid;

class DepartmentModel extends Model
{
    use SoftDeletes;

    protected $fillable = ['uuid', 'country_id', 'name', 'description', 'status'];

    protected $table = "department";

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
