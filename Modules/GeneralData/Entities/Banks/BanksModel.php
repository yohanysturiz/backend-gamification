<?php

namespace Modules\GeneralData\Entities\Banks;

use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\Country\Country;
use Modules\GeneralData\Entities\Currency\CurrencyModel;

class BanksModel extends Model
{
    protected $fillable = ['id', 'name', 'type', 'country_id', 'currency_id'];

    protected $table = "banks";

    public function country()
    {
        return $this->belongsTo(Country::class, "country_id");
    }

    public function currency()
    {
        return $this->belongsTo(CurrencyModel::class, "currency_id");
    }
}