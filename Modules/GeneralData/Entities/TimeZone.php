<?php

namespace Modules\GeneralData\Entities;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{
    protected $table = "timezones";
    protected $fillable = ['name'];
}
