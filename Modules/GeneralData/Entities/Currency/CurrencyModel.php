<?php

namespace Modules\GeneralData\Entities\Currency;

use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\Country\Country;

class CurrencyModel extends Model
{
    protected $fillable = ['id', 'name', 'symbol', 'country_id'];

    protected $table = "currency";

    public function country()
    {
        return $this->belongsTo(Country::class, "country_id");
    }
}