<?php

namespace Modules\GeneralData\Entities\Country;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\GeneralData\Entities\Department\DepartmentModel;
use Ramsey\Uuid\Uuid;

class Country extends Model
{
    use SoftDeletes;

    protected $table = "country";

    protected $fillable = [
        'uuid', 'name', 'description', 'status'
    ];

    protected $hidden = [
        'deleted_by',
        'updated_by',
        'created_by',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function departments()
    {
        return $this->hasMany(DepartmentModel::class);
    }

}
