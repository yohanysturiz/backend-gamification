<?php

namespace Modules\GeneralData\Entities\ProgrammingLenguage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use Modules\GeneralData\Entities\ProgrammingLenguagueModel;

class FrameworkLenguageModel extends Model
{
    use SoftDeletes;

    protected $fillable = ['uuid', 'programming_lenguage_id', 'name', 'description', 'slug', 'status', 'backend', 'frontend', 'type'];

    protected $table = "framework_lenguage";

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function lenguaje() {

        return $this->belongsTo('Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel', 'programming_lenguage_id');
    }
}
