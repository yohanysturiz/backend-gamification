<?php

namespace Modules\GeneralData\Entities\ProgrammingLenguage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\CustomersRequest;
use Ramsey\Uuid\Uuid;

class ProgrammingLenguagueModel extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'slug', 'status', 'backend', 'frontend', 'type'];

    protected $table = "programming_lenguage";

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function Developers()
    {
        return $this->belongsToMany(Developer::class);
    }

    public function CustomerRequest()
    {
        return $this->belongsToMany(CustomersRequest::class);
    }

}
