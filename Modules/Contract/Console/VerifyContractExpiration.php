<?php

namespace Modules\Contract\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Clients\Entities\Contract;
use Illuminate\Support\Carbon;
use Modules\Contract\Notifications\ContractExpiration;


class VerifyContractExpiration extends Command
{


    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'contract:verify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the contracts of the developers to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $developerContracts = Contract::where([
            ['user_id', '>', 0],
            ['status', '=', 'aprobado']
        ])->get();

        foreach ($developerContracts as $contract) {
            $expirationContract = new Carbon($contract->end_date);
            $diff = Carbon::now()->diffInDays($expirationContract);
            $this->info($diff);
            if ($diff = 29 || $diff = 14 || $diff = 2) {
                $contract->notify(new ContractExpiration());
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
