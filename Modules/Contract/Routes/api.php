<?php

use Illuminate\Http\Request;
use Modules\User\Entities\User;
use Modules\Contract\Notifications\ContractExpiration;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['auth:api']], function () {
    Route::get('get-developer-contracts/{developer_id}', 'ContractController@getDeveloperContracts');
    Route::post('contract/create/{id}/{type}', 'ContractController@store');
    Route::post('contract/update/{id}', 'ContractController@update');
    Route::post('contract/assigment-update', 'ContractController@assignmentDateChange');
    Route::get('contracts/verify-contracts', 'ContractController@verifyContracts');
});


Route::post('testSlack',function(){
    $prueba = User::find(1);
    $prueba->notify(new ContractExpiration());

    return response()->json($prueba);
});

Route::post('contract/re-send/{uuid}', 'ContractController@reSendEmailContract');
Route::get('contracts/logs/{id}', 'ContractController@getLogContracts');
Route::get('contracts/log/download/{uuid}', 'ContractController@downloadContractOld');