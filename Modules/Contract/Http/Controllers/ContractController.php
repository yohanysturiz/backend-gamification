<?php

namespace Modules\Contract\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Modules\Clients\Entities\Contract;
use Modules\Clients\Entities\ContractLogs;
use Modules\Clients\Entities\Customer;
use Modules\Developer\Entities\Developer;
use Modules\Document\Entities\Document;
use Modules\GeneralData\Entities\City\CityModel;
use Modules\User\Entities\User;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Model\SendSmtpEmail;

class ContractController extends Controller
{

    protected $config;
    protected $smtpApiInstance;
    protected $contactApiInstance;

    public function __construct()
    {
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', config('app.sendin'));
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('partner-key', config('app.sendin'));

        // Configure SMTP API key authorization: api-key
        $this->smtpApiInstance = new SMTPApi(new GuzzleClient(), $this->config);
        $contactApiInstance = new ContactsApi(new GuzzleClient, $this->config);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function getDeveloperContracts($developer_id)
    {
        $contracts = Contract::where('user_id', $developer_id)->with('Developer')->get();
        return response()->json($contracts, 200);
    }

    public function generateContratorPDF($id, $base64 = false)
    {
        $ContractPdf = Contract::where('uuid', $id)->with('Customer', 'Developer', 'Cityresidence', 'Cityexpedition')->first();
        $user = User::where('id', $ContractPdf->user_id)->first();
        if ($ContractPdf->type == "developer") {
            $user = User::where("email", $ContractPdf->developer->emailcandidate)->first();
        } else {
            $customer = Customer::where("email_company", $ContractPdf->customer->email_company)->first();
            $user = User::where("email", $customer->email_company)->first();
        }

        $full_name = $user->first_name . " " . $user->last_name;
        $format_start_contract = Carbon::parse($ContractPdf->start_date);
        $ContractPdf->format_start_contract = "{$format_start_contract->day} de {$format_start_contract->monthName} del {$format_start_contract->year}";

        $content_contract = $ContractPdf->content_contract;
        $content_contract = str_replace('$full_name', mb_strtoupper($full_name), $content_contract);
        $content_contract = str_replace('$type_document', mb_strtoupper($user->type_document), $content_contract);
        $content_contract = str_replace('$number_document', $user->number_document, $content_contract);
        $content_contract = str_replace('$city_expedition_doc', $user->city_expedition_document, $content_contract);
        $content_contract = str_replace('$city_resident', $user->city_expedition_document, $content_contract);
        $content_contract = str_replace('$lenguage_programming', mb_strtoupper($ContractPdf->programming_lenguage), $content_contract);
        $content_contract = str_replace('$contract_duration', $ContractPdf->contract_duration, $content_contract);
        $content_contract = str_replace('$contract_value_letter', mb_strtoupper($ContractPdf->contract_value), $content_contract);
        $content_contract = str_replace('$contract_value_number', number_format($ContractPdf->contract_value_number, 2, ',', '.'), $content_contract);
        $content_contract = str_replace('$start_date', $ContractPdf->format_start_contract, $content_contract);


        if ($ContractPdf->candidate_signed == 1) {
            if ($ContractPdf->type == "developer") {
                $user = User::where("email", $ContractPdf->developer->emailcandidate)->first();
            } else {
                $customer = Customer::where("email_company", $ContractPdf->customer->email_company)->first();
                $user = User::where("email", $customer->email_company)->first();
            }

            $insDocumentFirm = Document::where("type_document", "firma_digital")->where("user_id", $user->id)->first();

            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(10));
            $ContractPdf->firm = $file_firm;

            $format_date_firm = Carbon::parse($ContractPdf->date_firm);
            $ContractPdf->format_date_firm = "{$format_date_firm->day} de {$format_date_firm->monthName} del {$format_date_firm->year}";
        }


        Log::debug("TYPE_CONTRACT", [$ContractPdf->type_contract]);

        switch ($ContractPdf->type_contract) {
            case "0":
                $name = "Contracto_Prestacion_de_Servicio" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.Contract_clients_servicios', compact('ContractPdf'))->save($name);
                break;
            case "1":
                $name = "Contracto_Prestación_de_Servicios" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.contract-pdf', compact('content_contract', 'ContractPdf', 'user'))->save($name);
                break;
            case "Termino Indefinido":
                $name = "Contracto_Termino_Indefinido" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.termino-indefinido', compact('ContractPdf'))->save($name);
                break;
            case "Mandato":
                $name = "Contracto_Mandato" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.mandato', compact('ContractPdf'))->save($name);
                break;
            default:
                break;
        }

        if ($base64) return base64_encode(file_get_contents($name));

        return $pdf->stream($name);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, $id, $type)
    {
        $customer = Customer::where('uuid', $id)->first();

        $contractForm = $request->input();
        $contractForm["type"] = $request->type_user;
        $contractForm["client_id"] = $type == "client" ? $customer->id : 0;
        $contractForm["user_id"] = $id;
        $contractForm["status"] = "pendiente";
        $contractForm["candidate_signed"] = 0;
        $contractForm["type_contract"] = $request->type_contract;
        $contractForm["type_document"] = $request->type_document_representative_legal;
        $contractForm["number_document"] = $request->number_document_representative_legal;
        $contractForm["programming_lenguage"] = $request->lenguage_programming;
        $contractForm["city_id"] = CityModel::where("name", $request->city_expedition_name_representative_legal)->first()->id;
        $contractForm["date_firm"] = Carbon::now();
        $contractForm["contract_duration"] = $this->calculateDaysOfContract($contractForm);

        $insContract = Contract::create($contractForm);
        $insContract->hash = "{$insContract->uuid}{$insContract->client_id}{$insContract->user_id}";
        $insContract->update();

        return response()->json($insContract, 200);
    }


    public function reSendEmailContract(Request $request, $uuid)
    {
        $insContract = Contract::where("uuid", $uuid)->first();
        $this->sendEmailAsignDev($insContract);

        return response()->json($insContract, 200);
    }

    /**
     * Update Assigment Date
     * @param Request $request
     * @return JsonResponse
     */
    public function assignmentDateChange(Request $request): JsonResponse
    {

        $assigment = DB::table('customer_candidate_pivot')
            ->where('customer_id', $request->customer_id)
            ->where('developer_id', $request->developer_id)
            ->update(array('created_at' => Carbon::parse($request->created_at)->format('Y-m-d H:i:s')));

        if ($assigment) {
            return response()->json(['success' => true, 'message' => 'Fecha actualizada'], 201);
        } else {
            return response()->json(['success' => true, 'message' => 'Fecha no actualizada'], 201);
        }
    }


    /**
     * Function for calculate days duration of contract
     */
    protected function calculateDaysOfContract($data)
    {
        $start_date = new Carbon($data["start_contract_date"]);
        $end_date = new Carbon($data["end_date"]);
        $monts = ($start_date->diff($end_date)->days < 1)
            ? 'today'
            : $end_date->diffInMonths($start_date);

        if ($monts == 0) {
            $days = ($start_date->diff($end_date)->days < 1)
                ? 'today'
                : $end_date->diffInDays($start_date);

            return "{$days} Días";
        }

        if ($monts == 1) return "{$monts} Mes";

        return "{$monts} Meses";
    }


    /**
     * Send email notification by create contract for customer
     */
    protected function sendEmailAsignDev($insContract)
    {
        $contract_name = $this->getTypeContract($insContract->type_contract);
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $insContract->developer->emailcandidate));
        $sendSmtpEmail['templateId'] = 12;

        $sendSmtpEmail['attachment'] = array(array(
            "name" => $contract_name,
            "content" => $this->generateContratorPDF($insContract->uuid, true)
        ));

        $sendSmtpEmail['params'] = array(
            'nombre' => $insContract->developer->namecandidate,
            'link' => config("app.url_client") . "/#/candidate-signature/{$insContract->uuid}",
            'hash' => $insContract->hash
        );

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return;
    }

    protected function getTypeContract($type)
    {
        switch ($type) {
            case 'Termino Indefinido':
                $name = 'Contrato_Termino_Indefinido.pdf';
                break;
            case 'Mandato':
                $name = 'Contrato_Mandato.pdf';
                break;
            case 'Prestacion de Servicio Client':
                $name = 'Contrato_Prestacion_de_servicios_cliente.pdf';
                break;
            default:
                $name = "Contrato_Prestación_de_Servicios.pdf";
                break;
        }

        return $name;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('contract::show');
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $contract = Contract::find($id);
        $contract->content_contract = $request->content_contract;
        return $contract->update();
    }

    /**
     * Verify Developers Contracts
     * @return JsonResponse
     */
    public function verifyContracts(): JsonResponse
    {

        $developerContracts = Contract::select("contract.id", "contract.end_date", DB::raw("CONCAT(developers.namecandidate,' ',developers.lastnamecandidate) as full_name"))
            ->join('developers', function ($join) {
                $join->on('developers.id', '=', 'contract.user_id');
            })->where([
                ['contract.user_id', '>', 0],
                ['status', '=', 'aprobado']
            ])->orderBy('contract.end_date', 'asc')->get();

        $data = [];
        foreach ($developerContracts as $contract) {
            $expirationContract = new \Illuminate\Support\Carbon($contract->end_date);
            $diff = Carbon::now()->diffInDays($expirationContract);
            if ($diff < 60) {
                $data[] = $contract;
            }
        }
        return response()->json(['success' => true, 'contracts' => $data], 201);
    }


    public function getLogContracts(Request $request, $uuid)
    {

        $insContract = Contract::where("uuid", $uuid)->first();
        $insContractLogs = ContractLogs::where("contract_id", $insContract->id)->get();

        return $insContractLogs;
    }

    public function generateOldContratorPDF($id, $base64 = false)
    {
        $ContractPdf = Contract::where('uuid', $id)->with('Customer', 'Developer', 'Cityresidence', 'Cityexpedition')->first();
        $user = User::where('id', $ContractPdf->user_id)->first();

        if ($ContractPdf->type == "developer") {
            $user = User::where("email", $ContractPdf->developer->emailcandidate)->first();
        } else {
            $customer = Customer::where("email_company", $ContractPdf->customer->email_company)->first();
            $user = User::where("email", $customer->email_company)->first();
        }

        $full_name = $user->first_name . " " . $user->last_name;
        $format_start_contract = Carbon::parse($ContractPdf->start_date);
        $ContractPdf->format_start_contract = "{$format_start_contract->day} de {$format_start_contract->monthName} del {$format_start_contract->year}";

        $content_contract = $ContractPdf->content_contract;
        $content_contract = str_replace('$full_name', mb_strtoupper($full_name), $content_contract);
        $content_contract = str_replace('$type_document', mb_strtoupper($user->type_document), $content_contract);
        $content_contract = str_replace('$number_document', $user->number_document, $content_contract);
        $content_contract = str_replace('$city_expedition_doc', $user->city_expedition_document, $content_contract);
        $content_contract = str_replace('$city_resident', $user->city_expedition_document, $content_contract);
        $content_contract = str_replace('$lenguage_programming', mb_strtoupper($ContractPdf->programming_lenguage), $content_contract);
        $content_contract = str_replace('$contract_duration', $ContractPdf->contract_duration, $content_contract);
        $content_contract = str_replace('$contract_value_letter', mb_strtoupper($ContractPdf->contract_value), $content_contract);
        $content_contract = str_replace('$contract_value_number', number_format($ContractPdf->contract_value_number, 2, ',', '.'), $content_contract);
        $content_contract = str_replace('$start_date', $ContractPdf->format_start_contract, $content_contract);


        if ($ContractPdf->candidate_signed == 1) {
            if ($ContractPdf->type == "developer") {
                $user = User::where("email", $ContractPdf->developer->emailcandidate)->first();
            } else {
                $customer = Customer::where("email_company", $ContractPdf->customer->email_company)->first();
                $user = User::where("email", $customer->email_company)->first();
            }

            $insDocumentFirm = Document::where("type_document", "firma_digital")->where("user_id", $user->id)->first();

            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(10));
            $ContractPdf->firm = $file_firm;

            $format_date_firm = Carbon::parse($ContractPdf->date_firm);
            $ContractPdf->format_date_firm = "{$format_date_firm->day} de {$format_date_firm->monthName} del {$format_date_firm->year}";
        }


        Log::debug("TYPE_CONTRACT", [$ContractPdf->type_contract]);

        switch ($ContractPdf->type_contract) {
            case "0":
                $name = "Contracto_Prestacion_de_Servicio" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.Contract_clients_servicios', compact('ContractPdf'))->save($name);
                break;
            case "1":
                $name = "Contracto_Prestación_de_Servicios" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.contract-pdf', compact('content_contract', 'ContractPdf', 'user'))->save($name);
                break;
            case "Termino Indefinido":
                $name = "Contracto_Termino_Indefinido" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.termino-indefinido', compact('ContractPdf'))->save($name);
                break;
            case "Mandato":
                $name = "Contracto_Mandato" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.mandato', compact('ContractPdf'))->save($name);
                break;
            default:
                break;
        }

        if ($base64) return base64_encode(file_get_contents($name));

        return $pdf->stream($name);
    }

    public function downloadContractOld(Request $request, $uuid)
    {
        $insContractLogs = ContractLogs::where("uuid", $uuid)->first();
        $old_data = json_decode($insContractLogs->old_data);

        $ContractPdf = $old_data;
        $developer = Developer::where('id', $old_data->user_id)->first();
        $user = User::where('id', $developer->user_id)->first();

        $full_name = $user->first_name . " " . $user->last_name;
        $format_start_contract = Carbon::parse($ContractPdf->start_date);
        $ContractPdf->format_start_contract = "{$format_start_contract->day} de {$format_start_contract->monthName} del {$format_start_contract->year}";

        $content_contract = $ContractPdf->content_contract;
        $content_contract = str_replace('$full_name', mb_strtoupper($full_name), $content_contract);
        $content_contract = str_replace('$type_document', mb_strtoupper($user->type_document), $content_contract);
        $content_contract = str_replace('$number_document', $user->number_document, $content_contract);
        $content_contract = str_replace('$city_expedition_doc', $user->city_expedition_document, $content_contract);
        $content_contract = str_replace('$city_resident', $user->city_expedition_document, $content_contract);
        $content_contract = str_replace('$lenguage_programming', mb_strtoupper($ContractPdf->programming_lenguage), $content_contract);
        $content_contract = str_replace('$contract_duration', $ContractPdf->contract_duration, $content_contract);
        $content_contract = str_replace('$contract_value_letter', mb_strtoupper($ContractPdf->contract_value), $content_contract);
        $content_contract = str_replace('$contract_value_number', number_format($ContractPdf->contract_value_number, 2, ',', '.'), $content_contract);
        $content_contract = str_replace('$start_date', $ContractPdf->format_start_contract, $content_contract);


        if ($ContractPdf->candidate_signed == 1) {
            $insDocumentFirm = Document::where("type_document", "firma_digital")->where("user_id", $user->id)->first();
            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(10));
            $ContractPdf->firm = $file_firm;

            $format_date_firm = Carbon::parse($ContractPdf->date_firm);
            $ContractPdf->format_date_firm = "{$format_date_firm->day} de {$format_date_firm->monthName} del {$format_date_firm->year}";
        }

        $ContractPdf->format_date_firm = "no ha firmado";

        switch ($ContractPdf->type_contract) {
            case "1":
                $name = "Contracto_Prestación_de_Servicios" . strftime("%d de %B del %Y", strtotime(date("r"))) . "pdf";
                $pdf = PDF::loadView('clients::pdf.contract-pdf', compact('content_contract', 'ContractPdf', 'user'))->save($name);
                break;
            default:
                break;
        }

        return $pdf->stream($name);
    }
}
