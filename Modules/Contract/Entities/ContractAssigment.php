<?php

namespace Modules\Contract\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Clients\Entities\Customer;
use Modules\Developer\Entities\Developer;

class ContractAssigment extends Model
{
    protected $fillable = ['customer_id', 'developer_id', 'created_at'];
    protected $table = "customer_candidate_pivot";

    public function customer(): BelongsToMany
    {
        return $this->belongsToMany(Customer::class);
        return $this->belongsToMany(Developer::class);
    }
}
