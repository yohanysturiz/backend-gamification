<?php

namespace Modules\Contract\Events;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Modules\Clients\Entities\Contract;
use Modules\Clients\Entities\Customer;

class Datefinished
{
    use SerializesModels;

   /**
     * @var Customer
     */
    public $Customer;

    /**
     * @var Contract
     */
    public $contract;

    /**
     * Create a new event instance.
     *
     * @param Contract $developer
     * @param Request $request
     */
    public function __construct()
    {

    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
