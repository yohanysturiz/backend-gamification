<?php

namespace Modules\Contract\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Support\Carbon;


class ContractExpiration extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($channel = "#contratos")
    {
        
        $this->channel = $channel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {

        $developer = $notifiable->Developer;
        $expirationDate = new Carbon($notifiable->end_date);
        $message = "El contrato de {$developer->fullName()} esta proximo a vencer. Fecha de vencimiento: {$expirationDate->toFormattedDateString()} ";
        return (new SlackMessage)
            ->from('Imuko APP')
            ->to($this->channel)
            ->content($message);
    }



    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
