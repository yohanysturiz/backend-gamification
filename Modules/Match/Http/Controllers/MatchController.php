<?php

namespace Modules\Match\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Developer\Entities\Developer;

class MatchController extends Controller
{
    /**
     * Busca coincidencias con desarrolladores que pueden tomar una vacante
     */
    public function searchDeveloper(Request $request, $id)
    {

        $arrLenguageFilter = explode(",", $id);
        $match = Developer::select('developers.*')->join('developer_programming_lenguague_model', 'developers.id', '=', 'developer_programming_lenguague_model.developer_id')
            ->whereIn('developer_programming_lenguague_model.programming_lenguague_model_id', $arrLenguageFilter);

        foreach (Developer::$filters as $key => $filter) {
            if ($request->has($key)) {
                $value = $request->input($key);
                $match = call_user_func([$match, $filter], $value);
            }
        }

        $match = $match->distinct('developers.emailcandidate')->limit(6)->orderBy('id', 'DESC')->with('lenguages')->get();


        return response()->json($match, 200);
    }



    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('match::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('match::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('match::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('match::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
