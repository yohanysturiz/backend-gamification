<?php

namespace Modules\PaymentAccount\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreRetentionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|numeric',
            'payment_account_id' => 'required|numeric',
            'retention_name' => 'required|string',
            'retention_amount' => 'required|string',
        ];
    }

    public function messages(){
        return [
            'user_id.required'=> 'user_id is required',
            'payment_account_id.required'=> 'payment_account_id es requerido',
            'retention_name.required'=> 'retention_name es requerido',
            'retention_amount.required'=> 'retention_amount es requerido',
        ];

    }

}
