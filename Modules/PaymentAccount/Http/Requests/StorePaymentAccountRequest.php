<?php

namespace Modules\PaymentAccount\Http\Requests;

use App\Http\Requests\FormRequest;

class StorePaymentAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string|max:50',
            'last_name' => 'nullable|string',
            'type_document' => 'required|string',
            'nro_document' => 'required|numeric',
            'person_type' => 'required|string',
            'city_of_service' => 'required|string',
            'description_service' => 'required',
            'bank_name' => 'required|string',
            'type_bank_account' => 'required|string',
            'nro_bank_account' => 'required|string',
            'value_service_alphabet' => 'required|string',
            'value_service_number' => 'required|string'
        ];
    }

    public function messages(){
        return [
            'full_name.required'=> 'Name is required',
            'type_document.required'=> 'El tipo de documento es requerido',
            'nro_document.required'=> 'El numero de documento es requerido',
            'person_type.required'=> 'El tipo de persona es requerido',
            'city_of_service.required'=> 'La cuidad donde presto el servicio es requerida',
            'description_service.required'=> 'La descripcion del servicio es requerida',
            'bank_name.required'=> 'El nombre del banco es requerido',
            'nro_bank_account.required'=> 'El numero de cuenta es requerido',
            'value_service_alphabet.required'=> 'El valor en letras es requerido',
            'value_service_number.required'=> 'El valor en numeros es requerido',
        ];

    }

}
