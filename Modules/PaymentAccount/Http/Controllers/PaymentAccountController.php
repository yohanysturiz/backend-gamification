<?php

namespace Modules\PaymentAccount\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Modules\Document\Entities\Document;
use App\Jobs\SendEmail;
use App\Jobs\SendInvoice;
use App\Http\Controllers\EmailController;
use Modules\PaymentAccount\Entities\PaymentAccount;
use Modules\PaymentAccount\Entities\PaymentAccountGeneral;
use Modules\PaymentAccount\Entities\PaymentAccountVersion;
use Modules\PaymentAccount\Entities\Retencions;
use Modules\PaymentAccount\Http\Requests\StorePaymentAccountRequest;
use SendinBlue\Client\Model\SendSmtpEmail;
use App\Exports\PaymentAccountExport;
use Modules\Clients\Entities\CustomerRequests;
use Illuminate\Support\Carbon;
use PhpParser\Node\Expr\Cast\Object_;
use ZipArchive;
use PDFMerger;
use iio\libmergepdf\Merger;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;



class PaymentAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function index()
    {
        $arrPaymentAccount = PaymentAccount::latest()->take(20)->get();

        return response()->json($arrPaymentAccount , 200);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getAdminPaymentAccounts(Request $request)
    {
        $all = $request->all;
        if($all == "false" || $all == "undefined"){

            $arrPaymentAccount = PaymentAccount::latest()->take(10)->get();
            $pagination = null;
        }
        else  {
            $arrPaymentAccount = PaymentAccount::with(['developers','transactions'])->filtertag($request->input('term'))
            ->orderBy('id', 'DESC')
            ->paginate(10);
            $pagination = [
                'total'         => $arrPaymentAccount->total(),
                'currrent_page' => $arrPaymentAccount->currentPage(),
                'per_page'      => $arrPaymentAccount->perPage(),
                'last_page'     => $arrPaymentAccount->lastPage(),
                'from'          => $arrPaymentAccount->firstItem(),
                'to'            => $arrPaymentAccount->lastPage()

            ];
        }
        return [
            'pagination'      => $pagination,
            'paymentAccounts' => response()->json($arrPaymentAccount, 200)
        ];
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getAllPaymentAccountForUser()
    {
        $arrPaymentAccount = PaymentAccount::orderBy('id', 'DESC')->where("user_id", Auth::id())->paginate(15);

        return [
            'pagination' => [
                'total'         => $arrPaymentAccount->total(),
                'currrent_page' => $arrPaymentAccount->currentPage(),
                'per_page'      => $arrPaymentAccount->perPage(),
                'last_page'     => $arrPaymentAccount->lastPage(),
                'from'          => $arrPaymentAccount->firstItem(),
                'to'            => $arrPaymentAccount->lastPage()

            ],

            'developers' => response()->json($arrPaymentAccount, 200)
        ];
    }

    public function getLastPaymentAccountForUser() {
        $insPaymentAccount = PaymentAccount::select(
            "type_document",
            "nro_document",
            "nro_activity_economic",
            "person_type",
            "city_of_service",
            "description_service",
            "bank_name",
            "type_bank_account",
            "nro_bank_account",
            "value_service_alphabet",
            "value_service_number",
            "resident_country",
            "payment_personal",
            "rent",
            "document_firm_id"
        )
        ->where("user_id", Auth::id())->get()->last();
        return response()->json($insPaymentAccount, 200);
    }

    public function getLastMonthPaymentAccount() 
    {
        $date = Carbon::now();
        $month = $date->month;
        $year = $date->year;

        $existingAccount = PaymentAccount::whereMonth('created_at', $month)
        ->whereYear('created_at', $year)
        ->where('user_id', Auth::id())
        ->exists();
        
        if($existingAccount){
            return response()->json(
                [
                    'message' => 'Superaste el limite de cuentas de cobro',
                    'available' => False
                ],
                200
            );
        }

        return response()->json(
            [
                'message' => 'Disponible para cuentas de cobro',
                'available' => True
            ],
            200
        );
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getPaymentAccount($id)
    {
        $insPaymentAccount = PaymentAccount::where("uuid", $id)->first();
        $files = Document::whereIn('id', [$insPaymentAccount->document_firm_id, $insPaymentAccount->document_social_id])->get();
        $insPaymentAccount->files = $files;
        return response()->json($insPaymentAccount, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('paymentaccount::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StorePaymentAccountRequest $request)
    {
        $date = Carbon::now();
        $month = $date->month;
        $year = $date->year;

        $existingAccount = PaymentAccount::whereMonth('created_at', $month)
        ->whereYear('created_at', $year)
        ->where('user_id', Auth::id())
        ->exists();
        
        if($existingAccount){
            return response()->json(['message'=>'Superaste el limite de cuentas de cobro'], 400);
        }
    
        $insPaymentAccount = PaymentAccount::create($request->all());
        
        $insPaymentAccount["correlative_account_number_system"] = $this->createPaymentAccountGeneral($insPaymentAccount);
        $insPaymentAccount["document_firm_id"] = $this->saveImageFirm($request);
        $insPaymentAccount["document_social_id"] = $this->saveImageSocialSecurity($request, $insPaymentAccount->uuid);
        
        $this->createPaymentAccountVersion($insPaymentAccount);
        
        $insPaymentAccount->save();

        # SendEmail::dispatch($insPaymentAccount->uuid);
        $emailer = new EmailController;
        $pdfs = array();
        $pdf = array(
            "name" => strtolower(str_replace([" "], "-", $insPaymentAccount["full_name"]))."-cuenta-de-cobro".".pdf",
            "content" => $this->getPaymentRequestFile($insPaymentAccount["uuid"])
        );
        array_push($pdfs, $pdf);

        $social_pdf = $this->getSocialSecurityPay($insPaymentAccount["uuid"]);
        if($social_pdf != null) {
            $security_social_payment_pdf = array(
                "name" => strtolower(str_replace([" "], "-", $insPaymentAccount["full_name"]))."-seguridad-social-comprobante".".pdf",
                "content" => $social_pdf
            );
            array_push($pdfs, $security_social_payment_pdf);
        }

        $result = $emailer->PaymentAccountEmailCreate($insPaymentAccount->uuid, $pdfs);

        # TODO: Create document support in alegra
        // $this->createDocumentAlegra($insPaymentAccount);

        try {
            return response()->json($insPaymentAccount , 200);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('paymentaccount::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('paymentaccount::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $newForm = $request->all();
        $insPaymentAccount = PaymentAccount::where('uuid', $id)->first();

        $this->deleteImagenS3($insPaymentAccount, $request);

        if(!is_null($this->saveImageFirm($request))){
            $newForm["document_firm_id"] = $this->saveImageFirm($request);
        }

        // Guardamos el archivo de seguridad social en s3 y retornamos la url
        if(!is_null($this->saveImageSocialSecurity($request, $insPaymentAccount->uuid))){
            $newForm["document_social_id"] = $this->saveImageSocialSecurity($request, $insPaymentAccount->uuid);
        }

        $insPaymentAccount->update($newForm);

        return response()->json($insPaymentAccount , 200);
    }

    public function updateStatus(Request $request, $id)
    {
        //
        $insPaymentAccount = PaymentAccount::where('uuid', $id)->first();
        $insPaymentAccount->status = $request->status;
        $insPaymentAccount->created_at = $request->created_at;
        if($request->status == "2"){       
            // if(empty($insPaymentAccount->document_firm_id)) {
            //     $data = [ 'type' => 'error', 'title' => 'Documentos incompletos',
            //               'msg'=>'Por favor agrege la cuenta de cobro o la firma para guardar'];
            //     return response()->json($data, 200);
            // }

            $response_provider = Http::withHeaders([
                'Authorization' => 'Basic ' . base64_encode('tatiana@imuko.co:76f0558408018f3e061b')
            ])->get("https://api.alegra.com/api/v1/contacts?identification=700220150&type=provider");
            
            $provider_alegra = $response_provider->json();

            $response_create_support_document = Http::withHeaders([
                'Authorization' => 'Basic ' . base64_encode('tatiana@imuko.co:76f0558408018f3e061b')
            ])->post('https://api.alegra.com/api/v1/bills', [
                'date' => now(),
                'dueDate' => now(),
                'termsConditions' => "",
                'numberTemplate'=> [
                    "id" => 18
                ],
                'purchases' => [
                    'categories' => [
                        [
                            'id' => 5069,
                            'price' => $insPaymentAccount->value_service_number,
                            'quantity' => 1
                        ]
                    ]
                ],
                'paymentMethod' => "CASH",
                'paymentType' => "DEBIT_TRANSFER",
                'billOperationType' => "INDIVIDUAL",
                'provider' => $provider_alegra[0]["id"],
                'stamp' => [
                    'generateStamp' => false
                ]
            ]);


        }
        
        $insPaymentAccount->save();
        
        // if($request->status == "2"){         
        //     SendInvoice::dispatch($insPaymentAccount->uuid, $insPaymentAccount->user_id);
        // }


        return response()->json($insPaymentAccount , 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Generate a pdf with information paymentAccount of the developer
     * @param int $id
     * @return Response
     */
    public function generateExcel()
    {
        return Excel::download(new PaymentAccountExport, 'CuentasdeCobro.xlsx');
    }

    /**
     * Generate a pdf with information paymentAccount of the developer
     * @param int $id
     * @return Response
     */
    public function generatePDF($id)
    {
        $insPaymentAccount = PaymentAccount::where('uuid', $id)->firstOrFail();
        $insDocumentFirm = Document::where("type_document", "firma_digital")->where("user_id", $insPaymentAccount->user_id)->first();

        if($insDocumentFirm){
            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(100));
            $insPaymentAccount->firm = $file_firm;
        }
        $pdf = PDF::loadView('paymentaccount::pdf.payment-account', compact('insPaymentAccount'))->save(storage_path('app/') . 'cuenta_de_cobro.pdf');

        # Se buscar el archivo de comprobante de pago de seguridad social para hacer el join de documentos
        $social_document = Document::find($insPaymentAccount->document_social_id);

        if($social_document) {
            $social_pdf_s3 = $this->PdfCertificateSocialSecurity($id);
            $patch_pay_social = storage_path('app/' .$social_document->filename);
            $pay_social_document = file_put_contents($patch_pay_social, $social_pdf_s3);

            $pdf_merge = new Merger();
            $pdf_merge->addFile(storage_path('app/cuenta_de_cobro.pdf'));
            $pdf_merge->addFile($patch_pay_social);
            $finally_pdf = $pdf_merge->merge();

            return $finally_pdf;
        }

        return $pdf->stream();
    }

    public function generateSinglePDF($id)
    {
        $insPaymentAccount = PaymentAccount::where('uuid', $id)->firstOrFail();
        $insDocumentFirm = Document::where("type_document", "firma_digital")->where("user_id", $insPaymentAccount->user_id)->first();

        if($insDocumentFirm){
            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(100));
            $insPaymentAccount->firm = $file_firm;
        }
        $pdf = PDF::loadView('paymentaccount::pdf.payment-account', compact('insPaymentAccount'))->save(storage_path('app/') . 'cuenta_de_cobro.pdf');

        return $pdf->stream();
    }

    //descarga masiva de pdf
    public function masivosPDF(Request $request){

        $fecha_inicial =   $request->fecha_inicial;
        $fecha_final =  $request->fecha_final;

        //consulta de la tablas con relacion eloquent
        $insPaymentAccounts = PaymentAccount::with(['document'  => function($query) {

            $query->select("user_id","filename");

        }])->whereDate('created_at', '>=', $fecha_inicial)->whereDate('created_at', '<=',  $fecha_final)->orderby('id','DESC')->get();


        //creamos un pdf por cada usuario
        foreach($insPaymentAccounts as $data){

            // envio de la data por usuario
            $insPaymentAccount = $data;

            $insDocumentFirm = Document::find($insPaymentAccount->document_firm_id);

            if($insDocumentFirm){
                $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(10));
                $insPaymentAccount->firm = $file_firm;
            }

            $html = '';
            $view =view('paymentaccount::pdf.payment-account-massive', compact('insPaymentAccount'));
            $html .= $view->render();
            //creacion y guardado del pdf con los datos del usuario
            PDF::loadHTML($html)->save(storage_path('app/public/cuentas_cobro_masivas/') ."CUENTA-DE-COBRO"."-".$data->correlative_account_number."-"."DOCUMENTO"."-".$data->nro_document.'.pdf');
        }

        //================generando zip ================  $files = File::files(storage_path('app/public/cuentas_cobro_masivas/'));

        $zip = new ZipArchive;

        //nombre de la carpeta zip
        $fileName = 'Cuentas_de_cobro_mazibas.zip';

        //almacena el array de nombres de los pdf en storage
        $array_de_archivos = [];
        //ruta donde crear el zip en la carpeta publica de forma temporal
        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE) {

            //ruta donde almacena los pdf
            $files = File::files(storage_path('app/public/cuentas_cobro_masivas/'));
            //foreach de la ruta y value del nombre de los archivos
            foreach ($files as $key => $value) {

                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);

                //array con el nombre de los archivos  en el storage
                $array_de_archivos[] = $relativeNameInZipFile;

            }

            $zip->close();
            // foreach del array de los nombres de los archivos
            foreach($array_de_archivos as $doc){
                // elimina los archivos luego de crear el zip
                File::delete(storage_path('app/public/cuentas_cobro_masivas/').$doc);

            }
        }
            //realiza la descarga desde la carpeta publica y luego elimina el zip
            return response()->download(public_path($fileName))->deleteFileAfterSend(true);

    }

    //descarga masiva de pdf
    public function GenerateZipPayAccounts(Request $request){

        $fecha_inicial = $request->fecha_inicial;
        $fecha_final = $request->fecha_final;
        $pdfs = [];

        $insPaymentAccounts = PaymentAccount::with(
            ['document'  => function($query) {
                    $query->select("user_id","filename");
                }
            ]
        )->whereDate(
            'created_at', '>=', $fecha_inicial
        )->whereDate(
            'created_at', '<=',  $fecha_final
        )
        ->orderby('id','DESC')->get();

        // Create PDF'S
        foreach($insPaymentAccounts as $data){

            // envio de la data por usuario
            $insPaymentAccount = $data;

            $pdf = PDF::loadView('paymentaccount::pdf.payment-account', compact('insPaymentAccount'))->save("CUENTA-DE-COBRO"."-".$data->correlative_account_number."-"."DOCUMENTO"."-".$data->nro_document.'.pdf');
            // array_push($pdfs, $pdf->stream());
            array_push($pdfs, $pdf->stream());

            // $html = '';
            // $view =view('paymentaccount::pdf.payment-account-massive', compact('insPaymentAccount'));
            // $html .= $view->render();
            // creacion y guardado del pdf con los datos del usuario
            // PDF::loadHTML($html)->save(storage_path('app/public/cuentas_cobro_masivas/') ."CUENTA-DE-COBRO"."-".$data->correlative_account_number."-"."DOCUMENTO"."-".$data->nro_document.'.pdf');
        }

        //================generando zip ================  $files = File::files(storage_path('app/public/cuentas_cobro_masivas/'));

        $zip = new ZipArchive;

        //nombre de la carpeta zip
        $fileName = 'Cuentas_de_cobro_mazibas.zip';

        //almacena el array de nombres de los pdf en storage
        $array_de_archivos = [];
        //ruta donde crear el zip en la carpeta publica de forma temporal
        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE) {

            //ruta donde almacena los pdf
            $files = $pdfs;
            //foreach de la ruta y value del nombre de los archivos
            print $files;
            foreach ($files as $key => $value) {

                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);

                //array con el nombre de los archivos  en el storage
                $array_de_archivos[] = $relativeNameInZipFile;

            }

            $zip->close();
            // foreach del array de los nombres de los archivos
            foreach($array_de_archivos as $doc){
                // elimina los archivos luego de crear el zip
                File::delete(storage_path('app/public/cuentas_cobro_masivas/').$doc);
            }
        }
            //realiza la descarga desde la carpeta publica y luego elimina el zip
            return response()->download(public_path($fileName))->deleteFileAfterSend(true);

    }

    /**
     * Generate a pdf with information of the developer
     * @param int $id
     * @return Response
     */
    public function PdfCertificateSocialSecurity($id)
    {
        $insPaymentAccount = PaymentAccount::where('uuid', $id)->firstOrFail();
        $insDocument = Document::find($insPaymentAccount->document_social_id);

        if($insDocument) {
            $file_certificate_social = Storage::disk("s3")->get("certificate_payment_social_security/" . $insDocument->filename, now()->addSeconds(10));
            return $file_certificate_social;
        }

        return null;
    }


    /**
     * Generate a pdf with information of the developer
     * @param int $id
     * @return Response
     */
    public function generateInvoicePDF($id)
    {
        $name = "comprobante".strftime("%d de %B del %Y", strtotime(date("r"))).".pdf";
        $paymentAccount = PaymentAccount::where('uuid', $id)->first();
        $discounts = $this->discountsInvoice($paymentAccount);
        $retencions = Retencions::where('payment_account_id', $paymentAccount->uuid)->get();
        $total_retention = 0;

        foreach ($retencions as $key => $value) {
            $value->discount = $paymentAccount->value_service_number*$value->retention_amount/100;
            $total_retention = $total_retention + $value->discount;
        }

        $retencions->total_retentions = $total_retention;

        $pdf = PDF::loadView('paymentaccount::pdf.invoice', compact('paymentAccount', 'discounts', 'retencions'))->save($name);

        return $pdf->download($name);
    }


    /**
     * Generate a pdf of invoice
     * @param int $id
     * @return Response
     */
    public function SafeInvBase64($id)
    {
        $name = "comprobante".strftime("%d de %B del %Y", strtotime(date("r"))).".pdf";
        $paymentAccount = PaymentAccount::where('uuid', $id)->first();
        $discounts = $this->discountsInvoice($paymentAccount);

        PDF::loadView('paymentaccount::pdf.invoice', compact('paymentAccount', 'discounts'))->save($name);

        return base64_encode(file_get_contents($name));
    }

    public function discountsInvoice($account){
        $discounts["reteica"] = 0;
        $discounts["retefuente_discount"] = 0;

        if ($account->city_of_service == "Bogotá D.C") {
            $discounts["reteica"] = $account->value_service_number * 0.00966;
        }

        if ($account->rent == true) {
            $value = $account->value_service_number;
            $retefuente = 3.5;
            $percent_total = $value*$retefuente/100;
            // $percent_total = $value * $percent;

            $discounts["retefuente_discount"] = $percent_total;
        }

        return $discounts;
    }

    /**
     * Generate a pdf with information of the developer
     * @param int $id
     * @return Response
     */
    public function SafePaBase64($id)
    {
        $insPaymentAccount = PaymentAccount::where('uuid', $id)->first();
        $insDocumentFirm = Document::find($insPaymentAccount->document_firm_id);

        if($insDocumentFirm){
            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(10));
            $insPaymentAccount->firm = $file_firm;
        }

        $insDocument = Document::find($insPaymentAccount->document_social_id);
        if($insDocument) {
            $file_certificate_social = Storage::disk("s3")->temporaryUrl("certificate_payment_social_security/" . $insDocument->filename, now()->addSeconds(10));
            $insPaymentAccount->certificate_payment_social_security = $file_certificate_social;
        }

        $name = "cuenta".strftime("%d de %B del %Y", strtotime(date("r"))).".pdf";
        PDF::loadView('paymentaccount::pdf.payment-account', compact('insPaymentAccount'))->save($name);

        return base64_encode(file_get_contents($name));
    }

    /**
     * Create payment account general
     * @param request $request
     */
    private function createPaymentAccountGeneral($data) {

        $arrData = ["payment_account_user" => $data["user_id"]];
        $ins = PaymentAccountGeneral::create($arrData);

        return $ins->serial_correlative_system;
    }


    /**
     * Create Payment Account Version
     * @param PaymenAccount $insPaymentAccount
     */
    private function createPaymentAccountVersion($insPaymentAccount) {

        $arrData = [
            "serial_correlative_system" => $insPaymentAccount->correlative_account_number_system,
            "payment_account_user" => $insPaymentAccount->user_id,
            "version" => 1,
            "old_metadata" => $insPaymentAccount
        ];

        $insPaymentVersion = PaymentAccountVersion::create($arrData);

        return $insPaymentVersion;
    }

    /**
     * Create Payment Account Version
     * @param PaymenAccount $insPaymentAccount
     */
    private function createDocumentAlegra($insPaymentAccount) {
        return;
    }

    private function saveImageFirm($request){

        if (is_null($request->url_file_firm)) return null;

        $insDocument = Document::where("type_document", "firma_digital")->where("user_id", $request->user_id)->first();
        if (!empty($insDocument)) return $insDocument->id;

        $name_document = "firm-" .$request->user_id;
        $path = $request->file("url_file_firm")->storeAs("firm", $name_document, "s3");

        $firm = Document::create([
            "filename" => basename($path),
            "url" => Storage::disk('s3')->url($path),
            "user_id" => $request->user_id,
            "type_document" => "firma_digital",
            "description" => "Firma - " . $request->full_name,
            "status" => 0
        ]);

        return $firm->id;
    }

    private function saveImageSocialSecurity($request, $uuid){

        if (is_null($request->url_file_security_social)) return null;

        $name_document = "certificate_payment_social_security-" .$uuid;
        $path = $request->file("url_file_security_social")->storeAs("certificate_payment_social_security", $name_document ,"s3");

        $file_social = Document::create([
            "filename" => basename($path),
            "url" => Storage::disk('s3')->url($path),
            "user_id" => $request->user_id,
            "type_document" => "certificate_payment_social_security",
            "description" => "certificado pago seguridad social - " . $request->full_name,
            "status" => 0
        ]);

        return $file_social->id;
    }

    private function deleteImagenS3($insPaymentAccount, $request) {

        if (!is_null($insPaymentAccount->document_social_id) and !is_null($request->url_file_security_social)){
            $social_document = Document::find($insPaymentAccount->document_social_id);
            Storage::disk('s3')->delete('certificate_payment_social_security/'.$social_document->filename);
            $social_document->delete();
        }

        if (!is_null($insPaymentAccount->document_firm_id) and !is_null($request->url_file_firm)){
            $firm_document = Document::find($insPaymentAccount->document_firm_id);
            Storage::disk('s3')->delete('firma_digital/'.$firm_document->filename);
            $firm_document->delete();
        }


    }

    private function changeStatus($id, $status)
    {
        return PaymentAccount::where('uuid', $id)->update(["status" => $status]);
    }

    private function getPaymentRequestFile($id)
    {
        $insPaymentAccount = PaymentAccount::where('uuid', $id)->first();
        $insDocumentFirm = Document::where("type_document", "firma_digital")->where("user_id", $insPaymentAccount->user_id)->first();

        if($insDocumentFirm) {
            $file_firm = Storage::disk("s3")->temporaryUrl("firma_digital/" . $insDocumentFirm->filename, now()->addSeconds(10));
            $insPaymentAccount->firm = $file_firm;
        }

        $name = "{$insPaymentAccount->full_name}-cuenta-de-cobro.pdf";
        $pdf = PDF::loadView('paymentaccount::pdf.payment-account', compact('insPaymentAccount'))->save($name);

        return base64_encode(file_get_contents($name));
    }

    private function getSocialSecurityPay($id)
    {
        $social_pdf = $this->PdfCertificateSocialSecurity($id);

        if ($social_pdf != null) {
            return base64_encode($social_pdf);
        }

        return null;
    }

    private function getHtmlPayLote($payments)
    {
        $html = "";
        foreach ($payments as $key => $data) {
            $name = $data['full_name'];
            $bank_name = $data['bank_name'];
            $date = $data['creado'];
            $pay_date = $data['pay_date'];

            $html = $html . "<tr>
                <td>{$name}</td>
                <td>{$bank_name}</td>
            </tr>";

        }

        $table = "
            <table width='100%' border='1' bordercolor='#000000'>
            <tr>
                <td>Nombre</td>
                <td>Banco</td>
            </tr>".
            $html.
            "</table>";

        return $table;

    }


    public function sendLotePayments(Request $request) {
        $emailer = new EmailController;

        $fecha_inicial =   $request->fecha_inicial;
        $fecha_final =  $request->fecha_final;
        $status =  $request->status;

        //consulta de la tablas con relacion eloquent
        $insPaymentAccounts = PaymentAccount::whereDate('created_at', '>=', $fecha_inicial)->whereDate('created_at', '<=',  $fecha_final)->where('status', $status)->orderby('id','DESC')->get();

        $pdfs = array();
        $html = $this->getHtmlPayLote($insPaymentAccounts);

        foreach ($insPaymentAccounts as $key => $pay) {
            $pdf = array(
                "name" => strtolower(str_replace([" "], "-", $pay["full_name"]))."-cuenta-de-cobro".".pdf",
                "content" => $this->getPaymentRequestFile($pay["uuid"])
            );
            array_push($pdfs, $pdf);

            $social_pdf = $this->getSocialSecurityPay($pay["uuid"]);
            if($social_pdf != null) {
                $security_social_payment_pdf = array(
                    "name" => strtolower(str_replace([" "], "-", $pay["full_name"]))."-seguridad-social-comprobante".".pdf",
                    "content" => $social_pdf
                );
                array_push($pdfs, $security_social_payment_pdf);
            }

            // $this->changeStatus($pay["uuid"], 1);
        }

        return $emailer->sendEmailPayLote($html, 4, $pdfs);
    }


    public function validCountPerMounth() {
        $user = Auth::user();
        $month = date("m");
        $year = date("y");
        $payment_account = PaymentAccount::where('user_id', $user->id)->whereMonth("created_at", $month)->whereYear("created_at", $year)->get()->count();

        return response()->json($payment_account , 200);

    }

    public function getDocumentSupportAlegra() {
        
        $client = new Client([
            'headers' => [
                'Authorization' => 'Basic dGF0aWFuYUBpbXVrby5jbzo3NmYwNTU4NDA4MDE4ZjNlMDYxYg=='
            ]
        ]);

        $response = $client->request('GET', 'https://api.alegra.com/api/v1/debit-notes?metadata=true&order_field=date&order_direction=DESC&type=adjustmentNote');
        $body = $response->getBody()->getContents();
        $data = json_decode($body, true);

        return $data;
    }

}
