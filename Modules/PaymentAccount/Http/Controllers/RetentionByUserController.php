<?php

namespace Modules\PaymentAccount\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\PaymentAccount\Entities\Retencions;
use Modules\PaymentAccount\Entities\PaymentAccount;

class RetentionByUserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $arrayInsRetention = Retencions::latest()->take(20)->get();

        return response()->json($arrayInsRetention , 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try
        {
            $insPaymentAccount = PaymentAccount::where("uuid", $request->uuid)->first();

            foreach ($request->retencions as $key => $value) {
                Retencions::updateOrCreate(
                    [
                        "user_id" => $insPaymentAccount->user_id,
                        "payment_account_id" => $insPaymentAccount->uuid,
                        "retention_name" => $value['retention_name'],
                    ],
                    [
                        "user_id" => $insPaymentAccount->user_id,
                        "payment_account_id" => $insPaymentAccount->uuid,
                        "retention_name" => $value['retention_name'],
                        "retention_amount" => $value['retention_percent'],
                    ]
                );
            }

            return response()->json("Retenciones cargadas con exito" , 200);

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}