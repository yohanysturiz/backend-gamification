<?php

namespace Modules\PaymentAccount\Entities;

use Illuminate\Database\Eloquent\Model;

class Retencions extends Model
{
    protected $table = "retencion_by_user";

    protected $fillable = [
        'user_id',
        'payment_account_id',
        'retention_name',
        'retention_amount',
    ];

}
