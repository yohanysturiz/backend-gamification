<?php

namespace Modules\PaymentAccount\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class PaymentAccountGeneral extends Model
{
    protected $table = "payment_account_general";

    protected $fillable = ['uuid', 'payment_account_user', 'serial_correlative_system', 'status'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
            $query->serial_correlative_system = str_pad(PaymentAccountGeneral::count() + 1, 3, "0", STR_PAD_LEFT);
            $query->status = 0;
        });
    }
}
