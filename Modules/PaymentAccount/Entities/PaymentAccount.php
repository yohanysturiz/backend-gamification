<?php

namespace Modules\PaymentAccount\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Modules\Developer\Entities\Developer;
use Modules\Payments\Entities\Transaction;

class PaymentAccount extends Model
{
    protected $table = "payment_account";

    protected $fillable = [
        'uuid', 'user_id', 'correlative_account_number', 'correlative_account_number_system', 
        'client_id', 'status', 'full_name', 'last_name', 'type_document', 'nro_document', 'nro_activity_economic',
        'person_type', 'city_of_service', 'description_service', 'phone_number', 'bank_name', 'type_bank_account',
        'nro_bank_account', 'value_service_alphabet', 'value_service_number', 'resident_country', 'payment_personal',
        'rent', 'file_name', 'document_firm_id', 'document_social_id', 'created_at'
    ];

    protected $casts = [
        'resident_country' => 'boolean',
        'payment_personal' => 'boolean',
        'rent' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
            $query->correlative_account_number = str_pad(PaymentAccount::where("user_id", Auth::user()->id)->count() + 1, 3, "0", STR_PAD_LEFT);
            $query->status = 0;
        });
    }

    public function developers() {
        return $this->belongsTo(Developer::class, 'user_id', 'user_id');
    }

    public function document()
    {
        return $this->hasMany('Modules\Document\Entities\Document', 'user_id', 'user_id');
    } 

    public function scopeFilterTag($query, $term){
        if($term) {
           $query->whereHas('developers', function ($query) use ($term) {
                $query = $query->where('emailcandidate','like',"%$term%");
                $query = $query->orWhere('payment_account.full_name','like',"%$term%");
            });
        }
    }

    public function transactions() {
        return $this->hasMany(Transaction::class,'charge_account_id');
    }
}



