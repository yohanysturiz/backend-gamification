<?php

namespace Modules\PaymentAccount\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class PaymentAccountVersion extends Model
{
    protected $table = "payment_account_version";

    protected $fillable = ['uuid', 'payment_account_user', 'serial_correlative_system', 'version', 'old_metadata'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }
}
