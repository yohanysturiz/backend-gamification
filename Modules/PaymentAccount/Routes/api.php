<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('payment-account/{id}', 'PaymentAccountController@getPaymentAccount');
    Route::get('payment-account/pdf/{id}', 'PaymentAccountController@generatePDF');
    Route::get('payment-account/pdf-single/{id}', 'PaymentAccountController@generateSinglePDF');
    Route::get('payment-account/get/excel', 'PaymentAccountController@generateExcel');
    Route::get('payment-account/pdf-security-social-certificate/{id}', 'PaymentAccountController@PdfCertificateSocialSecurity');
    Route::get('me-payments-account', 'PaymentAccountController@getAllPaymentAccountForUser');
    Route::get('last-payment-account', 'PaymentAccountController@getLastPaymentAccountForUser');
    Route::get('payment-account/last/month', 'PaymentAccountController@getLastMonthPaymentAccount');


    Route::post('payment-account', 'PaymentAccountController@store');
    Route::post('payment-account/{id}', 'PaymentAccountController@update');
    Route::post('add-retentions', 'RetentionByUserController@store');

    Route::get('payment-account-per-mounth', 'PaymentAccountController@validCountPerMounth');
    Route::get('/payment-account/invoice/pdf/{id}', 'PaymentAccountController@generateInvoicePDF');
});

Route::group(['middleware' => ['analyst:api']], function () {
    Route::post('payment-account-update-status/{id}', 'PaymentAccountController@updateStatus');
    Route::get('payment-account', 'PaymentAccountController@index');
    Route::post('get-admin-payment-accounts', 'PaymentAccountController@getAdminPaymentAccounts');
});

Route::post('lote-payment-account', 'PaymentAccountController@sendLotePayments');
Route::post('payment/massive-pdf', 'PaymentAccountController@masivosPDF');

Route::get('payment-account/alegra/document-support', 'PaymentAccountController@getDocumentSupportAlegra');

