<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaymentAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_account', function (Blueprint $table) {
            $table->string("file_name")->nullable()->after("rent");
            $table->integer("document_firm_id")->nullable()->after("rent");
            $table->dropColumn("url_file_firm");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_account', function (Blueprint $table) {
            $table->dropColumn('file_name');
            $table->dropColumn('document_firm_id');
        });
    }
}
