<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentSocialIdPaymentAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_account', function (Blueprint $table) {
            $table->integer("document_social_id")->nullable()->after("document_firm_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_account', function (Blueprint $table) {
            $table->dropColumn('document_social_id');
        });
    }
}
