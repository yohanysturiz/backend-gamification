<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportDocumentAlegra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_document_alegra_pay_account', function (Blueprint $table) {
            $table->id();
            $table->string("user_id");
            $table->string("payment_account_id");
            $table->string("support_document_id")->nullable();
            $table->longText('content_support')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_document_alegra');
    }
}
