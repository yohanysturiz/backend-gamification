<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_account', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique();
            $table->string("user_id");
            $table->string("client_id")->nullable();
            $table->string("correlative_account_number");
            $table->string("correlative_account_number_system");
            $table->string("status")->nullable();
            $table->string("full_name")->nullable();
            $table->string("last_name")->nullable();
            $table->string("type_document");
            $table->string("nro_document");
            $table->integer("nro_activity_economic")->nullable();
            $table->string("person_type");
            $table->string("city_of_service")->nullable();
            $table->string("description_service")->nullable();
            $table->string("phone_number");
            $table->string("bank_name");
            $table->string("type_bank_account");
            $table->string("nro_bank_account");
            $table->string("value_service_alphabet")->nullable();
            $table->string("value_service_number")->nullable();
            $table->boolean('resident_country')->default(false);
            $table->boolean('payment_personal')->default(false);
            $table->boolean('rent')->default(false);
            $table->string("url_file_firm")->nullable();

            $table->timestamps();

            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_account');
    }
}
