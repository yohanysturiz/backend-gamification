<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::resource('gamification', MilestoneController::class);

# Milestones
Route::prefix('v2/organization/{org_id}/gamification')->group(function () {

    # create a milestone of organization
    Route::post('milestone', 'MilestoneController@store');

    # get all milestones templates
    Route::get('milestones/templates', 'MilestoneController@index');

    # get all milestones by organizations
    Route::get('milestones', 'MilestoneController@indexMilestoneByOrganization');

    # active milestone for organization
    Route::post('milestone/{milestone_id}/update', 'MilestoneController@update');
});

# Client
Route::get('gamification_client', 'GamificationClientController@index');
Route::get('gamification_client/{id}', 'GamificationClientController@gamificationclient');
Route::post('gamification_client', 'GamificationClientController@store');

# Target Routes
Route::prefix('v2/organization/{org_id}/gamification')->group(function () {
    # create a target of organization
    Route::post('target', 'TargetController@store');

    # get all targets by team of organization
    Route::get('targets/team/{team_id}', 'TargetController@index');

    # get all targets by organization
    Route::get('targets', 'TargetController@AllTargetsClient');
});

Route::get('gamification/target/{id}', 'TargetController@getTarget');



# Team Routes
Route::prefix('v2/organization/{org_id}/gamification')->group(function () {
    # find a team
    Route::get('/team/{id}', 'TeamController@find');

    # get all teams by organization
    Route::get('/teams', 'TeamController@index');

    # create a team of organization
    Route::post('/team', 'TeamController@store');

    #Update a team of organization
    Route::put('/team/{id}', 'TeamController@update');
});

# Area
Route::get('gamification/area', 'AreaController@index');

# Users Team
Route::get('gamification/users_team/{id}', 'UsersTeamController@index');
Route::get('gamification/users/team/target/{target_id}', 'UsersTeamController@usersWithTarget');
Route::post('gamification/users_team', 'UsersTeamController@store');
Route::delete('gamification/users_team/{user_team_id}', 'UsersTeamController@destroy');

# Team Targets
Route::get('gamification/teams/targets/{team_id}', 'TeamsTargetController@index');
Route::get('gamification/target/teams/{target_id}', 'TeamsTargetController@teamsTarget');
Route::post('gamification/teams/target', 'TeamsTargetController@store');
Route::delete('gamification/teams/{team_id}/target/{target_id}', 'TeamsTargetController@destroy');

# Targets User
Route::get('gamification/targets_user/{target_id}', 'TargetsUserController@index');
Route::post('gamification/targets_user', 'TargetsUserController@store');
Route::post('gamification/target_user/{target_id}', 'TargetsUserController@update');


# Reward Routes
Route::prefix('v2/organization/{org_id}/gamification')->group(function () {
    # create a rewards
    Route::post('reward', 'GamificationRewardController@store');

    # get a reward
    Route::get('reward/{id}', 'GamificationRewardController@gamificationRewardFind');

    # get all rewards
    Route::get('rewards', 'GamificationRewardController@index');

    #update a reward
    Route::put('reward/{id}', 'GamificationRewardController@update');
});
