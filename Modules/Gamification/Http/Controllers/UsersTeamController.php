<?php

namespace Modules\Gamification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gamification\Entities\UsersTeam;
use Modules\Gamification\Entities\UserTeamsTarget;
use Modules\Gamification\Http\Requests\StoreUsersTeamRequest;


class UsersTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($id)
    {
        $users_team = UsersTeam::join('user', 'users_team.user_id', '=', 'user.id')
        ->select(
            'user.first_name as first_name',
            'user.last_name as last_name',
            'users_team.status as status',
            'users_team.id as user_team_id',
        )
        ->where("users_team.team_id", "=", $id)
        ->orderBy("users_team.created_at", "DESC")
        ->get();

        return $users_team;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function usersWithTarget($target_id)
    {
        $users_team = UserTeamsTarget::join('user', 'user_teams_target.user_id', '=', 'user.id')
        ->select(
            'user.first_name as first_name',
            'user.last_name as last_name',
            'user_teams_target.status as status',
            'user_teams_target.id as user_team_id',
        )
        ->where("user_teams_target.teams_target_id", "=", $target_id)
        ->orderBy("user_teams_target.created_at", "DESC")
        ->get();

        return $users_team;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('users_team::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreUsersTeamRequest $request)
    {
        return UsersTeam::create($request->all());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('users_team::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('users_team::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($user_team_id)
    {
        return UsersTeam::where("id", $user_team_id)->delete();
    }
}
