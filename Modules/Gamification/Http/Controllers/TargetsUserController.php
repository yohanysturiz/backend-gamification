<?php

namespace Modules\Gamification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gamification\Entities\TargetsUser;
use Modules\Gamification\Http\Requests\StoreTargetsUserRequest;


class TargetsUserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($target_id)
    {
        $targets_users = TargetsUser::join('user', 'targets_user.user_id', '=', 'user.id')
        ->select(
            'user.first_name as first_name',
            'user.last_name as last_name',
            'targets_user.status as status',
            'targets_user.complete_percentege as complete_percentege',
        )
        ->where("targets_user.target_id", "=", $target_id)
        ->orderBy("targets_user.created_at", "DESC")
        ->get();

        return $targets_users;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('targets_user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreTargetsUserRequest $request)
    {
        return TargetsUser::create($request->all());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('targets_user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('targets_user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(StoreTargetsUserRequest $request, $target_id)
    {
        
        $target = TargetsUser::where('target_id', $target_id)->where('user_id', $request->user_id)->first();

        if(empty($target)){
            return response()->json('Target is empty');
        }

        $target->complete_percentege = $request->complete_percentege;
        $target->save();

        if($target->complete_percentege == 100){
             TargetsUser::where('target_id', $target_id)->where('user_id', $request->user_id)->update(['status' => 'complete']);        
        }
        return $target;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
