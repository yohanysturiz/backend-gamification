<?php

namespace Modules\Gamification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gamification\Entities\Target;
use Modules\Gamification\Http\Requests\StoreTargetRequest;


class TargetController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($client_id, $team_id)
    {
        $unassignedTargets = Target::where("client_id", $client_id)
        ->where("type", "company")
        ->whereNotIn('id', function ($query, $team_id=1) {
            $query->select('target_id')
                ->from('teams_target')->where('team_id', $team_id);
        })->get();

        return $unassignedTargets;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function getTarget($id)
    {
        $target = Target::find($id);

        return $target;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function AllTargetsClient($org_id)
    {
        $Targets = Target::where("client_id", $org_id)->get();

        return $Targets;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('target::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreTargetRequest $request, $org_id)
    {
        return Target::create($request->all());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('target::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('target::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
