<?php

namespace Modules\Gamification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gamification\Entities\Team;
use Modules\Gamification\Entities\TeamsTarget;
use Modules\Gamification\Entities\UsersTeam;
use Modules\Gamification\Http\Requests\StoreTeamRequest;


class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($org_id)
    {
        $teams = Team::join('area', 'team.area_id', '=', 'area.id')
        ->select(
            'team.id as id',
            'team.name as name',
            'team.leader_id as leader_id',
            'area.name as area',
            'area.id as area_id',
            'team.active as active',
            'team.slug as slug',
            'team.description as description'
        )
        ->where("team.client_id", $org_id)
        ->orderBy("team.created_at", "DESC")
        ->get();

        return $teams;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function find($org_id, $team_id)
    {
        $team = Team::join('area', 'team.area_id', '=', 'area.id')
        ->select(
            'team.id as id',
            'team.name as name',
            'area.name as area',
            'team.active as active',
            'team.slug as slug',
            'team.description as description'
        )
        ->where("team.client_id", $org_id)
        ->where("team.id", $team_id)
        ->first();

        $team_targets = TeamsTarget::where('team_id', $team_id)->count();
        $team_users = UsersTeam::where('team_id', $team_id)->count();
        $team_performance = 0;
        
        $team->users_team = $team_users;
        $team->team_targets = $team_targets;
        $team->team_performance = $team_performance;
        $team->team_leader = 'Jonathan Morales';

        return $team;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('team::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreTeamRequest $request)
    {
        return Team::create($request->all());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('team::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('team::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $org_id, $id)
    {
        $updateTeam = Team::where("id",$id)->where("client_id", $org_id)->update($request->all());
        return response()->json($updateTeam, 202);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
