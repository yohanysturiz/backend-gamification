<?php

namespace Modules\Gamification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gamification\Entities\TeamsTarget;
use Modules\Gamification\Entities\UsersTeam;
use Modules\Gamification\Entities\UserTeamsTarget;
use Modules\Gamification\Http\Requests\StoreTeamsTargetRequest;


class TeamsTargetController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($team_id)
    {
        $team_target = TeamsTarget::join('target', 'teams_target.target_id', '=', 'target.id')
        ->select(
            'target.id as target_id',
            'target.title as title',
            'target.value as value',
            'target.description as description',
            'target.active as active',
            'teams_target.team_id as team_id'
        )
        ->where("teams_target.team_id", "=", $team_id)
        ->where("target.type", "=", "company")
        ->orderBy("teams_target.created_at", "DESC")
        ->get();

        return $team_target;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function teamsTarget($target_id)
    {
        $team_target = TeamsTarget::join('team', 'teams_target.team_id', '=', 'team.id')
        ->select(
            'team.name as name',
            'team.description as description',
            'team.active as active',
            'teams_target.team_id as team_id'
        )
        ->where("teams_target.target_id", "=", $target_id)
        ->orderBy("teams_target.created_at", "DESC")
        ->get();

        return $team_target;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('teams_target::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreTeamsTargetRequest $request)
    {

        
        $targets_teams = TeamsTarget::create(
            [
                "team_id" => $request["team_id"],
                "target_id" => $request["target_id"],
            ]
        );

        // TODO: Refactorizar y aplicar patron repository
        if ($targets_teams && $request["assign_all_users"]) {
            
            $users_team = UsersTeam::where("team_id", $targets_teams->team_id)
            ->where("status", "active")
            ->get();

            for ($i=0; $i < count($users_team); $i++) { 
                UserTeamsTarget::create(
                    [
                        "user_id" => $users_team[$i]["user_id"],
                        "teams_target_id" => $targets_teams->id,
                        "status" => "active",
                    ]
                );
            }
        }

        return $targets_teams;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('teams_target::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('teams_target::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($team_id, $target_id)
    {
        $teams_target = TeamsTarget::where('team_id', $team_id)
        ->where('target_id', $target_id)
        ->first();

        UserTeamsTarget::where("teams_target_id", $teams_target->id)->delete();

        return $teams_target->delete();
    }
}
