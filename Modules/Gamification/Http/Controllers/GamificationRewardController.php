<?php

namespace Modules\Gamification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gamification\Entities\GamificationReward;

class GamificationRewardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($org_id)
    {
        $rewards = GamificationReward::where('org_id',$org_id)->get();
        return response()->json($rewards);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('gamification::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, $org_id)
    {
        $rewards = new GamificationReward();
        $rewards->title = $request->input('title');
        $rewards->description = $request->input('description');
        $rewards->value = $request->input('value');
        $rewards->stock = $request->input('stock');
        $rewards->conditions = $request->input('conditions');
        $rewards->org_id = $org_id;
        $rewards->save();
        return response()->json([$rewards]);
    }

    public function gamificationRewardFind($id){
        $gamificationReward = GamificationReward::find($id);
        return $gamificationReward; 
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('gamification::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('gamification::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $updateReward = GamificationReward::find($id)->update($request->all());
        return response()->json($updateReward, 202);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
