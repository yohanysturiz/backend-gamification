<?php

namespace Modules\Gamification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gamification\Entities\Gamification;
use Modules\Gamification\Http\Requests\StoreGamificationRequest;


class MilestoneController extends Controller
{
    public static $templates;

    public function __construct()
    {
        self::$templates = [
            [
                "title" => "Welcome",
                "description" => "It will be completed when the user activates his account. They are welcome points and are only acquired once.",
                "points" => 100,
                "icon" => "/icons/g-welcome.png",
                "class" => "image-gris"
            ],
            [
                "title" => "Aniversary",
                "description" => "It will be completed when the user completes one year in the company, it is redeemed for each year completed.",
                "points" => 100,
                "icon" => "/icons/g-happy-birthday.png",
                "class" => "image-gris"
            ],
            [
                "title" => "MVP of Quarter",
                "description" => "It is fulfilled when your leader rewards you as the most relevant collaborator of a quarter, it can be redeemed several times.",
                "points" => 100,
                "icon" => "/icons/g-mvp.png",
                "class" => "image-gris"
            ],
            [
                "title" => "Birthday",
                "description" => "Every time the user's birthday will be assigned the points of this milestone",
                "points" => 100,
                "icon" => "/icons/feliz-cumpleanos.png",
                "class" => "image-gris"
            ],
            [
                "title" => "Promotion",
                "description" => "It is redeemed each time a user obtains a promotion",
                "points" => 100,
                "icon" => "/icons/g-job-promotion.png",
                "class" => "image-gris"
            ],
        ];
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($org_id)
    {
        

        $milestones = Gamification::where("client_id", $org_id)->get()->toArray();
        
        for ($i=0; $i < count($milestones); $i++) {
            foreach (self::$templates as $t_key => $t_value) {
                if (in_array($milestones[$i]["name"], $t_value)) {
                    self::$templates[$t_key]["class"] = "";
                }
            }
        }

        return self::$templates;
    }

    /**
     * Display a listing of the resource for organization.
     * @return Renderable
     */
    public function indexMilestoneByOrganization($org_id)
    {
        $milestones = Gamification::where("client_id", $org_id)->get();
        $milestones_org_info = [];
        
        for ($i=0; $i < count($milestones); $i++) {
            foreach (self::$templates as $t_key => $t_value) {
                if (in_array($milestones[$i]["name"], $t_value)) {
                    self::$templates[$t_key]["class"] = "";
                    $item = self::$templates[$t_key];
                    array_push($milestones_org_info, $item);
                } 
            }
        }

        return $milestones_org_info;

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('gamification::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreGamificationRequest $request)
    {
        return Gamification::create($request->all());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('gamification::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('gamification::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
