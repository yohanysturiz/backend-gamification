<?php

namespace Modules\Gamification\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreTargetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'=>'required',
            'title'=>'required',
            'active'=>'nullable',
            'type'=>'nullable',
            'value'=>'nullable',
            'description'=>'nullable',
            'duration_start'=>'nullable',
            'duration_end'=>'nullable',
        ];
    }

    public function messages() {
        return [
            'name'=> 'Name is required',
            'points'=> 'Points is required',
        ];

    }

}