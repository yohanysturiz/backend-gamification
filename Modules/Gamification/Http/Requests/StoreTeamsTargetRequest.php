<?php

namespace Modules\Gamification\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreTeamsTargetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_id'=>'required',
            'target_id'=>'required',
            'status'=>'nullable',
            'complete_percentege'=>'nullable',
            'assign_all_users'=>'nullable',
        ];
    }

    public function messages() {
        return [
            'team_id'=> 'Team ID is required',
            'target_id'=> 'Target ID is required',
        ];

    }

    public function withValidator($validator) {
        $validator->after(function ($validator) {
            $data = $this->validated();
            
        });
    }

}