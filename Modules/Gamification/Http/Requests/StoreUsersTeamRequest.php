<?php

namespace Modules\Gamification\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreUsersTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_id'=>'required',
            'user_id'=>'required',
            'status'=>'nullable',
        ];
    }

    public function messages() {
        return [
            'team_id'=> 'Team ID is required',
            'user_id'=> 'User ID is required',
        ];

    }

}