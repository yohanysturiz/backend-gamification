<?php

namespace Modules\Gamification\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'=>'required',
            'area_id'=>'required',
            'active'=>'nullable',
            'name'=>'nullable',
            'description'=>'nullable',
            'leader_id'=>'nullable'
        ];
    }

    public function messages() {
        return [
            'name'=> 'Name is required',
            'area_id'=> 'Area is required',
        ];

    }

}