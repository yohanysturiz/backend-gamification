<?php

namespace Modules\Gamification\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use DateTime;

class StoreGamificationClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gamification_id'=>'required',
            'client_id'=>'required',
            'points'=>'required',
            'start_date'=>'nullable',
            'end_date'=>'nullable',
            'status'=>'nullable'
        ];
    }

    public function messages(){
        return [
            'gamifcation_id'=>'Gamification_id es requerido',
            'client_id'=>'Client_id es requerido',
            'points'=>'Points es requerido'
        ];
    }
    
    public function getValidateIntanceCLient(){
        $date_start = new DateTime();
        $validator = parent::getValidatorInstanceCLient();

        $validator->sometimes('start_date', 'nullable', function ($input) {
            return !isset($input->start_date);
        });
        $this->merge(['start_date' => $this->input('start_date', $date_start)]);

        return $validator;
    }
}
