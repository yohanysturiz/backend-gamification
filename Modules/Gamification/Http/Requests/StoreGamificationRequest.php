<?php

namespace Modules\Gamification\Http\Requests;

use App\Http\Requests\FormRequest;
use DateTime;
use DateInterval;

class StoreGamificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'points'=>'required',
            'active'=>'nullable',
            'client_id'=>'required',
            'duration_start'=>'nullable',
            'duration_end'=>'nullable',
        ];
    }

    public function messages() {
        return [
            'name'=> 'Name is required',
            'points'=> 'Points is required',
        ];

    }

    // protected function getValidatorInstance()
    // {
    //     $fecha_start = new DateTime();
    //     $validator = parent::getValidatorInstance();
        
    //     $validator->sometimes('duration_start', 'nullable', function ($input) {
    //         return !isset($input->duration_start);
    //     });
    //     $this->merge(['duration_start' => $this->input('duration_start', $fecha_start)]);

    //     $validator->sometimes('duration_end', 'nullable', function ($input) {
    //         return !isset($input->duration_end);
    //     });
    //     $this->merge(['duration_end' => $this->input('duration_end', $fecha_start->add(new DateInterval('P36M')))]);

    //     return $validator;
    // }

}
