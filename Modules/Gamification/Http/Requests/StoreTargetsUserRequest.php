<?php

namespace Modules\Gamification\Http\Requests;

use App\Http\Requests\FormRequest;

class StoreTargetsUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'target_id'=>'required',
            'user_id'=>'required',
            'status'=>'nullable',
            'complete_percentege'=>'nullable',
        ];
    }

    public function messages() {
        return [
            'target_id'=> 'Target ID is required',
            'user_id'=> 'User ID is required',
        ];

    }

}