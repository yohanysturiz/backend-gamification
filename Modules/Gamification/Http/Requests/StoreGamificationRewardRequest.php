<?php

namespace Modules\Gamification\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGamificationRewardRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'description'=>'nullable',
            'value'=>'required',
            'stock'=>'required',
            'conditions'=>'nullable'     
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
