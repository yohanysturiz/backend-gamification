<?php

namespace Modules\Gamification\Entities;

use Illuminate\Database\Eloquent\Model;

class GamificationClient extends Model
{
    protected $table = "gamification_clients";
    protected $fillable = ['gamification_id','client_id','points','start_date','end_date','status'];
}
