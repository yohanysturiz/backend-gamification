<?php

namespace Modules\Gamification\Entities;

use Illuminate\Database\Eloquent\Model;

class TargetsUser extends Model
{
    protected $table = "targets_user";
    protected $fillable = [
        'target_id',
        'user_id',
        'status',
        'complete_percentege'
    ];
}