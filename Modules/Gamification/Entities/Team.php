<?php

namespace Modules\Gamification\Entities;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = "team";
    protected $fillable = [
        'client_id',
        'area_id',
        'name',
        'slug',
        'description',
        'active',
        'leader_id'
    ];

    public static function boot() {
        parent::boot();

        static::saving(function ($team) {
            $team->slug = Str::slug($team->name);
        });
    }
}