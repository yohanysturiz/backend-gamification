<?php

namespace Modules\Gamification\Entities;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = "area";
    protected $fillable = [
        'client_id',
        'name',
        'slug',
        'description',
        'active'
    ];

    public static function boot() {
        parent::boot();

        static::saving(function ($area) {
            $area->slug = Str::slug($area->name);
        });
    }
}