<?php

namespace Modules\Gamification\Entities;

use Illuminate\Database\Eloquent\Model;

class UserTeamsTarget extends Model
{
    protected $table = "user_teams_target";
    protected $fillable = [
        'teams_target_id',
        'user_id',
        'status',
        'complete_percentege'
    ];
}