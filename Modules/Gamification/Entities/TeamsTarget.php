<?php

namespace Modules\Gamification\Entities;

use Illuminate\Database\Eloquent\Model;

class TeamsTarget extends Model
{
    protected $table = "teams_target";
    protected $fillable = [
        'team_id',
        'target_id',
        'status',
        'complete_percentege',
    ];
}