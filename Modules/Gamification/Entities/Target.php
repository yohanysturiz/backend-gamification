<?php

namespace Modules\Gamification\Entities;


use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $table = "target";
    protected $fillable = [
        'client_id',
        'title',
        'type',
        'value',
        'description',
        'active',
        'duration_start',
        'duration_end'
    ];
}