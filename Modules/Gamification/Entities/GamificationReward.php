<?php

namespace Modules\Gamification\Entities;

use Illuminate\Database\Eloquent\Model;

class GamificationReward extends Model
{
    protected $table = 'gamification_rewards';
    protected $fillable = ['title', 'description', 'value', 'stock', 'conditions'];
}
