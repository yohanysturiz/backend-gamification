<?php

namespace Modules\Gamification\Entities;


use Illuminate\Database\Eloquent\Model;

class Gamification extends Model
{
    protected $table = "gamification";
    protected $fillable = ['name','points','active','client_id','duration_start','duration_end'];
}