<?php

namespace Modules\Gamification\Entities;

use Illuminate\Database\Eloquent\Model;

class UsersTeam extends Model
{
    protected $table = "users_team";
    protected $fillable = [
        'team_id',
        'user_id',
        'status',
    ];
}