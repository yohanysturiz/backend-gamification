<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamification', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('points');
            $table->integer('client_id');
            $table->boolean('active')->default(1);
            $table->date('duration_start')->nullable();
            $table->date('duration_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gamification');
    }
}
