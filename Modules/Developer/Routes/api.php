<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['admin:api']], function () {
    Route::post('developers', 'DeveloperController@index');
    Route::get('developers/all', 'DeveloperController@getAll');
    Route::get('developers/{id}', 'DeveloperController@show');
    Route::get('developers/find/{id}', 'DeveloperController@find');
    Route::get('developers/cv/{id}', 'DeveloperController@GenerateCv');
    Route::get('inactivedeveloper/{id}', 'DeveloperController@inactivedeveloper');
    // Route::get('developers/developers/{email}, DeveloperController@getAssignment');
});

Route::group(['middleware' => ['api']], function () {
    // Update profile for developer
    Route::put('developers/{id}', 'DeveloperController@update');
    Route::get('laboral-certificate/{id}/{print_salary}', 'DeveloperController@GenerateLaboralCertificate');
    Route::get('laboral-certificate-preview/{id}', 'DeveloperController@PreviewLaboralCertificate');
    Route::post('request-laboral-certificate/', 'DeveloperController@requestLaboralCertificate');
    Route::get('laboral-certificates', 'DeveloperController@getRequestCertificate');


    // Routes for CRUD experience
    Route::get('experiences/{uuid}', 'ExperienceController@index');
    Route::get('experience-delete/{id}', 'ExperienceController@destroy');
    Route::post('experiences', 'ExperienceController@store');

    // Routes for CRUD Aptitudes
    Route::get('skills/{uuid}', 'SkillsController@index');
    Route::post('skills', 'SkillsController@store');
    Route::get('aptitud-delete/{id}', 'SkillsController@deleteAptitud');


    // Routes for CRUD Skills Tech
    Route::get('skills-tech/{uuid}', 'SkillsController@showSkillsTech');
    Route::get('skills-tech/{tech}/{id}', 'SkillsController@detachSkillTech');
    Route::post('skills-tech', 'SkillsController@addSkillTech');

    // Routes for CRUD Studies
    Route::get('studies/{uuid}', 'StudiesController@index');
    Route::get('study-delete/{id}', 'StudiesController@destroy');
    Route::post('studies', 'StudiesController@store');

    // Routes for CRUD Lenguages
    Route::get('lenguages/{uuid}', 'SpokenLenguagesController@index');
    Route::get('lenguage-delete/{id}', 'SpokenLenguagesController@destroy');
    Route::post('lenguages', 'SpokenLenguagesController@store');

    Route::get('developers/profile/{uuid}', 'DeveloperController@getDevWithUuid');

    //clientes developer
    Route::get('developer/clients/{id}', 'ClientsController@customer_candidate');
    Route::get('validacion', 'DeveloperController@validacion_tablas');
    Route::post('developer/migration-csv', 'DeveloperController@storeFromCsv');
});

// Create Developer
Route::post('developer', 'DeveloperController@store');

Route::get('developers/cv/{id}', 'DeveloperController@GenerateCv');
Route::get('developers/assigments/{email}', 'DeveloperController@getAssignment');
Route::get('developers/clients/{email}', 'DeveloperController@getClientsAsignados');
