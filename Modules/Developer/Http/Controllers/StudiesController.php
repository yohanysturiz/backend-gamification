<?php

namespace Modules\Developer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Developer\Entities\Studies;
use Modules\User\Entities\User;

class StudiesController extends Controller
{
    use Traits\TraitUserUtils;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($uuid)
    {   
        $insUuid = User::where('id', $uuid)->first()->uuid;
        $insUser = $this->getUser($insUuid);

        $studies = Studies::where('user_id', $insUser->id)->get();
        return response()->json($studies, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $study = new Studies();
        $study->user_id = $request->user_id;
        $study->college_name = $request->college_name;
        $study->discipline = $request->discipline;
        $study->city_id = $request->city_id;
        $study->start_date = $request->start_date;
        $study->end_date = $request->end_date;
        $study->description = $request->description;
        $study->save();
        return response()->json($study, 201);

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('developer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('developer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $study = Studies::find($id);
        $study->user_id = $request->user_id;
        $study->college_name = $request->college_name;
        $study->discipline = $request->discipline;
        $study->city_id = $request->city_id;
        $study->start_date = $request->start_date;
        $study->end_date = $request->end_date;
        $study->description = $request->description;
        $study->save();
        return response()->json($study, 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Studies::find($id)->delete();
        return response()->json(200);
    }
}
