<?php

namespace Modules\Developer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Developer\Entities\Hobby;

class HobbyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        $hobbies = Hobby::where('user_id', $id)->get();
        return response()->json($hobbies, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $hobby = new Hobby();
        $hobby->user_id = $request->user_id;
        $hobby->start_date = $request->start_date;
        $hobby->end_date = $request->end_date;
        $hobby->description = $request->description;
        $hobby->save();
        return response()->json($hobby, 201);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $hobby = Hobby::find($id);
        return response()->json($hobby, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('developer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $hobby = Hobby::find($id);
        $hobby->user_id = $request->user_id;
        $hobby->start_date = $request->start_date;
        $hobby->end_date = $request->end_date;
        $hobby->description = $request->description;
        $hobby->save();
        return response()->json($hobby, 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $hobby = Hobby::find($id);
        $hobby->delete();
        return response()->json(200);
    }
}
