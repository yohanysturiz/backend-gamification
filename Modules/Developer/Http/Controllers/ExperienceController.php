<?php

namespace Modules\Developer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Developer\Entities\Experience;
use Modules\User\Entities\User;

class ExperienceController extends Controller
{
    use Traits\TraitUserUtils;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($uuid)
    {   
        $insUuid = User::where('id', $uuid)->first()->uuid;
        $insUser = $this->getUser($insUuid);

        $experiences = Experience::OrderBy("start_date", "DESC")->where('user_id', $insUser->id)->get();
        return response()->json($experiences, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $experience = Experience::updateOrCreate(
            [
                'id' => $request->id,
                'user_id' => Auth::id(),
            ], $request->all());

        return response()->json($experience, 201);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $experience = Experience::find($id);
        return response()->json($experience, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('developer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $experience = Experience::find($id);
        $experience->user_id = $request->user_id;
        $experience->company_name = $request->company_name;
        $experience->position = $request->position;
        $experience->city_id = $request->city_id;
        $experience->job_type = $request->job_type;
        $experience->start_date = $request->start_date;
        $experience->end_date = $request->end_date;
        $experience->description = $request->description;
        $experience->save();
        return response()->json($experience, 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Experience::find($id)->delete();
        return response()->json(200);
    }
}
