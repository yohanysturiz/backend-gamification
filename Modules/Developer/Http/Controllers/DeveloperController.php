<?php

namespace Modules\Developer\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Modules\Developer\Entities\Developer;
use Modules\Developer\Entities\Experience;
use Modules\Developer\Entities\Skills;
use Modules\Developer\Entities\SpokenLenguages;
use Modules\Developer\Entities\Studies;
use Modules\Developer\Entities\RequestCertificatedLaboral;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Modules\Developer\Events\CreateDeveloperUser;
use App\Http\Controllers\EmailController;
use FuncInfo;
use Illuminate\Support\Facades\Hash;
use Modules\GeneralData\Entities\City\CityModel;
use Modules\Clients\Entities\Contract;
use Modules\Clients\Entities\customer_candidate_pivot;
use Modules\Clients\Entities\Customer;
use Spatie\Permission\Models\Role;
use GuzzleHttp\Client as GuzzleClient;
use PhpParser\Node\Expr\Cast\Object_;
use PHPUnit\Util\Json;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Model\SendSmtpEmail;
use Modules\Developer\Http\Controllers\Client;

class DeveloperController extends Controller
{
    use Traits\TraitUserUtils;

    protected $config;
    protected $smtpApiInstance;
    protected $contactApiInstance;

    public function __construct()
    {
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', config('app.sendin'));
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('partner-key', config('app.sendin'));

        // Configure SMTP API key authorization: api-key
        $this->smtpApiInstance = new SMTPApi(new GuzzleClient(), $this->config);
        $contactApiInstance = new ContactsApi(new GuzzleClient, $this->config);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {

        $developers = $this->search($request->input('term'), $request);

        if ($request->input('term') != null and $developers->total() == 0) {
            $term_split = explode(" ", $request->input('term'));
            $developers = $this->search($term_split[0], $request);
        }

        return [
            'pagination' => [
                'total' => $developers->total(),
                'current_page' => $developers->currentPage(),
                'per_page' => $developers->perPage(),
                'last_page' => $developers->lastPage(),
                'from' => $developers->firstItem(),
                'to' => $developers->lastPage(),

            ],
            'developers' => response()->json($developers, 200)
        ];
    }

    public function search($term, $request)
    {

        return Developer::with('lenguages', 'frameworks')->filtertag($term)
            ->lenguage($request->input('lenguages'))
            ->framework($request->input('frameworks'))
            ->education($request->input('leveleducation'))
            ->orderBy('id', 'DESC')
            ->paginate(20);
    }

    public function validacion_tablas()
    {

        $experience = Experience::where("user_id", "=", Auth::user()->id)->count();
        $idioma = SpokenLenguages::where("user_id", "=", Auth::user()->id)->count();
        $actitud = Skills::where("user_id", "=", Auth::user()->id)->count();
        $estudios = Studies::where("user_id", "=", Auth::user()->id)->count();
        $developer = Developer::with(['lenguages' => function ($query) {
            $query->select("name");
        }])->with(['frameworks' => function ($query) {
            $query->select("name");
        }])->where("user_id", "=", Auth::user()->id)->count();

        if ($experience >= 1 && $idioma >= 1 && $actitud >= 1 && $developer >= 1) {
            return response()->json("0");
        } else {
            return response()->json("1");
        }
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function getAll()
    {
        $developers = Developer::orderBy('id', 'DESC')->where('statuscandidate', 'activo')->get();
        return response()->json($developers, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validEmailExist = $this->validEmailExist($request->emailcandidate);
        if ($validEmailExist != false) return response()->json("El email ya pertenece a otra cuenta", 400);

        $user = $this->createUser($request);

        $developer = $request->input();
        $developer["user_id"] = $user->id;
        $developer = Developer::create($developer);

        $developer->lenguages()->attach($request->lenguajes);
        $developer->frameworks()->attach($request->frameworks);
        $developer->password = $request->password;

        // event(new CreateDeveloperUser($developer, $request));

        $emailer = new EmailController;
        $emailer->WelcomeEmail($developer, 1, 2);

        return response()->json($developer, 201);
    }

    /**
     * Create Collaborator a newly created resource in storage.
     * @param user_id int,
     * @param status string,
     * @return OrganizationsUsers
     */
    public function createCollaborator($user_id, $status)
    {

        $developer = Developer::create(
            [
                "user_id" => $user_id,
                "status" => $status
            ]
        );

        return $developer;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $developer = Developer::where('uuid', $id)->with('lenguages', 'frameworks', 'city.department.country')->first();
        $insUser = User::where("email", $developer->emailcandidate)->first();
        if (empty(!$insUser)) {
            $developer->user_id = $insUser->id;
            $developer->type_document = $insUser->type_document;
        }

        return response()->json($developer, 200);
    }

    /**
     * Find Developer
     * @param $id
     * @return JsonResponse
     */
    public function find($id): JsonResponse
    {
        $developer = Developer::select("user.id", "first_name", "last_name", "type_document", "number_document", "city_expedition_document")
            ->where('developers.id', $id)
            ->join('user', function ($join) {
                $join->on('user.id', '=', 'developers.user_id');
            })
            ->first();
        $insUser = User::where("email", $developer->emailcandidate)->first();
        if (empty(!$insUser)) {
            $developer->user_id = $insUser->id;
            $developer->type_document = $insUser->type_document;
        }

        return response()->json($developer, 200);
    }

    /**
     * Generate a pdf with information of the developer
     * @param int $id
     * @return Response
     */
    public function GenerateCv(request $request, $id)
    {

        $user = Auth::user();
        $user->roles = $user->roles()->first()->name;
        switch ($user->roles) {
            case "administrator":
                $param = 1;
                break;
            case "candidate":
                $param = 2;
                break;
            case "client":
                $param = 3;
                break;
            default:
                break;
        }

        $developer = Developer::where('uuid', $id)->with('lenguages', 'frameworks')->first();
        $user_id = User::where('email', $developer->emailcandidate)->value('id');
        $experience = Experience::where('user_id', $user_id)->get();
        $skills = Skills::where('user_id', $user_id)->get();
        $studies = Studies::where('user_id', $user_id)->with('City')->get();
        $lenguages = SpokenLenguages::where('user_id', $user_id)->get();
        if ($developer->levelprogramingidcandidate > 3) $developer->levelprogramingidcandidate = "Senior";
        else $developer->levelprogramingidcandidate = "Semi-senior";

        $name = "cv" . strftime("%d de %B del %Y", strtotime(date("r")));
        $pdf = PDF::loadView('developer::pdf.cv-candidate', compact('developer', 'experience', 'skills', 'studies', 'lenguages', 'param'));
        return $pdf->stream($name);
    }

    /**
     * Generate a laboral certificate pdf about the developer
     * @param int $id
     * @return Response
     */


    public function getContracts($email)
    {
        $developer = Developer::where('emailcandidate', $email)->first();
        $contract = Contract::where('user_id', $developer->id)->first();

        return $contract;
    }


    public function GenerateLaboralCertificate($id, $print_salary)
    {
        $developer = Developer::where('uuid', $id)->with('lenguages', 'frameworks')->first();
        $user_id = $developer->id;
        $insUser = User::where('id', $developer->user_id)->first();
        $contract = Contract::where('user_id', $user_id)->get()->last();
        $city = CityModel::where('id', $contract->city_id)->first();
        $city_name = $city->name ?? null;
        $start_date = Carbon::parse($contract->start_date)->locale('es');
        $date = Carbon::now()->locale('es');
        $name_pdf = "certificado_laboral.pdf";
        $pdf = PDF::loadView('developer::pdf.laboral-certificate', compact('developer', 'contract', 'start_date', 'city_name', 'date', 'print_salary', 'insUser'))->save($name_pdf);
        $doc = base64_encode(file_get_contents($name_pdf));

        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $insUser->email));
        $sendSmtpEmail['templateId'] = 22;
        $sendSmtpEmail['attachment'] = array(array(
            "name" => $name_pdf,
            "content" => $doc
        ));

        $certificate = RequestCertificatedLaboral::where("user_id", $developer->user_id)->latest('id')->first();
        $certificate->status = 'aprobado';
        $certificate->save();

        try {
            $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $pdf->download('certificado');
    }

    public function PreviewLaboralCertificate($id)
    {
        $developer = Developer::where('uuid', $id)->with('lenguages', 'frameworks')->first();
        $contract = Contract::where('user_id', $developer->id)->get()->last();
        $insUser = User::where('id', $developer->user_id)->first();
        $insCity = CityModel::where('id', $insUser->city_id)->first();

        $city_name = $insCity->name ?? null;
        $start_date = Carbon::parse($contract->start_date)->locale('es');
        $date = Carbon::now()->locale('es');
        $print_salary = False;
        $name_pdf = "certificado_laboral.pdf";
        $pdf = PDF::loadView('developer::pdf.laboral-certificate', compact('developer', 'contract', 'start_date', 'city_name', 'date', 'print_salary', 'insUser'))->save($name_pdf);

        return $pdf->stream();
    }

    public function requestLaboralCertificate(Request $request)
    {
        $certificate = new RequestCertificatedLaboral();
        $certificate->user_id = $request->user_id;
        $certificate->profile_uuid = $request->profile_uuid;
        $certificate->print_salary = $request->print_salary;
        $certificate->save();
        return response()->json($certificate, 201);
    }

    public function getRequestCertificate()
    {
        $arrRequestCertificates = RequestCertificatedLaboral::orderBy('id', 'DESC')->with('user')->paginate(15);

        return [
            'pagination' => [
                'total'         => $arrRequestCertificates->total(),
                'currrent_page' => $arrRequestCertificates->currentPage(),
                'per_page'      => $arrRequestCertificates->perPage(),
                'last_page'     => $arrRequestCertificates->lastPage(),
                'from'          => $arrRequestCertificates->firstItem(),
                'to'            => $arrRequestCertificates->lastPage()

            ],

            'requests' => response()->json($arrRequestCertificates, 200)
        ];
    }


    /**
     * Generate a pdf with information of the developer
     * @param int $id
     * @return Response
     */
    public function SafeCvBase64($id)
    {
        $developer = Developer::where('uuid', $id)->with('lenguages', 'frameworks')->first();
        $user_id = User::where('email', $developer->emailcandidate)->value('id');
        $experience = Experience::where('user_id', $user_id)->get();
        $skills = Skills::where('user_id', $user_id)->get();
        $studies = Studies::where('user_id', $user_id)->get();
        $lenguages = SpokenLenguages::where('user_id', $user_id)->get();
        if ($developer->levelprogramingidcandidate > 3) $developer->levelprogramingidcandidate = "Senior";
        else $developer->levelprogramingidcandidate = "Semi-senior";

        $name = "cv" . strftime("%d de %B del %Y", strtotime(date("r"))) . ".pdf";
        PDF::loadView('developer::pdf.cv-candidate', compact('developer', 'experience', 'skills', 'studies', 'lenguages'))->save($name);

        return base64_encode(file_get_contents($name));
    }

    /**
     * Busqueda de programadores que mas se adaptan a la oferta "match"
     *
     */
    public function searcMatchVacant($id)
    {
        $arrLenguageFilter = explode(",", $id);
        $filterExperience = (empty($_GET["experience"])) ? null : $_GET["experience"];
        $filterAvailability = (empty($_GET["availabilty"])) ? null : $_GET["availabilty"];
        $filterSalary = (empty($_GET["salary"])) ? null : $_GET["salary"];

        $arrDevelopersMatch = Developer::select('developers.*')->join('developer_programming_lenguague_model', 'developers.id', '=', 'developer_programming_lenguague_model.developer_id')
            ->whereIn('developer_programming_lenguague_model.programming_lenguague_model_id', $arrLenguageFilter)
            ->experience($filterExperience)
            ->availability($filterAvailability)
            ->salary($filterSalary)
            ->distinct('developers.emailcandidate')->orderBy('id', 'DESC')->with('lenguages')->get();

        return response()->json($arrDevelopersMatch, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('developer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $arrData = [
            "first_name" => $request->namecandidate,
            "last_name" => $request->lastnamecandidate,
            "phone" => $request->phonecandidate,
            "city_id" => $request->city_id,
            "description" => $request->descriptioncandidate,
            "type_document" => $request->type_document,
            "number_document" => $request->number_document,
            "city_expedition_document" => $request->city_expedition_document,
        ];

        $arrDeveloper = $request->all();
        unset(
            $arrDeveloper["city"],
            $arrDeveloper["created_at"],
            $arrDeveloper["updated_at"],
            $arrDeveloper["user_uuid"],
            $arrDeveloper["type_document"],
            $arrDeveloper["number_document"],
            $arrDeveloper["city_expedition_document"],
        );

        Developer::where('uuid', $request->uuid)->update($arrDeveloper);
        User::where('email', $request->emailcandidate)->update($arrData);

        return response()->json("Ok", 200);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function getDevWithUuid($uuid)
    {
        $insUuid = User::where('id', $uuid)->first()->uuid;
        $insUser = $this->getUser($insUuid);

        $developer = Developer::where('emailcandidate', $insUser->email)->first();
        $developer->city = CityModel::with('country')->find($developer->city_id);
        $developer->user_uuid = $insUser->uuid ?? null;
        $developer->type_document = $insUser->type_document;
        $developer->number_document = $insUser->number_document;
        $developer->city_expedition_document = $insUser->city_expedition_document;
        $developer->clients = $this->getClients($developer->id);


        return response()->json($developer, 200);
    }

    public function getClients($user_id)
    {
        $ids_clients = customer_candidate_pivot::where('developer_id', $user_id)->get();
        $clients = [];

        foreach ($ids_clients as $clave => $valor) {

            $client = Customer::where('id', $valor->customer_id)->first();
            $client["date_asignacion"] = $valor->created_at;
            array_push($clients, $client);
        }

        return $clients;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function validEmailExist($email)
    {
        return Developer::where("emailcandidate", $email)->firstOr(function () {
            return false;
        });
    }

    public function inactivedeveloper($id)
    {
        $inactivedeveloper = Developer::where('id', $id)->update(['statuscandidate' => "inactivo"]);
        return response()->json($inactivedeveloper, 200);
    }

    public function storeFromCsv(Request $request)
    {

        if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {
            $fp = fopen($_FILES['archivo']['tmp_name'], "r");

            while (($data = fgetcsv($fp)) !== false) {
                $validEmailExist = $this->validEmailExist($data[2]);
                if ($validEmailExist != false) continue;

                $roleToAssign = Role::findByName('candidate', 'api');
                $user = User::create([
                    'first_name' => $data[0],
                    'last_name' => $data[1],
                    'email' => $data[2],
                    'password' => Hash::make($data[0]),
                    'status' => 1,
                    'phone' => $data[3],
                    'city_id' => $data[10]
                ])->assignRole($roleToAssign);

                Developer::create([
                    'user_id' => $user->id,
                    'namecandidate' => $data[0],
                    'lastnamecandidate' => $data[1],
                    'emailcandidate' => $data[2],
                    'phonecandidate' => $data[3],
                    'salarycandidate' => $data[4],
                    'bilinguecandidate' => $data[5],
                    'remotecandidate' => $data[6],
                    'levelprogramingidcandidate' => $data[7],
                    'leveleducation' => $data[8],
                    'areadevelop' => $data[9],
                    'city' => $data[10],
                ]);
            }
        }
    }

    public function getAssignment($email)
    {
        $user = User::where('email', $email)->first();
        $developer = Developer::where('user_id', $user->id)->first();
        $assigments = customer_candidate_pivot::where('developer_id', $developer->id)->get();
        $customer_ids = [];

        // buscar en la tabla customer por los ids de la variables assigments
        foreach ($assigments as $key => $val) {
            array_push($customer_ids, $val->customer_id);
        };
        $asig = Customer::whereIn('id', $customer_ids)->get();
        return response()->json($asig, 200);
    }

    public function getClientsAsignados($email)
    {
        $users = User::where('email', $email)->first();
        $client = Customer::where('user_id', $users->id)->first();
        $assigmen = customer_candidate_pivot::where('customer_id', $client->id)->get();
        $developers_id = [];

        foreach ($assigmen as $key => $valor) {
            array_push($developers_id, $valor->developer_id);
        }
        $developers = Developer::whereIn('id', $developers_id)->get();
        return response()->json($developers, 200);
    }
}
