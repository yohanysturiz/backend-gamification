<?php

namespace Modules\Developer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Modules\Developer\Entities\Developer;
use Modules\Clients\Entities\customer_candidate_pivot;
use Modules\User\Entities\User;

class ClientsController extends Controller
{

        public function  customer_candidate(Request $request){
          
          $clients_developer = Developer::with(['customers'  => function($query){
            
              $query ->select('id','company_name','responsable');

          }])->where("user_id", "=", $request->id)->get();

         return  response()->json($clients_developer , 200);
        }
}