<?php

namespace Modules\Developer\Http\Controllers\Traits;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\User\Http\Controllers\UserController;

trait TraitUserUtils
{
    /** 
     * Get User
     */
    public function getUser($uuid) {
        $insUser = UserController::show($uuid);
        return $insUser;
    }


}