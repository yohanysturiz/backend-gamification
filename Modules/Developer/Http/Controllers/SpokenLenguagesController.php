<?php

namespace Modules\Developer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Developer\Entities\SpokenLenguages;
use Modules\User\Entities\User;

class SpokenLenguagesController extends Controller
{
    use Traits\TraitUserUtils;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($uuid)
    {
        $insUuid = User::where('id', $uuid)->first()->uuid;
        $insUser = $this->getUser($insUuid);
        
        $SpokenLenguage = SpokenLenguages::where('user_id', $insUser->id)->get();
        return response()->json($SpokenLenguage, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $spLen = new SpokenLenguages();
        $spLen->user_id = $request->user_id;
        $spLen->name = $request->name;
        $spLen->level = $request->level;
        $spLen->url_certificate = $request->url_certificate;
        $spLen->description = $request->description;
        $spLen->save();
        return response()->json($spLen, 201);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('developer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('developer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $spLen = SpokenLenguages::find($id);
        $spLen->user_id = $request->user_id;
        $spLen->name = $request->name;
        $spLen->level = $request->level;
        $spLen->url_certificate = $request->url_certificate;
        $spLen->description = $request->description;
        $spLen->save();
        return response()->json($spLen, 201);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        SpokenLenguages::find($id)->delete();
        
        return response()->json(200);
    }
}
