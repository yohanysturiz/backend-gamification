<?php

namespace Modules\Developer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Developer\Entities\Developer;
use Modules\Developer\Entities\Skills as Skill;
use Modules\User\Entities\User;

class SkillsController extends Controller
{
    use Traits\TraitUserUtils;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($uuid)
    {
        $insUuid = User::where('id', $uuid)->first()->uuid;
        $insUser = $this->getUser($insUuid);
        $Skills = Skill::where('user_id', $insUser->id)->get();
        
        return response()->json($Skills, 200);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function showSkillsTech($uuid)
    {
        $insUuid = User::where('id', $uuid)->first()->uuid;
        $insUser = $this->getUser($insUuid);
        $insDeveloper = Developer::where('emailcandidate', $insUser->email)->with('Contract', 'lenguages', 'frameworks', 'city.department.country')->first();
        
        return response()->json(['developer' => $insDeveloper], 200);
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('developer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $skill = new Skill();
        $skill->user_id = $request->user_id;
        $skill->name = $request->name;
        $skill->save();
        return response()->json($skill, 201);
    }

    public function storeTech(Request $request)
    {
        $skill = new Skill();
        $skill->user_id = $request->user_id;
        $skill->name = $request->name;
        $skill->save();
        return response()->json($skill, 201);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $skill = Skill::find($id);
        return response()->json($skill, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('developer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $skill = Skill::find($id);
        $skill->user_id = $request->user_id;
        $skill->name = $request->name;
        $skill->save();
        return response()->json($skill, 200);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function addSkillTech(Request $request)
    {
        $developer = Developer::where('emailcandidate', Auth::user()->email)->first();
        $developer->lenguages()->detach($request->lenguajes);
        $developer->lenguages()->attach($request->lenguajes);

        $developer->frameworks()->detach($request->frameworks);
        $developer->frameworks()->attach($request->frameworks);

        return response()->json($developer, 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function detachSkillTech($tech, $id)
    {
        $developer = Developer::where('emailcandidate', Auth::user()->email)->first();

        switch ($tech) {
            case "lenguajes":
                $developer->lenguages()->detach($id);
                break;
            case "frameworks":
                $developer->frameworks()->detach($id);
                break;
        }

        return response()->json(200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function deleteAptitud($id)
    {
        Skill::find($id)->delete();

        return response()->json(200);
    }
}
