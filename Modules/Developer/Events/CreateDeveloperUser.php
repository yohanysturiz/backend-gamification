<?php

namespace Modules\Developer\Events;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Modules\Developer\Entities\Developer;

class CreateDeveloperUser
{
    use SerializesModels;

   /**
     * @var Developer
     */
    public $developer;

    /**
     * @var Request
     */
    public $request;

    /**
     * Create a new event instance.
     *
     * @param Developer $developer
     * @param Request $request
     */
    public function __construct(Developer $developer, Request $request)
    {
        $this->developer = $developer;
        $this->request = $request;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
