<?php

namespace Modules\Developer\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\GeneralData\Entities\ProgrammingLenguage\ProgrammingLenguagueModel;
use Modules\GeneralData\Entities\ProgrammingLenguage\FrameworkLenguageModel;
use Modules\GeneralData\Entities\City\City;
use Ramsey\Uuid\Uuid;
use Modules\Clients\Entities\Customer;
use Modules\User\Entities\User;

class Developer extends Model
{

    protected $table = "collaborator";


    protected $fillable = [
        'user_id',
        'salary',
        'bilingue',
        'status',
        'area', 
        'description',
        'social_networks'
    ];

    protected $guarded = ['created_at', 'updated_at'];


    //determina el parametro de busqueda y el scope en el modelo para realizar el filtro de la vacante
    public static $filters = [
        "experience" => "experience",
        "availabilty" => "availability",
        "salary" => "salary"

    ];

    protected $casts = [
        'social_networks' => 'object'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'customer_candidate_pivot')->withPivot('customer_id','status')->withTimestamps();
    }

    public function lenguages()
    {
        return $this->belongsToMany(ProgrammingLenguagueModel::class);
    }

    public function frameworks()
    {
        return $this->belongsToMany(FrameworkLenguageModel::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function validEmailExist($email)
    {
        return $this->where("email", $email)->firstOr(function () {
            return false;
        });
    }

    public function fullName()
    {
        return $this->namecandidate . " " . $this->lastnamecandidate;
    }

    public function user()
    {
        return User::find($this->user_id);
    }


    // Scope
    public function scopeFiltertag($query, $term){
        if($term != null) {
            $query->where(function ($query) use ($term) {
                $query = $query->orWhere('namecandidate','like',"%$term%");
                $query = $query->orWhere('lastnamecandidate','like',"%$term%");
                $query = $query->orWhere('emailcandidate','like',"%$term%");
            });
        }
    }

    public function scopeLenguage($query, $lenguage){
        if($lenguage != null) {
            $query->whereHas('lenguages', function ($query) use ($lenguage) {
                $query->whereIn('programming_lenguague_model_id', $lenguage);
            });
        }
    }

    public function scopeFramework($query, $framework){
        if($framework != null) {
            $query->whereHas('frameworks', function ($query) use ($framework) {
                $query->whereIn('framework_lenguage_model_id', $framework);
            });
        }
    }

    public function scopeEducation($query, $education){
        if($education != null) {
            $query->where('developers.leveleducation', "=", $education);
        }
    }

    public function scopeExperience($query, $experience){
        if($experience != null) {
            $query->where('developers.levelprogramingidcandidate', ">=", $experience);
        }
    }

    public function scopeAvailability($query, $availability){
        if($availability != null) {
            $query->where('developers.availabalitycandidate', $availability);
        }
    }

    public function scopeSalary($query, $salary){
        if($salary != null) {
            $query->where('developers.salarycandidate', $salary);
        }
    }
    public function Contract() {
        return $this->hasMany('Modules\Clients\Entities\Contract', 'user_id');
    }

}
