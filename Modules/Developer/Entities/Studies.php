<?php

namespace Modules\Developer\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Modules\GeneralData\Entities\City\CityModel;

class Studies extends Model
{
    protected $table = "studies";

    protected $fillable = [
        'uuid', 'user_id', 'college_name', 'discipline', 'city_id', 'start_date', 'end_date', 'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
            $query->user_id = Auth::user()->id;
        });
    }
    public function City() {

        return $this->belongsTo(CityModel::class);
    }

}
