<?php

namespace Modules\Developer\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Hobby extends Model
{
    protected $table = "hobby";

    protected $fillable = [
        'uuid', 'user_id', 'name', 'start_date', 'end_date', 'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
            $query->user_id = Auth::user()->id;
        });
    }
}
