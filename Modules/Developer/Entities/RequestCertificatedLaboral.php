<?php

namespace Modules\Developer\Entities;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Modules\User\Entities\User;

class RequestCertificatedLaboral extends Model
{
    protected $table = "request_certificate_laboral";

    protected $fillable = [
        'uuid', 'user_id', 'profile_uuid', 'status', 'print_salary'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
        });
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
