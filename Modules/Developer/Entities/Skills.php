<?php

namespace Modules\Developer\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Skills extends Model
{
    protected $table = "skills";

    protected $fillable = [
        'uuid', 'user_id', 'name'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
            $query->user_id = Auth::user()->id;
        });
    }
}
