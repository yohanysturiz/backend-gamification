<?php

namespace Modules\Developer\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Experience extends Model
{
    protected $table = "experience";

    protected $fillable = [
        'uuid', 'user_id', 'company_name', 'position', 'city_id', 'job_type', 'start_date', 'end_date', 'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
            $query->user_id = Auth::user()->id;
        });
    }
}
