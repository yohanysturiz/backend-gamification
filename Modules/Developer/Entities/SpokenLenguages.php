<?php

namespace Modules\Developer\Entities;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SpokenLenguages extends Model
{
    protected $table = "spoken_lenguages";

    protected $fillable = [
        'uuid', 'user_id', 'name', 'level', 'url_certificate', 'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query){
            $query->uuid = Uuid::uuid4();
            $query->user_id = Auth::user()->id;
        });
    }
}
