<?php

namespace Modules\Developer\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Developer\Entities\Developer;

class SeedCandidatesExcelDevelopersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         // Model::unguard();
         $file = fopen(public_path('csv/candidatos.csv'), 'r');

         while (($data = fgetcsv($file)) !== false) {
            $insDeveloper = Developer::create([
                 'namecandidate' => $data[0],
                 'lastnamecandidate' => $data[1],
                 'emailcandidate' => $data[2],
                 'phonecandidate' => $data[3],
                 'salarycandidate' => $data[5],
                 'bilinguecandidate' => $data[6],
                 'remotecandidate' => $data[7],
                 'leveleducation' => $data[9],
                 'areadevelop' => $data[10],
                 'city_id' => $data[11],
                 'availabalitycandidate' => $data[12],
             ]);
            $arrLenguages = explode("-", $data[14]);
            $insDeveloper->lenguages()->attach(array_filter($arrLenguages));
            
            $arrFrameworks = explode("-", $data[13]);
            $insDeveloper->frameworks()->attach(array_filter($arrFrameworks));
         }
    }
}
