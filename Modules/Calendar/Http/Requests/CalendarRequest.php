<?php

namespace Modules\Calendar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalendarRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'eventName' => 'required|string|max:180',
            'eventDescription' => 'nullable|string|max:256',
            'eventLink' => 'required|string|max:48',
            'eventDate' => 'required',
            'invited' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'summary.required' => 'El nombre del evento es requerido',
        ];

    }
}
