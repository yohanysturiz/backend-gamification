<?php

namespace Modules\Calendar\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Calendar\Http\Requests\CalendarRequest;
use Modules\Calendar\Repositories\Calendar\CalendarRepositoryInterface;
use Modules\Calendar\Repositories\CalendarAttendee\CalendarAttendeeRepositoryInterface;
use Modules\User\Repositories\User\UserRepositoryInterface;
use Spatie\GoogleCalendar\Event;

class CalendarController extends Controller
{

    /**
     * Get All Events
     * @param CalendarRepositoryInterface $calendarRepository
     * @return JsonResponse
     */
    public function index(CalendarRepositoryInterface $calendarRepository): JsonResponse
    {

        $role = Auth::user()->getRoleNames()[0];
        return response()->json([
            'success' => true,
            'data' => ($role === 'administrator') ? $calendarRepository->list() : $calendarRepository->listByUser(Auth::user()->id)
        ], 200);
    }

    /**
     * Get All Events
     * @param Request $request
     * @param CalendarRepositoryInterface $calendarRepository
     * @return JsonResponse
     */
    public function getEventsByMonth(Request $request, CalendarRepositoryInterface $calendarRepository): JsonResponse
    {
        $role = Auth::user()->getRoleNames()[0];
        $month = (isset($request['month']) && ((int)$request['month'] >= 1 && (int)$request['month'] <= 12)) ? (int)$request['month'] : Carbon::now()->month;
        return response()->json([
            'success' => true,
            'data' => ($role === 'administrator') ? $calendarRepository->getEventsByMonth($month) : $calendarRepository->getEventsByMonthAndUser($month, Auth::user()->id)
        ], 200);
    }

    /**
     * Get All Events Grouped
     * @param Request $request
     * @param CalendarRepositoryInterface $calendarRepository
     * @return JsonResponse
     */
    public function getEventsByMonthGrouped(Request $request, CalendarRepositoryInterface $calendarRepository): JsonResponse
    {
        $role = Auth::user()->getRoleNames()[0];
        $month = (isset($request['month']) && ((int)$request['month'] >= 1 && (int)$request['month'] <= 12)) ? (int)$request['month'] : Carbon::now()->month;
        return response()->json([
            'success' => true,
            'data' => ($role === 'administrator') ? $calendarRepository->getEventsByMonthGrouped($month) : $calendarRepository->getEventsByMonthAndUserGrouped($month, Auth::user()->id)
        ], 200);
    }

    /**
     * Get All Events By Date
     * @param Request $request
     * @param CalendarRepositoryInterface $calendarRepository
     * @return JsonResponse
     */
    public function getEventsByDate(Request $request, CalendarRepositoryInterface $calendarRepository): JsonResponse
    {
        $role = Auth::user()->getRoleNames()[0];
        return response()->json([
            'success' => true,
            'data' => ($role === 'administrator') ? $calendarRepository->getEventsByDate($request['date']) : $calendarRepository->getEventsByDateAndUser($request['date'], Auth::user()->id)
        ], 200);
    }

    /**
     * Save Event To Google Calendar
     * @param CalendarRequest $calendarRequest
     * @param CalendarRepositoryInterface $calendarRepository
     * @param CalendarAttendeeRepositoryInterface $calendarAttendeeRepository
     * @return JsonResponse
     */
    public function store(
        CalendarRequest $calendarRequest,
        CalendarRepositoryInterface $calendarRepository,
        CalendarAttendeeRepositoryInterface $calendarAttendeeRepository,
        UserRepositoryInterface $userRepository): JsonResponse
    {

        $role = Auth::user()->getRoleNames()[0];

        try {

            if ($role === 'administrator' || $role === 'client') {

                DB::beginTransaction();
                //Get Request
                $calendarEvent = new Event;
                $calendarEvent->summary = $calendarRequest->eventName;
                $calendarEvent->description = $calendarRequest->eventDescription;
                $calendarEvent->startDateTime = Carbon::parse($calendarRequest->eventDate[0], 'America/Bogota');
                $calendarEvent->endDateTime = Carbon::parse($calendarRequest->eventDate[1], 'America/Bogota');
                if (env('APP_ENV') == 'production' || env('APP_ENV') == 'staging') {
                    //Save Attendee to Google Calendar
                    foreach ($calendarRequest->invited as $invited) {
                        if ($user = $userRepository->getUserById($invited)) {
                            $calendarEvent->addAttendee(['email' => $user->email]);
                        }
                    }
                }

                //Save Event to Google
                $result = $calendarEvent->save();

                //Save Event to Table
                $data = [
                    'google_calendar_id' => $result->id,
                    'summary' => $calendarRequest->eventName,
                    'description' => $calendarRequest->eventDescription,
                    'link' => $calendarRequest->eventLink,
                    'start_date' => $calendarRequest->eventDate[0],
                    'end_date' => $calendarRequest->eventDate[1],
                ];
                $calendar_result = $calendarRepository->store($data);

                //Save Attendee to Calendar
                foreach ($calendarRequest->invited as $invited) {
                    $calendarAttendeeRepository->store([
                        'calendarId' => $calendar_result->id,
                        'userId' => $invited,
                    ]);
                }

                DB::commit();
                //Response
                return response()->json([
                    'success' => true,
                    'message' => 'Event Created',
                    'data' => $data
                ], 201);
            } else {
                return response()->json(["message" => "The event could not be saved. Access denied"], 403);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            //Response
            return response()->json(["message" => "The event could not be saved"], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param CalendarRequest $calendarRequest
     * @param string $calendarId
     * @param CalendarRepositoryInterface $calendarRepository
     * @return JsonResponse
     */
    public function update(
        CalendarRequest $calendarRequest,
        string $calendarId,
        CalendarRepositoryInterface $calendarRepository): JsonResponse
    {

        $role = Auth::user()->getRoleNames()[0];

        //try {

        if ($role === 'administrator' || $role === 'client') {

            DB::beginTransaction();
            //Find Event on Google Calendar
            $event = Event::find($calendarId);

            //Get Request
            $event->summary = $calendarRequest->eventName;
            $event->description = $calendarRequest->eventDescription;
            $event->startDateTime = Carbon::parse($calendarRequest->eventDate[0], 'America/Bogota');
            $event->endDateTime = Carbon::parse($calendarRequest->eventDate[1], 'America/Bogota');

            //Update Google Calendar
            $event->save();

            //Update Calendar Table
            $data = [
                'summary' => $calendarRequest->eventName,
                'description' => $calendarRequest->eventDescription,
                'link' => $calendarRequest->eventLink,
                'start_date' => $calendarRequest->eventDate[0],
                'end_date' => $calendarRequest->eventDate[1],
            ];
            $calendarRepository->update($calendarId, $data);

            DB::commit();
            //Response
            return response()->json([
                'success' => true,
                'message' => 'Event Updated',
                'data' => $data
            ], 201);
        } else {
            return response()->json(["message" => "The event could not be updated. Access denied"], 403);
        }

        /*} catch (\Exception $e) {

            DB::rollBack();
            //Response
            return response()->json(["message" => "The event could not be updated"], 500);
        }*/
    }

    /**
     * Remove the specified resource from storage.
     * @param $calendarId
     * @param CalendarRepositoryInterface $calendarRepository
     * @return JsonResponse
     */
    public function destroy($calendarId, CalendarRepositoryInterface $calendarRepository): JsonResponse
    {
        $role = Auth::user()->getRoleNames()[0];

        try {

            if ($role === 'administrator' || $role === 'client') {

                DB::beginTransaction();
                //Find Event on Google Calendar
                $event = Event::find($calendarId);

                //Delete Event from Google Calendar
                $event->delete();

                //Delete Calendar Table
                $calendarRepository->delete($calendarId);

                DB::commit();
                //Response
                return response()->json([
                    'success' => true,
                    'message' => 'Event Deleted',
                ], 201);

            } else {
                return response()->json(["message" => "The event could not be deleted. Access denied"], 403);
            }

        } catch (\Exception $e) {

            DB::rollBack();
            //Response
            return response()->json(["message" => "The event could not be deleted"], 500);

        }
    }
}
