<?php

namespace Modules\Calendar\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Calendar\Repositories\Calendar\CalendarRepository;
use Modules\Calendar\Repositories\Calendar\CalendarRepositoryInterface;
use Modules\Calendar\Repositories\CalendarAttendee\CalendarAttendeeRepository;
use Modules\Calendar\Repositories\CalendarAttendee\CalendarAttendeeRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CalendarRepositoryInterface::class,
            CalendarRepository::class
        );
        $this->app->bind(
            CalendarAttendeeRepositoryInterface::class,
            CalendarAttendeeRepository::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
