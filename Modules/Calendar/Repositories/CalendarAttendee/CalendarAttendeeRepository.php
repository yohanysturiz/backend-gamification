<?php


namespace Modules\Calendar\Repositories\CalendarAttendee;


use Modules\Calendar\Entities\CalendarAttendee;

class CalendarAttendeeRepository implements CalendarAttendeeRepositoryInterface
{

    /***
     * Store CalendarAttendee
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        return CalendarAttendee::create([
            'calendar_id' => $data['calendarId'],
            'user_id' => $data['userId'],
        ]);
    }

    /**
     * Delete CalendarAttendee
     * @param $calendarId
     * @param $userId
     * @return mixed
     */
    public function delete($calendarId, $userId)
    {
        $calendarAttendee = CalendarAttendee::whereCalendarId($calendarId)->whereUserId($userId)->first();
        return $calendarId->delete();
    }
}
