<?php


namespace Modules\Calendar\Repositories\CalendarAttendee;


interface CalendarAttendeeRepositoryInterface
{
    public function store($data);
    public function delete($calendarId, $userId);
}
