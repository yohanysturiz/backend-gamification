<?php


namespace Modules\Calendar\Repositories\Calendar;


interface CalendarRepositoryInterface
{
    public function list();
    public function listByUser($userId);
    public function getEventsByMonth($month);
    public function getEventsByMonthAndUser($month, $userId);
    public function getEventsByMonthGrouped($month);
    public function getEventsByMonthAndUserGrouped($month, $userId);
    public function getEventsByDate($date);
    public function getEventsByDateAndUser($date, $userId);
    public function store($data);
    public function update($calendarId, $data);
    public function delete($calendarId);
}
