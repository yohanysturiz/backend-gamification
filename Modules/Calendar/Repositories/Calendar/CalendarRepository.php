<?php

namespace Modules\Calendar\Repositories\Calendar;

use Illuminate\Support\Facades\DB;
use Modules\Calendar\Entities\Calendar;

class CalendarRepository implements CalendarRepositoryInterface
{

    /**
     * Get All Events
     * @return mixed
     */
    public function list()
    {
        return Calendar::select("*")->get();
    }

    /**
     * Get Calendar By User
     * @param $userId
     * @return mixed
     */
    public function listByUser($userId)
    {
        return Calendar::select("calendar.*")
            ->join('calendar_attendee', 'calendar_attendee.calendar_id', '=', 'calendar.id')
            ->where('calendar_attendee.user_id', $userId)
            ->get();
    }

    /**
     * Get Events By Month
     * @param $month
     * @return mixed
     */
    public function getEventsByMonth($month)
    {
        return Calendar::whereRaw("MONTH(start_date) = $month")->get();
    }

    /**
     * Get Events By Month By User
     * @param $month
     * @param $userId
     * @return mixed
     */
    public function getEventsByMonthAndUser($month, $userId)
    {
        return Calendar::select("calendar.*")
            ->whereRaw("MONTH(start_date) = $month")
            ->join('calendar_attendee', 'calendar_attendee.calendar_id', '=', 'calendar.id')
            ->where('calendar_attendee.user_id', $userId)
            ->get();
    }

    /**
     * Get Events By Month Grouped
     * @param $month
     * @return mixed
     */
    public function getEventsByMonthGrouped($month)
    {
        return Calendar::select(DB::raw('DATE(start_date) AS start_date'), DB::raw('COUNT("*") AS total_events'))
            ->whereRaw("MONTH(start_date) = $month")
            ->groupBy(DB::raw('DATE(start_date)'))
            ->get();
    }

    /**
     * Get Events By Month Grouped By User
     * @param $month
     * @param $userId
     * @return mixed
     */
    public function getEventsByMonthAndUserGrouped($month, $userId)
    {
        return Calendar::select(DB::raw('DATE(start_date) AS start_date'), DB::raw('COUNT("*") AS total_events'))
            ->join('calendar_attendee', 'calendar_attendee.calendar_id', '=', 'calendar.id')
            ->where('calendar_attendee.user_id', $userId)
            ->whereRaw("MONTH(start_date) = $month")
            ->groupBy(DB::raw('DATE(start_date)'))
            ->get();
    }


    /**
     * Get Events By Date
     * @param $date
     * @return mixed
     */
    public function getEventsByDate($date)
    {
        return Calendar::select('calendar.*', DB::raw('DATE(start_date) AS start_date'), DB::raw('TIME(start_date) AS start_time'), DB::raw('TIME(end_date) AS end_time'))
            ->whereRaw("DATE(start_date) = '$date'")
            ->with('CalendarAttendeeProfile')
            ->get();
    }

    /**
     * Get Events By Date By User
     * @param $date
     * @param $userId
     * @return mixed
     */
    public function getEventsByDateAndUser($date, $userId)
    {
        return Calendar::select('calendar.*', DB::raw('DATE(start_date) AS start_date'), DB::raw('TIME(start_date) AS start_time'), DB::raw('TIME(end_date) AS end_time'))
            ->join('calendar_attendee', 'calendar_attendee.calendar_id', '=', 'calendar.id')
            ->where('calendar_attendee.user_id', $userId)
            ->whereRaw("DATE(start_date) = '$date'")
            ->with('CalendarAttendeeProfile')
            ->get();
    }

    /**
     * Save Calendar
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        return Calendar::create([
            'google_calendar_id' => $data['google_calendar_id'],
            'summary' => $data['summary'],
            'description' => $data['description'],
            'link' => $data['link'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
        ]);
    }

    /**
     * Update Calendar
     * @param $calendarId
     * @param $data
     * @return mixed
     */
    public function update($calendarId, $data)
    {
        //Find Calendar
        $calendar = Calendar::whereGoogleCalendarId($calendarId)->first();
        //Fill
        $calendar->summary = $data['summary'];
        $calendar->description = $data['description'];
        $calendar->start_date = $data['start_date'];
        $calendar->end_date = $data['end_date'];
        //Save and return
        return $calendar->save();
    }

    /**
     * Delete Calendar
     * @param $calendarId
     * @return mixed
     */
    public function delete($calendarId)
    {
        //Find Calendar
        $calendar = Calendar::whereGoogleCalendarId($calendarId)->first();
        return $calendar->delete();
    }

}
