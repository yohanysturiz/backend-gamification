<?php

namespace Modules\Calendar\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\User\Entities\User;

class CalendarAttendee extends Model
{
    protected $table = 'calendar_attendee';
    protected $fillable = [
        'calendar_id', 'user_id'
    ];


    //Calendar
    public function Calendar(): BelongsTo
    {
        return $this->belongsTo(Calendar::class);
    }

    //User
    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
