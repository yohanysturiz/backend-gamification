<?php

namespace Modules\Calendar\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Calendar extends Model
{
    protected $table = 'calendar';
    protected $fillable = [
        'google_calendar_id', 'summary', 'description', 'link', 'start_date', "end_date"
    ];

    // Calendar Attendee
    public function CalendarAttendee(): HasMany
    {
        return $this->hasMany(CalendarAttendee::class);
    }

    // Calendar Attendee With Name
    public function CalendarAttendeeProfile(): HasMany
    {
        return $this->hasMany(CalendarAttendee::class)
            ->select('calendar_attendee.*', 'user.first_name', 'user.last_name', 'user.email')
            ->join('user', function ($join) {
                $join
                    ->on('user.id', '=', 'calendar_attendee.user_id');
            });
    }
}
