<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api']], function () {
    Route::prefix('calendar')->group(function() {
        Route::get('/', 'CalendarController@index');
        Route::post('/events-by-month', 'CalendarController@getEventsByMonth');
        Route::post('/events-by-month-grouped', 'CalendarController@getEventsByMonthGrouped');
        Route::post('/events-by-date', 'CalendarController@getEventsByDate');
        Route::post('/', 'CalendarController@store');
        Route::put('/{id}', 'CalendarController@update');
        Route::delete('/{id}', 'CalendarController@destroy');
    });
});
