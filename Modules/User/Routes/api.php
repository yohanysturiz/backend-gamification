<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Stadistics
 */
Route::get('/total-register-data', 'DataMeasurement@index');

/**
 * Analist Role
 */
Route::group(['middleware' => ['analyst:api']], function () {
    Route::post('create-analyst', 'UserController@createAnalystUser');
});


/**
 * Users
 */
Route::get('/get-users', 'UserController@getUsers');
Route::middleware('auth:api')->get('/get-last/users', 'UserController@getLastUsers');
Route::post('create-analyst', 'UserController@createAnalystUser');
Route::put('/user/update/{id}', 'UserController@update');

/**
 * Socials Authentication
 */
Route::middleware('auth:api')->post('/user/socials/{userId}', 'UserController@getSocials');

/**
 * Update DNI
 */
Route::middleware('auth:api')->post('/user/update-dni/{userId}', 'UserController@updateDNI');

// Get Templates
Route::get('/templates', 'TemplatesController@index');
Route::get('/templates/user/{id}', 'TemplatesController@getTemplatesByUser');
Route::get('/templates/save/{id}', 'TemplatesController@saveTemplate');
Route::get('/templates/delete/{id}', 'TemplatesController@deleteTemplate');


/**
 * Banks
 */
Route::middleware('auth:api')->post('/user/banks/{id}', 'UserController@saveUserBanks');
Route::middleware('auth:api')->get('/user/banks/{id}', 'UserController@getUserBanks');


// Users V2
Route::prefix('v2/organizations')->group(function () {
    Route::get('/{org_id}/users/', 'UserController@getUsersOrganizationInNotTeam');
    Route::post('/login', 'AuthController@authenticate');
    Route::post('users/create', 'UserController@createUsersOrganization');
});

Route::prefix('v2/organization/{org_id}/users')->group(function () {
    # Get all users organizations
    Route::get('/all', 'UserController@getUsersOrganization');
});