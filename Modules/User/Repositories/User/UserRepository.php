<?php

namespace Modules\User\Repositories\User;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;
use Modules\Gamification\Entities\TargetsUser;
use Modules\Gamification\Entities\Team;
use Modules\Gamification\Entities\UsersTeam;

class UserRepository implements UserRepositoryInterface
{
    
    /**
     * Create User
     * @return mixed
     */

     public function createUser($user_data, $rol)
     {
        $roleToAssign = Role::findByName($rol, 'api');
        $arrUser = [
            'first_name' => $user_data["first_name"],
            'last_name' => $user_data["last_name"],
            'email' => $user_data["email"],
            'phone' => $user_data["phone"],
            'country_id' => $user_data["country_id"],
            'password' => Hash::make($user_data["password"]),
            'status' => 1
        ];

        $insUser = User::create($arrUser)->assignRole($roleToAssign);

        return $insUser;
     }
    
    /**
     * Get Users
     * @return mixed
     */
    public function getUsers()
    {
        return User::select('id', 'email', 'first_name', 'last_name', 'status')->get();
    }

    /**
     * Get Last Users
     * @return mixed
     */
    public function getLastUsers()
    {
        return User::latest()->take(5)->get();
    }

    /**
     * Get User By Id
     * @param $userId
     * @return mixed
     */
    public function getUserById($userId)
    {
        return User::findOrFail($userId);
    }

    /**
     * Get User By Id
     * @param $userId
     * @return mixed
     */
    public function getUsersOrganizationInNotTeam($orgId)
    {
        return User::select('user.*')
        ->leftJoin('users_team', 'user.id', '=', 'users_team.user_id')
        ->whereNull('users_team.user_id')
        ->orWhereNotIn('users_team.team_id', function ($query) {
            $query->select('team_id')
                ->from('users_team')
                ->whereColumn('users_team.user_id', 'user.id');
        })
        ->get();
    }

    public function getUsersOrganization($orgId)
    {
        $users = User::select('user.*')
        ->join('organizations_users', 'user.id', '=', 'organizations_users.user_id')
        ->where("organizations_users.organization_id", $orgId)
        ->latest()
        ->take(5)
        ->get();

        foreach ($users as $key => $user) {
            $user["targets"] = TargetsUser::where("user_id", $user->id)->count();
            $user["team_name"] = "Without team";

            $user_team = UsersTeam::where("user_id", $user->id)->first();

            if (isset($user_team)) {
                $team = Team::find($user_team->team_id);
                $user["team_name"] = $team->name;
            }

        }

        return $users;
    }
}
