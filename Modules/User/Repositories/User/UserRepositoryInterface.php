<?php


namespace Modules\User\Repositories\User;


interface UserRepositoryInterface
{
    public function createUser($user_data, $rol);
    public function getUsers();
    public function getLastUsers();
    public function getUserById($userId);
    public function getUsersOrganizationInNotTeam($orgId);
    public function getUsersOrganization($orgId);
}
