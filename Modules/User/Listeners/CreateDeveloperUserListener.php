<?php

namespace Modules\User\Listeners;

use Modules\Developer\Events\CreateDeveloperUser;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class CreateDeveloperUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CreateDeveloperUser $event
     * @return void
     */
    public function handle(CreateDeveloperUser $event)
    {
        //
        $developer = $event->developer;
        $request = $event->request;

        // $this->createUser($developer);
    }

    // protected function createUser($developer){

    //     $roleToAssign = Role::findByName('candidate', 'api');

    //     User::create([
    //         'first_name' => $developer->namecandidate,
    //         'last_name' => $developer->lastnamecandidate,
    //         'email' => $developer->emailcandidate,
    //         'password' => Hash::make($developer->password),
    //         'status' => 1
    //     ])->assignRole($roleToAssign);

    //     return;
    // }
}
