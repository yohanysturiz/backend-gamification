<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Calendar\Entities\Calendar;

class UserEmail extends Model
{
    protected $table = 'user_emails';
    protected $fillable = [
        'template_id', 'user_id'
    ];
    
    //User
    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
