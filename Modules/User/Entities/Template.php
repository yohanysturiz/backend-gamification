<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Template extends Model
{
    protected $table = 'templates';
    protected $fillable = [
        'template_provider_id', 'name'
    ];

    //User
    public function userEmails(): HasMany
    {
        return $this->hasMany(UserEmails::class);
    }
}
