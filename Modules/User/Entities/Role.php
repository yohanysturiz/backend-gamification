<?php

namespace Modules\User\Entities;

use Spatie\Permission\Models\Role;

class RoleModel extends Role
{
    protected $table = "roles";

    protected $fillable= [ 'name','guard_name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}