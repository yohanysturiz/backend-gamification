<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Calendar\Entities\CalendarAttendee;
use Ramsey\Uuid\Uuid;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Str;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;


    protected $fillable = [
        "uuid",
        "first_name",
        "last_name",
        "email",
        "password",
        "description",
        "status",
        "phone",
        "country_id",
        "industry_id",
        "rol",
        "city_id",
        "address",
        "api_token",
        "id_google",
        "id_github",
        "id_linkedin",
        "type_document",
        "number_document",
        "city_expedition_document",
        "created_at",
        "person_type",
        "nro_activity_economic",
        "resident_country",
        "city_of_service",
        "position"
    ];

    public function routeNotificationForSlack($notification)
    {
        return config('logging.channels.slack.url');
    }

    protected $table = "user";

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->uuid = Uuid::uuid4();
            $query->api_token = Str::random(60);
        });
    }


    //Calendar
    public function CalendarAttendee(): HasMany
    {
        return $this->hasMany(CalendarAttendee::class);
    }

}
