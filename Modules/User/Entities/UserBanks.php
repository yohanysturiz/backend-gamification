<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserBanks extends Model
{
    protected $table = 'banks_user';
    protected $fillable = [
        'bank_id', 'user_id', 'is_active', 'type_bank', 'type_bank_account', 'nro_bank_account'
    ];

    //User
    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
