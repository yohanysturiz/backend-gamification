<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Developer\Entities\Developer;
use Modules\User\Entities\User;
use Modules\User\Entities\UserBanks;
use Modules\User\Http\Requests\UpdateDNIRequest;
use Modules\User\Repositories\User\UserRepositoryInterface;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Modules\GeneralData\Entities\Banks\BanksModel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('user::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('user::create');
    }

    public function getSocials($userId)
    {
        $socials = User::select("id_google as google", "id_github as github", "id_linkedin as linkedin")
            ->where('id', $userId)
            ->get();

        return ResponseBuilder::success(["id" => $socials]);
    }

    public function createUsersOrganization(Request $request)
    {
        $validEmailExist = $this->validEmailExist($request->analyst_email);

        if ($validEmailExist) return response()->json("duplicated-email", 400);
        $insUser = $this->createUser($request, $request->rol);
    }

    public function createAnalystUser(Request $request)
    {
        $validEmailExist = $this->validEmailExist($request->analyst_email);

        if ($validEmailExist) return response()->json("duplicated-email", 400);
        $insUser = $this->createUser($request);
    }

    protected function createUser($request, $rol = 'collaborator')
    {
        $roleToAssign = Role::findByName($rol, 'api');
        $arrUser = [
            'first_name' => $request->analyst_name,
            'last_name' => $request->analyst_lastname,
            'email' => $request->analyst_email,
            'password' => Hash::make($request->analyst_password),
            'status' => 1,
        ];

        $insUser = User::create($arrUser)->assignRole($roleToAssign);

        return $insUser;
    }

    public function validEmailExist($email)
    {
        $user = User::where("email", $email)->first();
        if ($user) return true;

        return false;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public static function show($uuid)
    {
        return User::where("uuid", $uuid)->first();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $updateReward = User::find($id)->update($request->all());
        return response()->json($updateReward, 202);
    }

    /**
     * Save Banks for user.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function saveUserBanks(Request $request, $id)
    {
        $validBankExist = $this->validBankExist($id);

        if ($validBankExist) return response()->json("duplicated-bank", 200);

        $user_banks = UserBanks::create([
            "bank_id" => $request->bank_id,
            "user_id" => $id,
            "type_bank" => $request->type_bank,
            "type_bank_account" => $request->type_bank_account,
            "nro_bank_account" => $request->nro_bank_account,
            "is_active" => true
        ]);

        return response()->json($user_banks, 201);
    }

    /**
     * Get Banks User
     * @return JsonResponse
     */
    public function getUserBanks($id)
    {
        $user_bank = UserBanks::where("user_id", $id)->get()->last();
        if ($user_bank) {
            $user_bank->bank = BanksModel::find($user_bank->bank_id);
            $user_bank->bank->currency = $user_bank->bank->currency;
        }

        return response()->json($user_bank, 200);
    }

    public function validBankExist($user_id)
    {
        UserBanks::where("user_id", $user_id)->delete();

        return false;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update DNI Info
     * @param UpdateDNIRequest $request
     * @param $userId
     * @return JsonResponse
     */
    public function updateDNI(UpdateDNIRequest $request, $userId): JsonResponse
    {

        try {
            User::where('uuid', $userId)->update([
                'type_document' => $request->type_document,
                'number_document' => $request->number_document,
                'city_expedition_document' => $request->city_expedition_document
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Info Updated'
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => 'Info not Updated'
            ], 500);
        }
    }

    /**
     * Get Users
     * @param UserRepositoryInterface $userRepository
     * @return JsonResponse
     */
    public function getUsers(UserRepositoryInterface $userRepository): JsonResponse
    {
        return response()->json($userRepository->getUsers(), 200);
    }

    /**
     * Get Lasts Users
     * @param UserRepositoryInterface $userRepository
     * @return JsonResponse
     */
    public function getLastUsers(UserRepositoryInterface $userRepository): JsonResponse
    {
        return response()->json($userRepository->getLastUsers(), 200);
    }

    /**
     * Get Users in not Team
     * @param UserRepositoryInterface $userRepository
     * @return JsonResponse
     */
    public function getUsersOrganizationInNotTeam(UserRepositoryInterface $userRepository, $orgId): JsonResponse
    {
        return response()->json($userRepository->getUsersOrganizationInNotTeam($orgId), 200);
    }

    /**
     * Get Users Organization
     * @param UserRepositoryInterface $userRepository
     * @return JsonResponse
     */
    public function getUsersOrganization(UserRepositoryInterface $userRepository, $orgId): JsonResponse
    {
        return response()->json($userRepository->getUsersOrganization($orgId), 200);
    }
}
