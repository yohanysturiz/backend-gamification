<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\User\Entities\Template;
use Modules\User\Entities\UserEmail;

class TemplatesController extends Controller
{
    /**
     * Get All Templates
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(Template::get(), 200);
    }

    /**
     * Get Templates By User
     * @param $id
     * @return JsonResponse
     */
    public function getTemplatesByUser($id): JsonResponse
    {
        return response()->json(UserEmail::where('user_id', $id)->get(), 200);
    }

    /**
     * Save Template By User
     * @param $id
     * @return JsonResponse
     */
    public function saveTemplate($id): JsonResponse
    {
        $save = UserEmail::create([
            'template_id' => $id,
            'user_id' => Auth::user()->id,
        ]);
        return response()->json(['success' => true, 'message' => 'Templated saved'], 200);
    }


    /**
     * Delete Template By User
     * @param $id
     * @return JsonResponse
     */
    public function deleteTemplate($id): JsonResponse
    {

        $userEmail = UserEmail::where('template_id', $id)->where('user_id', Auth::user()->id)->first();
        $template = UserEmail::find($userEmail->id);
        $template->delete();
        return response()->json(['success' => true, 'message' => 'Templated deleted'], 200);
    }


}
