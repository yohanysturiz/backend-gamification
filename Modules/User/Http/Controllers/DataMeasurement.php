<?php

namespace Modules\User\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;


class DataMeasurement extends Controller
{


    
    public function index()
    {
        $users      = \DB::table('user')->count();
        $developers = \DB::table('developers')->count();
        $vacancies  = \DB::table('customer_requests')->count();
        $closed     = \DB::table('customer_request_developer')->count();

        return response()->json(
            [
                'developers'=> $developers,
                'vacancies'=> $vacancies,
                'closed'=> $closed,
                'users'=> $users
            ]
        );
    }
}