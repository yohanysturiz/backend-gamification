<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class DataMesuarement extends Controller
{
    
    public function index()
    {   

        $developers = \DB::table('developers')->count();
        $vacancies  = \DB::table('customer_requests')->count();
        $closed     = \DB::table('customer_request_developer')->count();

        return response()->json(['developers'=>$developers, 
                                'vacancies'=>$vacancies,
                                'closed' =>$closed ]);
               
    }

    
}
