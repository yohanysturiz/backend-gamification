<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUsersTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
       Schema::table('user', function (Blueprint $table) {
           $table->string('type_document',100)->nullable();
           $table->string('number_document',100)->nullable();
           $table->string('city_expedition_document',100)->nullable();
       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::table('user', function (Blueprint $table) {
           $table->dropColumn(['type_document','number_document','city_expedition_document']);
       });
   }
}
