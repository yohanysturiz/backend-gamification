<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique();

            $table->string('first_name');
            $table->string("last_name");
            $table->string("email")->unique();
            $table->string('password');
            $table->string("description")->nullable();
            $table->integer("status")->default(1);
            $table->string("phone")->nullable();
            $table->integer("city_id")->nullable();
            $table->text("address")->nullable();

            $table->unsignedBigInteger("country_id")->nullable();
            $table->foreign('country_id')
                  ->references('id')
                  ->on('country')
                  ->onDelete('cascade');

            $table->unsignedBigInteger("industry_id")->nullable();
            $table->foreign('industry_id')
                ->references('id')
                ->on('industry')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
