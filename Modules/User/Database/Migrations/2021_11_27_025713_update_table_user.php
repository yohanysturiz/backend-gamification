<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->string('person_type',100)->nullable();
            $table->string('nro_activity_economic',100)->nullable();
            $table->string('resident_country',100)->nullable();
            $table->string('city_of_service',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn(['person_type','nro_activity_economic','resident_country', 'city_of_service']);
        });
    }
}
