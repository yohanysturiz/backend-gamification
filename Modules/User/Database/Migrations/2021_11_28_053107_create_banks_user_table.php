<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks_user', function (Blueprint $table) {
            $table->id();
            $table->integer('bank_id');
            $table->integer('user_id');
            $table->string('type_bank')->nullable();
            $table->string('type_bank_account')->nullable();
            $table->string('nro_bank_account');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks_user');
    }
}
