<?php
namespace Modules\User\Database\Seeders;

use Modules\User\Entities\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserAdminSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
	{
	
	}

	public function run()
	{
        $roleToAssign = Role::findByName('administrator', 'api');
        
        User::create([
            'first_name' => "Administrator",
            'last_name' => "System",
            'email' => "administrator@imuko.co",
            'password' => Hash::make("AdminImuko2580$"),
            'status' => 1
        ])->assignRole($roleToAssign);

    }
}