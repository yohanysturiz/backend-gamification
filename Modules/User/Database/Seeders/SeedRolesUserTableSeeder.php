<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class SeedRolesUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        Role::create(['name' => 'administrator', 'guard_name' => 'api' ]);
        Role::create(['name' => 'candidate', 'guard_name' => 'api']);
        Role::create(['name' => 'collaborator', 'guard_name' => 'api']);
        Role::create(['name' => 'client', 'guard_name' => 'api']);
        Role::create(['name' => 'analyst', 'guard_name' => 'api']);
    }
}
