---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/docs/collection.json)

<!-- END_INFO -->

#Country


APIs for managing general data
<!-- START_c7fae8a06934c43d9aac54f559ea8bee -->
## Lista de Paises

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/country" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/country"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "locale": "en",
    "message": "OK",
    "data": [
        {
            "uuid": "",
            "name": "",
            "description": "",
            "status": ""
        }
    ]
}
```

### HTTP Request
`GET api/country`


<!-- END_c7fae8a06934c43d9aac54f559ea8bee -->

<!-- START_ad737d96b4511e210e932bbb8c8a5b05 -->
## Crear Pais

> Example request:

```bash
curl -X POST \
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/country" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"ex","description":"consequatur"}'

```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/country"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "ex",
    "description": "consequatur"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 201,
    "locale": "en",
    "message": "OK",
    "data": {
        "uuid": "ASWDA6547875ACS"
    }
}
```

### HTTP Request
`POST api/country`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nombre del pais Ejm: Escocia
        `description` | string |  required  | Descripcion del pais Ejm: Europa
    
<!-- END_ad737d96b4511e210e932bbb8c8a5b05 -->

<!-- START_650ba60fbca8927d674af627cddb2c2f -->
## Eliminar Pais

> Example request:

```bash
curl -X DELETE \
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/country/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"voluptatem","description":"aut"}'

```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/country/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "voluptatem",
    "description": "aut"
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "locale": "en",
    "message": "OK",
    "data": {}
}
```

### HTTP Request
`DELETE api/country/{id}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nombre del pais Ejm: Escocia
        `description` | string |  required  | Descripcion del pais Ejm: Europa
    
<!-- END_650ba60fbca8927d674af627cddb2c2f -->

#Lenguage


APIs for managing general data
<!-- START_7716fd369236ae2df36b28c2c425079c -->
## Lista de Lenguajes

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/lenguage" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/lenguage"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "locale": "en",
    "message": "OK",
    "data": [
        {
            "uuid": "",
            "name": "",
            "description": "",
            "slug": "",
            "status": "",
            "backend": "",
            "frontend": "",
            "type": ""
        }
    ]
}
```

### HTTP Request
`GET api/lenguage`


<!-- END_7716fd369236ae2df36b28c2c425079c -->

<!-- START_4bf9e2c8f3c9914e9e1cfe635df945e3 -->
## Crear Lenguaje de Programacion

> Example request:

```bash
curl -X POST \
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/lenguage" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"qui","description":"et","slug":"qui","status":2,"backend":false,"frontend":true,"type":"fugit"}'

```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/lenguage"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "qui",
    "description": "et",
    "slug": "qui",
    "status": 2,
    "backend": false,
    "frontend": true,
    "type": "fugit"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 201,
    "locale": "en",
    "message": "OK",
    "data": {
        "uuid": "",
        "name": "",
        "description": "",
        "slug": "",
        "status": "",
        "backend": "",
        "frontend": "",
        "type": ""
    }
}
```

### HTTP Request
`POST api/lenguage`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Nombre del lenguaje Ejm: ASP Clasico
        `description` | string |  optional  | Descripcion del lenguaje Ejm: Asp lenguaje de progracion backend
        `slug` | string |  required  | Nombre para hacer el filtro o la busqueda del lenguaje Ejm: asp-clasico
        `status` | integer |  required  | Estatus del lenguaje para ser mostrado en las consultas o listas Ejm: 1
        `backend` | boolean |  required  | Boolean para identificar si es un lenguaje para el frontend o el backend Ejm: 1
        `frontend` | boolean |  required  | Boolean para identificar si es un lenguaje para el frontend o el backend Ejm: 0
        `type` | string |  optional  | Campo que puede ser usado para cualquier filtro Ejm: 0
    
<!-- END_4bf9e2c8f3c9914e9e1cfe635df945e3 -->

<!-- START_eb2416bdcc40ba817da9486fea45025d -->
## Eliminar Lenguage

> Example request:

```bash
curl -X DELETE \
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/lenguage/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/lenguage/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "locale": "en",
    "message": "OK",
    "data": {}
}
```

### HTTP Request
`DELETE api/lenguage/{id}`


<!-- END_eb2416bdcc40ba817da9486fea45025d -->

#general


<!-- START_ebc622411eb3a60a5ec334d83975bb9f -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/auth" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/auth"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET auth`


<!-- END_ebc622411eb3a60a5ec334d83975bb9f -->

<!-- START_2d02ccccc20837c33830c73e9534cd14 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[]
```

### HTTP Request
`GET api/developers`


<!-- END_2d02ccccc20837c33830c73e9534cd14 -->

<!-- START_0b2d9fddf749eebf188ee18656695d5d -->
## Show the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/developers/{id}`


<!-- END_0b2d9fddf749eebf188ee18656695d5d -->

<!-- START_81fd63e5d32c6134eadd8d9ce5f43cad -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developer" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developer"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/developer`


<!-- END_81fd63e5d32c6134eadd8d9ce5f43cad -->

<!-- START_0e2ac3fcc4006fe864ada86262c2d2f7 -->
## Generate a pdf with information of the developer

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developers/cv/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/developers/cv/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/developers/cv/{id}`


<!-- END_0e2ac3fcc4006fe864ada86262c2d2f7 -->

<!-- START_cf76b27c1b33e2979448a26f9f27dcc1 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/developer" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/developer"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[]
```

### HTTP Request
`GET developer`


<!-- END_cf76b27c1b33e2979448a26f9f27dcc1 -->

<!-- START_13479cb5bd328c8b4cb71374f6c088af -->
## Display a listing of the Departments.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/departments/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/departments/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[]
```

### HTTP Request
`GET api/departments/{id}`


<!-- END_13479cb5bd328c8b4cb71374f6c088af -->

<!-- START_6197457c48b0cc97b8c9fa8e10092601 -->
## Display a listing of the Cities.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/cities/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/cities/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[]
```

### HTTP Request
`GET api/cities/{id}`


<!-- END_6197457c48b0cc97b8c9fa8e10092601 -->

<!-- START_4d36a2828ff43205fcdf97b0cf6fdcfe -->
## Display a listing of the Cities.

> Example request:

```bash
curl -X POST \
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/cities" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/cities"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/cities`


<!-- END_4d36a2828ff43205fcdf97b0cf6fdcfe -->

<!-- START_5cb782d40bcb167100dc47e33ab24dbe -->
## Display a listing of the Frameworks.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/frameworks" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/api/frameworks"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    {
        "id": 1,
        "uuid": "d8750102-44d8-4dee-97ac-16578cf18c7e",
        "programming_lenguage_id": 268,
        "name": "Angular",
        "description": null,
        "slug": "angular",
        "status": 1,
        "backend": 0,
        "frontend": 1,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 2,
        "uuid": "ed172b1a-6f2f-4c25-b31d-4595cc4f472e",
        "programming_lenguage_id": 360,
        "name": "Laravel",
        "description": null,
        "slug": "laravel",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 3,
        "uuid": "050a9632-dc9b-4951-b76f-ddc9e3f35cb5",
        "programming_lenguage_id": 422,
        "name": "Sinatra",
        "description": null,
        "slug": "sinatra",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 4,
        "uuid": "dd9de284-012b-4fb4-b8c0-3eb86600a4a4",
        "programming_lenguage_id": 360,
        "name": "CodeIgnater",
        "description": null,
        "slug": "codeignater",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 5,
        "uuid": "531e7f8c-2cff-4d49-8788-0d4928fe17b7",
        "programming_lenguage_id": 268,
        "name": "Jquery",
        "description": null,
        "slug": "jquery",
        "status": 1,
        "backend": 0,
        "frontend": 1,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 6,
        "uuid": "15bcaf6d-19a7-494c-bd79-ee2f658e1e17",
        "programming_lenguage_id": 422,
        "name": "RubyOnRails",
        "description": null,
        "slug": "rubyonrails",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 7,
        "uuid": "2f4e1170-c241-4ec9-880e-c6515cc9b0f6",
        "programming_lenguage_id": 38,
        "name": ".Net",
        "description": null,
        "slug": ".net",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 8,
        "uuid": "2da7ba13-a71a-4e8b-93f0-09f2736c2841",
        "programming_lenguage_id": 268,
        "name": "Vuejs",
        "description": null,
        "slug": "vuejs",
        "status": 1,
        "backend": 0,
        "frontend": 1,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 9,
        "uuid": "9c88d235-29e9-4ab6-8325-7c06eb1555dd",
        "programming_lenguage_id": 268,
        "name": "React",
        "description": null,
        "slug": "react",
        "status": 1,
        "backend": 0,
        "frontend": 1,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 10,
        "uuid": "011bca0a-efac-4f21-9664-b89ed8c7bb51",
        "programming_lenguage_id": 268,
        "name": "React Native",
        "description": null,
        "slug": "react native",
        "status": 1,
        "backend": 0,
        "frontend": 1,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 11,
        "uuid": "6c7f2659-ad4c-44e8-a764-c9a3a5bf2266",
        "programming_lenguage_id": 268,
        "name": "IONIC",
        "description": null,
        "slug": "ionic",
        "status": 1,
        "backend": 0,
        "frontend": 1,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 12,
        "uuid": "a0ec128a-d116-4c12-875d-d90cc8c6d500",
        "programming_lenguage_id": 400,
        "name": "Django",
        "description": null,
        "slug": "django",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 13,
        "uuid": "ade7cdd7-26fc-4011-8822-94ebf76dc2fd",
        "programming_lenguage_id": 268,
        "name": "Xamarin",
        "description": null,
        "slug": "xamarin",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 14,
        "uuid": "6aa783c1-8e9f-498d-b889-4db02a9310a8",
        "programming_lenguage_id": 146,
        "name": "Flutter",
        "description": null,
        "slug": "flutter",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 15,
        "uuid": "50777a18-cbaf-4863-a86c-652dd2edf0c4",
        "programming_lenguage_id": 400,
        "name": "TensorFlow",
        "description": null,
        "slug": "tensorflow",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 16,
        "uuid": "f5e1496b-7d14-423c-858f-7b4ace7c5f2f",
        "programming_lenguage_id": 267,
        "name": "Spring",
        "description": null,
        "slug": "spring",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 17,
        "uuid": "7d4c1d3e-b5b9-40d0-af56-09e79122df03",
        "programming_lenguage_id": 268,
        "name": "Nodejs",
        "description": null,
        "slug": "nodejs",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 18,
        "uuid": "4876033f-4e26-4a49-b0c2-83b6628c4b3b",
        "programming_lenguage_id": 360,
        "name": "CakePhp",
        "description": null,
        "slug": "cakephp",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    },
    {
        "id": 19,
        "uuid": "dae1a218-ba7a-4ddd-8b52-2206d442801c",
        "programming_lenguage_id": 360,
        "name": "Symfony",
        "description": null,
        "slug": "symfony",
        "status": 1,
        "backend": 1,
        "frontend": 0,
        "type": null,
        "created_at": "2020-05-10T09:52:00.000000Z",
        "updated_at": "2020-05-10T09:52:00.000000Z",
        "deleted_at": null,
        "created_by": null,
        "updated_by": null,
        "deleted_by": null
    }
]
```

### HTTP Request
`GET api/frameworks`


<!-- END_5cb782d40bcb167100dc47e33ab24dbe -->

<!-- START_e4e361226a59112af91aedc5f4b811d0 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/generaldata" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/generaldata"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET generaldata`


<!-- END_e4e361226a59112af91aedc5f4b811d0 -->

<!-- START_3bcedda78ae45ef5c0f4c97a4963b7a1 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://ec2-54-189-244-145.us-west-2.compute.amazonaws.com/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET user`


<!-- END_3bcedda78ae45ef5c0f4c97a4963b7a1 -->


