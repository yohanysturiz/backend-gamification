<?php

namespace App\Http\Middleware;

use Closure;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()) {
            $userRol = $request->user()->roles()->first()->name;
            if ($userRol == "administrator" || $userRol == "analyst") return $next($request);
        }
        
        return ResponseBuilder::error(403);
    }
}
