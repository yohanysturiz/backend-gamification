<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\UserEmail;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\SMTPApi;
use Modules\PaymentAccount\Http\Controllers\PaymentAccountController;
use SendinBlue\Client\Model\SendSmtpEmail;
use Modules\User\Entities\User;
use GuzzleHttp\Client as GuzzleClient;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\Model\CreateContact;

class EmailController extends Controller
{

    protected $config;
    protected $smtpApiInstance;
    protected $pc;
    protected $contactApiInstance;

    /**
     * Email controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', config('app.sendin'));
        $this->config = Configuration::getDefaultConfiguration()->setApiKey('partner-key', config('app.sendin'));

        // Configure SMTP API key authorization: api-key
        $this->smtpApiInstance = new SMTPApi(new GuzzleClient(), $this->config);
        $this->pc = new PaymentAccountController;

        // Configure Contact API key authorization: api-key
        $this->contactApiInstance = new ContactsApi(new GuzzleClient, $this->config);
    }

    /**
     * Send Email When Developer Creates Account.
     * @param $contact
     * @param $templateId
     * @param $listId
     * @return Response
     * @throws \SendinBlue\Client\ApiException
     */
    public function WelcomeEmail($contact, $templateId, $listId)
    {

        if (!UserEmail::where('user_id', $contact->user_id)->where('template_id', $templateId)->first()) {
            $sendSmtpEmail = new SendSmtpEmail();
            $sendSmtpEmail['to'] = array(array('email' => $contact->emailcandidate));
            $sendSmtpEmail['templateId'] = $templateId;
            try {
                // $this->CreateDeveloperContact($contact, $listId);
                $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
                return $result;
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        } else {
            return false;
        }
    }

    /**
     * Send Email When Client Creates Request.
     * @return Response
     */
    public function RequestEmailResponse($order, $templateId, $listId, $lenguaje, $framework)
    {
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $order->email));
        $sendSmtpEmail['templateId'] = $templateId;
        $sendSmtpEmail['params'] = array('orderid' => $order->id, 'description' => $order->description, 'name' => $order->first_name . ' ' . $order->last_name);


        $sendSmtpEmailAdmin = new SendSmtpEmail();
        $sendSmtpEmailAdmin['to'] = array(array('email' => config('app.admin_email')));
        $sendSmtpEmailAdmin['templateId'] = 3;
        $sendSmtpEmailAdmin['params'] = array(
            'orderid' => $order->id,
            'description' => $order->description,
            'name' => $order->first_name . ' ' . $order->last_name,
            'email' => $order->email,
            'telefono' => $order->phone,
            'lenguaje' => 'Lenguaje:' . ' ' . $lenguaje,
            'framework' => 'Framework:' . ' ' . $framework,
        );
        try {
            // $this->CreateClientContact($order, $listId);
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            $admin = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmailAdmin);
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Send Email When Client Creates Request.
     * @return Response
     */
    public function PaymentAccountEmailResponse($id)
    {
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => config('app.admin_nuevo', 'app.admin_contadora')));
        $sendSmtpEmail['templateId'] = 7;
        $sendSmtpEmail['attachment'] = array(array(
            "name" => "cuenta-cobro.pdf",
            "content" => $this->pc->SafePaBase64($id)
        ));
        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Send Email When Candidate Create Payment Account.
     * @return Response
     */
    public function PaymentAccountEmailCreate($id, $pdfs)
    {
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => config('app.admin_email')));
        $sendSmtpEmail['templateId'] = 7;
        $sendSmtpEmail['attachment'] = $pdfs;

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    /**
     * Send Email When Client Creates Request.
     * @return Response
     */
    public function InvoiceEmailResponse($id, $user_id)
    {
        $user = User::find($user_id);
        $res = $this->GetContact($user->email);
        if ($res == false) {
            $createContact = new CreateContact();
            $createContact['email'] = $user->email;
            $createContact['attributes'] = array(
                'SMS' => '+570000000000',
                'NOMBRE' => $user->first_name,
                'APELLIDOS' => $user->last_name,
                'USERNAME' => $user->first_name . " " . $user->last_name
            );
            $createContact['listIds'] = array(2);
            $createContact['emailBlacklisted'] = false;
            $createContact['smsBlacklisted'] = false;

            $sendSmtpEmail = new SendSmtpEmail();
            $sendSmtpEmail['to'] = array(array('email' => $user->email));
            $sendSmtpEmail['templateId'] = 8;
            $sendSmtpEmail['attachment'] = array(array(
                "name" => "comprobante.pdf",
                "content" => $this->pc->SafeInvBase64($id)
            ));

            try {
                // $result = $this->contactApiInstance->createContact($createContact);
                $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
                return $result;
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $sendSmtpEmail = new SendSmtpEmail();
            $sendSmtpEmail['to'] = array(array('email' => $user->email));
            $sendSmtpEmail['templateId'] = 8;
            $sendSmtpEmail['attachment'] = array(array(
                "name" => "comprobante.pdf",
                "content" => $this->pc->SafeInvBase64($id)
            ));
            try {
                $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
                return $result;
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * Send Email To Client When Candidates Are Selected.
     * @return Response
     */
    public function EmailRecoveryPassword($contact, $rp, $templateId, $listId)
    {
        $url = config("app.url_client", "https://app.imuko.co") . "/#/reset-password?token=" . $rp->remember_token;
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $contact->email));
        $sendSmtpEmail['templateId'] = $templateId;
        $sendSmtpEmail['params'] = array('LINK' => $url);

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $result;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Send Email To Client When Candidates Are Selected.
     * @return Response
     */
    public function WelcomeCustomerPassword($contact)
    {
        $res = $this->GetContact($contact->email_company);
        if ($res == false) {
            $createContact = new CreateContact();
            $createContact['email'] = $contact->email_company;
            $createContact['attributes'] = array(
                'SMS' => $contact->phone,
                'NOMBRE' => $contact->responsable,
                'APELLIDOS' => $contact->responsable,
                'USERNAME' => $contact->company_name
            );
            $createContact['listIds'] = array(3);
            $createContact['emailBlacklisted'] = false;
            $createContact['smsBlacklisted'] = false;

            $sendSmtpEmail = new SendSmtpEmail();
            $sendSmtpEmail['to'] = array(array('email' => $contact->email_company));
            $sendSmtpEmail['templateId'] = 9;

            try {
                $result = $this->contactApiInstance->createContact($createContact);
                $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
                return $result;
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $sendSmtpEmail = new SendSmtpEmail();
            $sendSmtpEmail['to'] = array(array('email' => $contact->email_company));
            $sendSmtpEmail['templateId'] = 9;

            try {
                $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
                return $result;
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     * Send Email To Client When Candidates Are Selected.
     * @return Response
     */


    public function EmailDeveloperSelected($order, $templateId, $pdfs, $emailcandidates)
    {
        $link = config("app.url_client", 'https://app.imuko.co') . "/#/selected-candidates/" . $order->uuid;
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $order->email));
        $sendSmtpEmail['templateId'] = $templateId;
        $sendSmtpEmail['params'] = array('orderid' => $order->id, 'name' => $order->first_name . ' ' . $order->last_name, 'description' => $order->description, 'link' => $link);

        try {
            return $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sendEmailPayLote($html, $templateId, $pdfs)
    {
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => "kimberlincaceres321@gmail.com"));
        $sendSmtpEmail['templateId'] = 20;
        $sendSmtpEmail['params'] = array('description' => $html);
        $sendSmtpEmail['attachment'] = $pdfs;

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $result;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Send Email To Client When Candidates Are Selected.
     * @return Response
     */
    public function EmailPasswordChangeConfirm($contact, $templateId)
    {
        $sendSmtpEmail = new SendSmtpEmail();
        $sendSmtpEmail['to'] = array(array('email' => $contact->email));
        $sendSmtpEmail['templateId'] = $templateId;

        try {
            $result = $this->smtpApiInstance->sendTransacEmail($sendSmtpEmail);
            return $result;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function CreateDeveloperContact($contact, $listId)
    {
        $res = $this->GetContact($contact->emailcandidate);
        if ($res == false) {
            $createContact = new CreateContact();
            $createContact['email'] = $contact->emailcandidate;
            $createContact['attributes'] = array('NOMBRE' => $contact->namecandidate, 'APELLIDOS' => $contact->lastnamecandidate, 'USERNAME' => $contact->namecandidate);
            $createContact['listIds'] = array($listId);
            $createContact['emailBlacklisted'] = false;
            $createContact['smsBlacklisted'] = false;

            try {
                $result = $this->contactApiInstance->createContact($createContact);
                return $result;
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            return true;
        }
    }

    public function CreateClientContact($contact, $listId)
    {
        $res = $this->GetContact($contact->email);
        if ($res == false) {
            $createContact = new CreateContact();
            $createContact['email'] = $contact->email;
            $createContact['attributes'] = array('NOMBRE' => $contact->first_name, 'APELLIDOS' => $contact->first_name, 'USERNAME' => $contact->first_name);
            $createContact['listIds'] = array($listId);
            $createContact['emailBlacklisted'] = false;
            $createContact['smsBlacklisted'] = false;

            try {
                $result = $this->contactApiInstance->createContact($createContact);
                return $result;
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            return true;
        }
    }

    public function GetContact($email)
    {
        $curl = curl_init();
        $api_key = "api-key: " . config('app.sendin');
        $param = "https://api.sendinblue.com/v3/contacts/" . $email;
        curl_setopt_array($curl, [
            CURLOPT_URL => $param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                $api_key
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            if (substr($response, 2, 4) == "code") {
                return false;
            } else {
                return true;
            }
        }
    }
}
