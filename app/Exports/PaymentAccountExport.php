<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\PaymentAccount\Entities\PaymentAccount;


class PaymentAccountExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PaymentAccount::select('uuid', 'user_id', 'correlative_account_number', 'correlative_account_number_system', 
        'client_id', 'status', 'full_name', 'last_name', 'type_document', 'nro_document', 'nro_activity_economic',
        'person_type', 'city_of_service', 'description_service', 'phone_number', 'bank_name', 'type_bank_account',
        'nro_bank_account', 'value_service_alphabet', 'value_service_number', 'resident_country', 'payment_personal',
        'rent', 'created_at')->get();
    }
    public function headings():array{
        return[
            'Uuid',
            'User_id',
            'Nro. de cuenta ',
            'Nro. de cuenta sistema',
            'Id-cliente',
            'Status',
            'Nombre',
            'Apellido',
            'Tipo de documento',
            'No. de documento',
            'No. de actividad economica',
            'Tipo de persona',
            'Ciudad del servicio',
            'Descripcion del servicio',
            'Numero de telefono',
            'Banco',
            'Tipo de cuenta',
            'No de cuenta',
            'valor del servicio',
            'valor del servicio numero',
            'Pais de residencia',
            'Payment_personal',
            'Rent',
            'Fecha de creación'
        ];
    }
   
}
