<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Modules\Clients\Entities\Contract;
use Modules\Developer\Entities\Developer;
use Modules\GeneralData\Entities\Country\Country;


class ContractClientExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Contract::select(
            'customer.company_name',
            'contract.contract_value_number',
            'contract.type_document',
            'contract.number_document',
            'contract.type_contract',
            'contract.status',
            'contract.start_date',
            'contract.end_date',
            DB::raw('IF(contract.candidate_signed = 0, "Por firmar", "Firmado") AS Signed'))
            ->leftjoin('customer', 'customer.id', '=', 'contract.client_id')
            ->where('contract.type','client')
            ->get();
    }
    public function headings():array{
        return[
            'Company Name',
            'Value',
            'Document Type',
            'Document Number',
            'Contract',
            'Status',
            'Start Date',
            'End Date',
            'Signed',
        ];
    }

}
