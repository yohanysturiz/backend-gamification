<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Clients\Events\Datefinished;
use Modules\Clients\Listeners\QueryExecutedListener;
use Modules\Clients\Listeners\ReloaddateListener;
use Modules\Developer\Events\CreateDeveloperUser;
use Modules\User\Listeners\CreateDeveloperUserListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        CreateDeveloperUser::class => [
            CreateDeveloperUserListener::class
        ],
        Datefinished::class => [
            ReloaddateListener::class
        ],
        'Illuminate\Database\Events\QueryExecuted' => [
            QueryExecutedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
