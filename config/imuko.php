<?php

return [

    /** Save Files Disk */
    'disk' => env('DRIVER_SAVE_FILE', 'public'),

    'document_list_candidate' => [
        "documento_de_identidad",
        "certificado_cuenta_bancaria",
        "rut",
        "firma_digital",
        "recibo_servicio_publico_residencia",
        "contrato_fisico",
        "carta_renuncia",
        "hoja_de_vida"
    ],

    'document_list_client' => [
        "camara_comercio",
        "cc_representante_legal",
        "certificado_cuenta_bancaria",
        "recibo_servicio_publico_residencia",
        "documento_de_identidad",
        "referencias_comerciales",
        "rut",
        "firma_digital",
        "contrato_fisico"
    ],

    'contract_type' => [
        0 => 'prestacion de servicios',
        1 => 'por obra labor',
        2 => 'termino indefinido'
    ]
];
